﻿using System;
using System.Runtime.Serialization;

namespace HWEffectLibray
{
    [Serializable]
    public sealed class EffectException : Exception
    {
        public EffectException() { }

        public EffectException(string message) : base(message) { }

        public EffectException(string message, Exception innerException) : base(message, innerException) { }
    }
}