﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace HWEffectLibray
{
    internal delegate void NodeChangedEventHandler();

    internal class Node
    {
        private Node parent = null;
        private List<Node> children = new List<Node>();
        private Vector3 position = Vector3.Zero;
        private Vector3 rotation = Vector3.Zero;
        private Vector3 scale = Vector3.One;

        public event NodeChangedEventHandler PositionChanged;
        public event NodeChangedEventHandler RotationChanged;
        public event NodeChangedEventHandler ScaleChanged;
        public event NodeChangedEventHandler ParentChanged;

        private void InvokeChange(NodeChangedEventHandler evt)
        {
            evt.Invoke();
            foreach (Node child in Children)
                child.InvokeChange(evt);
        }

        public Node Parent
        {
            get { return parent; }
            set
            {
                if (parent == value) return;
                parent = value;
                InvokeChange(ParentChanged);
            }
        }

        public List<Node> Children
        {
            get { return children; }
        }

        public Vector3 Position
        {
            get { return position; }
            set
            {
                if (position == value) return;
                position = value;
                InvokeChange(PositionChanged);
            }
        }

        public Vector3 Rotation
        {
            get { return rotation; }
            set
            {
                if (rotation == value) return;
                rotation = value;
                InvokeChange(RotationChanged);
            }
        }

        public Vector3 Scale
        {
            get { return scale; }
            set
            {
                if (scale == value) return;
                scale = value;
                InvokeChange(ScaleChanged);
            }
        }

        public Matrix4 NodeMatrix
        {
            get
            {
                return Matrix4.CreateScale(scale) *
                    Matrix4.CreateRotationZ(rotation.Z) *
                    Matrix4.CreateRotationY(rotation.Y) *
                    Matrix4.CreateRotationX(rotation.X) *
                    Matrix4.CreateTranslation(position);
            }
        }

        public Matrix4 WorldMatrix
        {
            get
            {
                Matrix4 m = NodeMatrix;
                for (Node x = Parent; x != null; x = x.Parent)
                {
                    m *= x.NodeMatrix;
                }
                return m;
            }
        }

        public void AddChild(Node child)
        {
            Children.Add(child);
            child.Parent = this;
        }

        public int ChildrenCount => Children.Count;
        public int TotalChildrenCount
        {
            get
            {
                int count = ChildrenCount;
                foreach (Node x in Children)
                    count += x.TotalChildrenCount;
                return count;
            }
        }
    }

    internal class Particle : Node
    {
        private Vector3 velocity;
        private float drag;

        public Vector3 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public float Drag
        {
            get { return drag; }
            set { drag = value; }
        }

        public void Update(float delta)
        {
            Parent.Position += Velocity * delta;
            Velocity *= (float)Math.Pow(Drag, delta);
        }
    }
}