﻿using System;
using static HWEffectLibray.Utils;

namespace HWEffectLibray.Effects
{
    public class HyperspaceEffectProperties : EffectProperties
    {
        public EffectBlending Blending { get; set; } = EffectBlending.NoBlending;
        public ColorKeyframes Color { get; internal set; } = new ColorKeyframes();
        public float Duration { get; set; } = 1f;
        public ColorKeyframes EdgeColor { get; internal set; } = new ColorKeyframes();
        public string EdgeTexture { get; set; } = "";
        public FloatKeyframes EdgeTexture_U_Offset { get; internal set; } = new FloatKeyframes();
        public FloatKeyframes EdgeTexture_U_Repeat { get; internal set; } = new FloatKeyframes();
        public string EdgeTexture2 { get; set; } = "";
        public FloatKeyframes EdgeTexture2_U_Offset { get; internal set; } = new FloatKeyframes();
        public FloatKeyframes EdgeTexture2_U_Repeat { get; internal set; } = new FloatKeyframes();
        public FloatKeyframes Height { get; internal set; } = new FloatKeyframes();
        public FloatKeyframes LineWidth { get; internal set; } = new FloatKeyframes();
        public VectorKeyframes Offset { get; internal set; } = new VectorKeyframes();
        public string Texture { get; set; } = "";
        public FloatKeyframes Width { get; internal set; } = new FloatKeyframes();
    }

    public class HyperspaceEffect : Effect
    {
        public override EffectStyle Style => EffectStyle.Hyperspace;
        protected new HyperspaceEffectProperties Properties = new HyperspaceEffectProperties();

        private HyperspaceEffect(string name, EffectProperties props) : base(name, props) { }

        internal static HyperspaceEffect LoadEffect(string name, LuaProperties luaprops)
        {
            if (EffectLoaded(name)) return (HyperspaceEffect)GetEffect(name);

            HyperspaceEffectProperties props = new HyperspaceEffectProperties();

            props.Blending = (EffectBlending)GetPropertyValue(luaprops, "Blending", 0);
            props.Color = GetPropertyValue(luaprops, "Colour", new ColorKeyframes());
            props.Duration = GetPropertyValue(luaprops, "Duration", 1f);
            props.EdgeColor = GetPropertyValue(luaprops, "EdgeColor", new ColorKeyframes());
            props.EdgeTexture = GetPropertyValue(luaprops, "EdgeTexture", "");
            props.EdgeTexture_U_Offset = GetPropertyValue(luaprops, "EdgeTexture_U_Offset", new FloatKeyframes());
            props.EdgeTexture_U_Repeat = GetPropertyValue(luaprops, "EdgeTexture_U_Repeat", new FloatKeyframes());
            props.EdgeTexture2 = GetPropertyValue(luaprops, "EdgeTexture", "");
            props.EdgeTexture2_U_Offset = GetPropertyValue(luaprops, "EdgeTexture2_U_Offset", new FloatKeyframes());
            props.EdgeTexture2_U_Repeat = GetPropertyValue(luaprops, "EdgeTexture2_U_Repeat", new FloatKeyframes());
            props.Height = GetPropertyValue(luaprops, "Height", new FloatKeyframes());
            props.LineWidth = GetPropertyValue(luaprops, "LineWidth", new FloatKeyframes());
            props.Offset = GetPropertyValue(luaprops, "Offset", new VectorKeyframes());
            props.Texture = GetPropertyValue(luaprops, "Texture", "");
            props.Width = GetPropertyValue(luaprops, "Width", new FloatKeyframes());

            return new HyperspaceEffect(name, props);
        }

        public HyperspaceEffect

        public override void Reset()
        {

        }

        public override void Update(float delta)
        {
            base.Update(delta);


        }

        public override void Render()
        {

        }

        public override void Cleanup()
        {

        }
    }
}
