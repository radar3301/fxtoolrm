﻿using System;
using static HWEffectLibray.Utils;

namespace HWEffectLibray.Effects
{
    public class RingEffectProperties : EffectProperties
    {
        internal string birthspawnFX;
        internal string spawnFX;

        private Effect birthspawnEffect = null;
        private Effect spawnEffect = null;

        public bool AlternateFOV { get; set; }
        public bool Billboard { get; set; }
        public Effect BirthspawnEffect
        {
            get { return GetEffect(ref birthspawnEffect, ref birthspawnFX); }
            set { SetEffect(value, ref birthspawnEffect, ref birthspawnFX); }
        }
        public EffectBlending Blending { get; set; }
        public ColorKeyframes Colour { get; internal set; }
        public float DepthOffset { get; set; }
        public float Duration { get; set; }
        public bool Loop { get; set; }
        public string Mesh { get; set; }
        public VectorKeyframes Offset { get; internal set; }
        public float PointDistance { get; set; }
        public FloatKeyframes Radius { get; internal set; }
        public float RadiusEpsilon { get; set; }
        public bool RandomBitmap { get; set; }
        public float RateEpsilon { get; set; }
        public bool SelfIlluminated { get; set; }
        public int SortOrder { get; set; }
        public Effect SpawnEffect
        {
            get { return GetEffect(ref spawnEffect, ref spawnFX); }
            set { SetEffect(value, ref spawnEffect, ref spawnFX); }
        }
        public FloatKeyframes SpinX { get; internal set; }
        public FloatKeyframes SpinY { get; internal set; }
        public FloatKeyframes SpinZ { get; internal set; }
        public float SpinEpsilonX { get; set; }
        public float SpinEpsilonY { get; set; }
        public float SpinEpsilonZ { get; set; }
        public bool SpinRandom { get; set; }
        public string Texture { get; set; }
        public bool UseDepthSort { get; set; }
        public bool UseDepthTest { get; set; }
        public bool UseMesh { get; set; }
    }

    public class RingEffect : Effect
    {
        public override EffectStyle Style => EffectStyle.Ring;
        protected new RingEffectProperties Properties = new RingEffectProperties();

        private RingEffect(string name, EffectProperties props) : base(name, props) { }

        internal static RingEffect LoadEffect(string name, LuaProperties luaprops)
        {
            if (EffectLoaded(name)) return (RingEffect)GetEffect(name);

            RingEffectProperties props = new RingEffectProperties();

            props.AlternateFOV = GetPropertyValue(luaprops, "AlternateFOV", false);
            props.Billboard = GetPropertyValue(luaprops, "Billboard", false);
            props.birthspawnFX = GetPropertyValue(luaprops, "Birthspawn_FX", "");
            props.Blending = (EffectBlending)GetPropertyValue(luaprops, "Blending", 0);
            props.Colour = GetPropertyValue(luaprops, "Colour", new ColorKeyframes());
            props.DepthOffset = GetPropertyValue(luaprops, "DepthOffset", 0f);
            props.Duration = GetPropertyValue(luaprops, "Duration", 0f);
            props.Loop = GetPropertyValue(luaprops, "Loop", false);
            props.Mesh = GetPropertyValue(luaprops, "Mesh", "");
            props.Offset = GetPropertyValue(luaprops, "Offset", new VectorKeyframes());
            props.PointDistance = GetPropertyValue(luaprops, "Duration", 0f);
            props.Radius = GetPropertyValue(luaprops, "Radius", new FloatKeyframes());
            props.RadiusEpsilon = GetPropertyValue(luaprops, "RadiusEpsilon", 0f);
            props.RandomBitmap = GetPropertyValue(luaprops, "RandomBitmap", false);
            props.RateEpsilon = GetPropertyValue(luaprops, "RateEpsilon", 0f);
            props.SelfIlluminated = GetPropertyValue(luaprops, "SelfIlluminated", false);
            props.SortOrder = GetPropertyValue(luaprops, "SortOrder", 0);
            props.spawnFX = GetPropertyValue(luaprops, "Spawn_FX", "");
            props.SpinX = GetPropertyValue(luaprops, "SpinX", new FloatKeyframes());
            props.SpinY = GetPropertyValue(luaprops, "SpinY", new FloatKeyframes());
            props.SpinZ = GetPropertyValue(luaprops, "SpinZ", new FloatKeyframes());
            props.SpinEpsilonX = GetPropertyValue(luaprops, "SpinEpsilonX", 0f);
            props.SpinEpsilonY = GetPropertyValue(luaprops, "SpinEpsilonY", 0f);
            props.SpinEpsilonZ = GetPropertyValue(luaprops, "SpinEpsilonZ", 0f);
            props.SpinRandom = GetPropertyValue(luaprops, "SpinRandom", false);
            props.Texture = GetPropertyValue(luaprops, "Texture", "");
            props.UseDepthSort = GetPropertyValue(luaprops, "UseDepthSort", false);
            props.UseDepthTest = GetPropertyValue(luaprops, "UseDepthTest", false);
            props.UseMesh = GetPropertyValue(luaprops, "UseMesh", false);

            return new RingEffect(name, props);
        }

        public override void Reset()
        {
            if (Properties.SpawnEffect != null) Properties.SpawnEffect.Reset();
            if (Properties.BirthspawnEffect != null) Properties.BirthspawnEffect.Reset();
        }

        public override void Update(float delta)
        {
            base.Update(delta);
            if (Properties.SpawnEffect != null) Properties.SpawnEffect.Update(delta);
            if (Properties.BirthspawnEffect != null) Properties.BirthspawnEffect.Update(delta);


        }

        public override void Render()
        {
            if (Properties.SpawnEffect != null) Properties.SpawnEffect.Render();
            if (Properties.BirthspawnEffect != null) Properties.BirthspawnEffect.Render();


        }

        public override void Cleanup()
        {
            if (Properties.SpawnEffect != null) Properties.SpawnEffect.Cleanup();
            if (Properties.BirthspawnEffect != null) Properties.BirthspawnEffect.Cleanup();
        }
    }
}
