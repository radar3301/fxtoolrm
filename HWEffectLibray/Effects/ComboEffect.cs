﻿using NLua;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HWEffectLibray.Utils;

namespace HWEffectLibray.Effects
{
    public class ComboEffectProperties : EffectProperties
    {
        internal string fx1;
        internal string fx2;
        internal string fx3;
        internal string fx4;
        internal string fx5;
        internal string fx6;
        internal string fx7;
        internal string fx8;

        internal Effect effect1;
        internal Effect effect2;
        internal Effect effect3;
        internal Effect effect4;
        internal Effect effect5;
        internal Effect effect6;
        internal Effect effect7;
        internal Effect effect8;

        public Effect Effect1
        {
            get { return GetEffect(ref effect1, ref fx1); }
            set { SetEffect(value, ref effect1, ref fx1); }
        }
        public Effect Effect2
        {
            get { return GetEffect(ref effect2, ref fx2); }
            set { SetEffect(value, ref effect2, ref fx2); }
        }
        public Effect Effect3
        {
            get { return GetEffect(ref effect3, ref fx3); }
            set { SetEffect(value, ref effect3, ref fx3); }
        }
        public Effect Effect4
        {
            get { return GetEffect(ref effect4, ref fx4); }
            set { SetEffect(value, ref effect4, ref fx4); }
        }
        public Effect Effect5
        {
            get { return GetEffect(ref effect5, ref fx5); }
            set { SetEffect(value, ref effect5, ref fx5); }
        }
        public Effect Effect6
        {
            get { return GetEffect(ref effect6, ref fx6); }
            set { SetEffect(value, ref effect6, ref fx6); }
        }
        public Effect Effect7
        {
            get { return GetEffect(ref effect7, ref fx7); }
            set { SetEffect(value, ref effect7, ref fx7); }
        }
        public Effect Effect8
        {
            get { return GetEffect(ref effect8, ref fx8); }
            set { SetEffect(value, ref effect8, ref fx8); }
        }
    }

    public class ComboEffect : Effect
    {
        public override EffectStyle Style => EffectStyle.Combo;
        protected new ComboEffectProperties Properties = new ComboEffectProperties();

        private ComboEffect(string name, EffectProperties props) : base(name, props) { }

        internal static ComboEffect LoadEffect(string name, LuaProperties luaprops)
        {
            if (EffectLoaded(name)) return (ComboEffect)GetEffect(name);

            ComboEffectProperties props = new ComboEffectProperties();

            props.fx1 = GetPropertyValue(luaprops, "fx1", "");
            props.fx2 = GetPropertyValue(luaprops, "fx2", "");
            props.fx3 = GetPropertyValue(luaprops, "fx3", "");
            props.fx4 = GetPropertyValue(luaprops, "fx4", "");
            props.fx5 = GetPropertyValue(luaprops, "fx5", "");
            props.fx6 = GetPropertyValue(luaprops, "fx6", "");
            props.fx7 = GetPropertyValue(luaprops, "fx7", "");
            props.fx8 = GetPropertyValue(luaprops, "fx8", "");

            return new ComboEffect(name, props);
        }

        public override void Reset()
        {
            if (Properties.Effect1 != null) Properties.Effect1.Reset();
            if (Properties.Effect2 != null) Properties.Effect2.Reset();
            if (Properties.Effect3 != null) Properties.Effect3.Reset();
            if (Properties.Effect4 != null) Properties.Effect4.Reset();
            if (Properties.Effect5 != null) Properties.Effect5.Reset();
            if (Properties.Effect6 != null) Properties.Effect6.Reset();
            if (Properties.Effect7 != null) Properties.Effect7.Reset();
            if (Properties.Effect8 != null) Properties.Effect8.Reset();
        }

        public override void Update(float delta)
        {
            base.Update(delta);
            if (Properties.Effect1 != null) Properties.Effect1.Update(delta);
            if (Properties.Effect2 != null) Properties.Effect2.Update(delta);
            if (Properties.Effect3 != null) Properties.Effect3.Update(delta);
            if (Properties.Effect4 != null) Properties.Effect4.Update(delta);
            if (Properties.Effect5 != null) Properties.Effect5.Update(delta);
            if (Properties.Effect6 != null) Properties.Effect6.Update(delta);
            if (Properties.Effect7 != null) Properties.Effect7.Update(delta);
            if (Properties.Effect8 != null) Properties.Effect8.Update(delta);
        }

        public override void Render()
        {
            if (Properties.Effect1 != null) Properties.Effect1.Render();
            if (Properties.Effect2 != null) Properties.Effect2.Render();
            if (Properties.Effect3 != null) Properties.Effect3.Render();
            if (Properties.Effect4 != null) Properties.Effect4.Render();
            if (Properties.Effect5 != null) Properties.Effect5.Render();
            if (Properties.Effect6 != null) Properties.Effect6.Render();
            if (Properties.Effect7 != null) Properties.Effect7.Render();
            if (Properties.Effect8 != null) Properties.Effect8.Render();
        }

        public override void Cleanup()
        {
            if (Properties.Effect1 != null) Properties.Effect1.Cleanup();
            if (Properties.Effect2 != null) Properties.Effect2.Cleanup();
            if (Properties.Effect3 != null) Properties.Effect3.Cleanup();
            if (Properties.Effect4 != null) Properties.Effect4.Cleanup();
            if (Properties.Effect5 != null) Properties.Effect5.Cleanup();
            if (Properties.Effect6 != null) Properties.Effect6.Cleanup();
            if (Properties.Effect7 != null) Properties.Effect7.Cleanup();
            if (Properties.Effect8 != null) Properties.Effect8.Cleanup();
        }
    }
}
