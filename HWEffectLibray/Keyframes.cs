﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWEffectLibray
{
    public interface IKeyframe { }

    public struct FloatKeyframe : IKeyframe
    {
        public FloatKeyframe(float time, float value) : this()
        {
            Time = time;
            Value = value;
        }

        public float Time { get; set; }
        public float Value { get; set; }
    }

    public struct VectorKeyframe : IKeyframe
    {
        public VectorKeyframe(float time, Vector3 value) : this()
        {
            Time = time;
            Value = value;
        }

        public float Time { get; set; }
        public Vector3 Value { get; set; }
    }

    public struct ColorKeyframe : IKeyframe
    {
        public ColorKeyframe(float time, Color4 value) : this()
        {
            Time = time;
            Value = value;
        }

        public float Time { get; set; }
        public Color4 Value { get; set; }
    }

    public abstract class Keyframes<T> : List<T> where T : IKeyframe
    {
        public Keyframes() { }

        public new T this[int index]
        {
            get { return base[index]; }
            set { base[index] = value; Sort(); }
        }

        public new void Add(T item)
        {
            base.Add(item);
            Sort();
        }

        public new void Insert(int index, T item)
        {
            base.Insert(index, item);
            Sort();
        }

        public new bool Remove(T item)
        {
            bool result = base.Remove(item);
            if (result) Sort();
            return result;
        }

        public new void RemoveAt(int index)
        {
            base.RemoveAt(index);
            Sort();
        }

        protected T First => this[0];
        protected T Last => this[Count - 1];
    }
    
    public class FloatKeyframes : Keyframes<FloatKeyframe>
    {
        public FloatKeyframes() { }
        
        public void Add(float time, float value)
        {
            Add(new FloatKeyframe(time, value));
        }

        public float At(float time)
        {
            if (Count == 0) return 0;
            if (time <= First.Time) return First.Value;
            if (time >= Last.Time) return Last.Value;
            for (int i = 0; i < Count - 1; ++i)
            {
                if (this[i].Time < time && time <= this[i + 1].Time)
                {
                    float k = (time - this[i].Time) / (this[i + 1].Time - this[i].Time);
                    return (1 - k) * this[i].Value + k * this[i + 1].Value;
                }
            }
            throw new Exception("unreachable");
        }
    }

    public class VectorKeyframes : Keyframes<VectorKeyframe>
    {
        public VectorKeyframes() { }

        public void Add(float time, Vector3 value)
        {
            Add(new VectorKeyframe(time, value));
        }

        public Vector3 At(float time)
        {
            if (Count == 0) return Vector3.Zero;
            if (time <= First.Time) return First.Value;
            if (time >= Last.Time) return Last.Value;
            for (int i = 0; i < Count - 1; ++i)
            {
                if (this[i].Time < time && time <= this[i + 1].Time)
                {
                    float k = (time - this[i].Time) / (this[i + 1].Time - this[i].Time);
                    return (1 - k) * this[i].Value + k * this[i + 1].Value;
                }
            }
            throw new Exception("unreachable");
        }
    }

    public class ColorKeyframes : Keyframes<ColorKeyframe>
    {
        public ColorKeyframes() { }

        public void Add(float time, Color4 value)
        {
            Add(new ColorKeyframe(time, value));
        }

        public Color4 At(float time)
        {
            if (Count == 0) return new Color4();
            if (time <= First.Time) return First.Value;
            if (time >= Last.Time) return Last.Value;
            for (int i = 0; i < Count - 1; ++i)
            {
                if (this[i].Time < time && time <= this[i + 1].Time)
                {
                    float k = (time - this[i].Time) / (this[i + 1].Time - this[i].Time);
                    return Color4.FromXyz((1 - k) * Color4.ToXyz(this[i].Value) + k * Color4.ToXyz(this[i + 1].Value));
                }
            }
            throw new Exception("unreachable");
        }
    }
}
