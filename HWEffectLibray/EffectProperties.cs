﻿using NLua;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;

namespace HWEffectLibray
{
    internal class LuaProperties : Dictionary<string, dynamic>
    {
        public LuaProperties(LuaTable luaprops)
        {
            foreach (LuaTable prop in luaprops)
            {
                string name = ((string)prop["name"]).ToLower();
                string type = ((string)prop["type"]).ToUpper();
                object rawvalue = prop["value"];
                dynamic value;

                if (type == "VARTYPE_FLOAT")
                    value = (float)(double)rawvalue;
                else if (type == "VARTYPE_BOOL")
                    value = (double)rawvalue != 0;
                else if (type == "VARTYPE_INT")
                    value = (int)(double)rawvalue;
                else if (type == "VARTYPE_STRING")
                    value = ((string)rawvalue).ToLower();
                else if (type == "VARTYPE_COLOUR")
                    value = ParseColor((LuaTable)rawvalue);
                else if (type == "VARTYPE_VECTOR3")
                    value = ParseVector((LuaTable)rawvalue);
                else if (type == "VARTYPE_ARRAY_TIMEFLOAT")
                    value = ParseTimeFloatArray((LuaTable)rawvalue);
                else if (type == "VARTYPE_ARRAY_TIMECOLOUR")
                    value = ParseTimeColorArray((LuaTable)rawvalue);
                else if (type == "VARTYPE_ARRAY_TIMEVECTOR3")
                    value = ParseTimeVectorArray((LuaTable)rawvalue);
                else
                    throw new EffectException("unrecognized property type `" + type + "`");

                Add(name, value);
            }
        }

        private static Color4 ParseColor(LuaTable table)
        {
            float r = (float)((double)table[1] * 255);
            float g = (float)((double)table[2] * 255);
            float b = (float)((double)table[3] * 255);
            float a = (float)((double)table[4] * 255);

            return new Color4(r, g, b, a);
        }

        private static Vector3 ParseVector(LuaTable table)
        {
            float x = (float)(double)table[1];
            float y = (float)(double)table[2];
            float z = (float)(double)table[3];

            return new Vector3(x, y, z);
        }

        private static FloatKeyframes ParseTimeFloatArray(LuaTable table)
        {
            FloatKeyframes keyframes = new FloatKeyframes();

            for (int i = 1; i <= table.Keys.Count; i += 2)
            {
                keyframes.Add((float)(double)table[i], (float)(double)table[i + 1]);
            }

            return keyframes;
        }

        private static ColorKeyframes ParseTimeColorArray(LuaTable table)
        {
            ColorKeyframes keyframes = new ColorKeyframes();

            foreach (string key in table.Keys)
            {
                LuaTable entry = (LuaTable)table[key];
                float time = (float)(double)entry[1];
                byte r = (byte)((double)entry[2] * 255);
                byte g = (byte)((double)entry[3] * 255);
                byte b = (byte)((double)entry[4] * 255);
                byte a = (byte)((double)entry[5] * 255);
                keyframes.Add(time, new Color4(r, g, b, a));
            }

            return keyframes;
        }

        private static VectorKeyframes ParseTimeVectorArray(LuaTable table)
        {
            VectorKeyframes keyframes = new VectorKeyframes();

            foreach (string key in table.Keys)
            {
                LuaTable entry = (LuaTable)table[key];
                float time = (float)(double)entry[1];
                float x = (float)(double)entry[2];
                float y = (float)(double)entry[3];
                float z = (float)(double)entry[4];
                keyframes.Add(time, new Vector3(x, y, z));
            }

            return keyframes;
        }
    }

    public abstract class EffectProperties
    {
        protected Effect GetEffect(ref Effect effect, ref string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return null;
            else if (effect == null)
                effect = Effect.GetEffect(str);
            return effect;
        }

        protected void SetEffect(Effect value, ref Effect effect, ref string str)
        {
            if (value == null)
            {
                effect = null;
                str = "";
            }
            else
            {
                effect = value;
                str = value.Name;
            }
        }
    }
}