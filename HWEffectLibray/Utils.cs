﻿using System;
using System.Collections.Generic;
using static System.Convert;

namespace HWEffectLibray
{
    internal static class Utils
    {
        public static T First<T>(this IList<T> list)
        {
            return list[0];
        }

        public static T Last<T>(this IList<T> list)
        {
            return list[list.Count - 1];
        }

        public static T GetPropertyValue<T>(LuaProperties props, string name, T defaultValue)
        {
            name = name.ToLower();
            return props.ContainsKey(name) ? props[name] : defaultValue;
        }

        public static T GetPropertyValue<T>(LuaProperties props, string name) where T : new()
        {
            name = name.ToLower();
            return props.ContainsKey(name) ? props[name] : new T();
        }
    }
}
