﻿using HWEffectLibray.Effects;
using HWEffectLibray.Meshes;
using NLua;
using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWEffectLibray
{
    public enum EffectStyle
    {
        Spray,
        Ring,
        Beam,
        Trail,
        Combo,
        Light,
        Hyperspace,
        LensFlare
    }

    public enum EffectBlending
    {
        NoBlending,
        AlphaBlend,
        AdditiveBlend,
        MultiplyBlend,
        AdditiveAlphaBlend
    }

    public enum EmitterDirection
    {
        Default = 0,
        Up,
        Down
    }

    public enum ParticleDynamics
    {
        /// <summary>
        /// Particles travel at a constant velocity and trajectory for the duration of the effect.
        /// </summary>
        Default = 0,

        /// <summary>
        /// Particles are affected by gravity but disappear when they collide with the ground.
        /// </summary>
        Gravity_GroundDisappear,

        /// <summary>
        /// Particles are affected by gravity but will bounce off the ground as well.
        /// </summary>
        Gravity_GroundBounce,

        /// <summary>
        /// Particles are affected by gravity but will stick to the ground and stay in place.
        /// </summary>
        Gravity_GroundStick,

        /// <summary>
        /// Particles are affected by wind, according to the specified wind vectors.
        /// </summary>
        Wind,

        /// <summary>
        /// Particles will align themselves to the surface of the water plane in the game.
        /// These will instantly appear on the water.
        /// Given a particle speed, will travel outwards from the center of the effect.
        /// </summary>
        Water,

        /// <summary>
        /// All particles created with this dynamic stay local to an emitter’s orientation and translation.
        /// </summary>
        StayLocal,

        /// <summary>
        /// Particles emitted will align themselves to the surface of the ground plane,
        /// and if given a particle speed, will follow the contours of the landscape.
        /// </summary>
        StayOnGround,

        /// <summary>
        /// Particles are affected by gravity but will ignore the ground plane.
        /// </summary>
        Gravity_IgnoreGround,

        /// <summary>
        /// Particles will align itself to the highest surface.
        /// </summary>
        Elevate,

        /// <summary>
        /// Particles will be attracted back on to the emitter.
        /// </summary>
        EmitterAttract
    }

    public abstract class Effect
    {
        private static Dictionary<string, Effect> LoadedEffects = new Dictionary<string, Effect>();

        public abstract EffectStyle Style { get; }
        public string Name { get; }
        protected EffectProperties Properties;
        internal Node Parent { get; set; }

        public virtual void Reset() { }
        public virtual void Update(float delta) { }
        public virtual void Render() { }
        public virtual void Cleanup() { }

        protected Effect(string name, EffectProperties props)
        {
            Name = name;
            Properties = props;
        }

        public static Effect LoadEffect(string filename)
        {
            string name = Path.GetFileNameWithoutExtension(filename).ToLower();

            if (!File.Exists(filename)) return null;

            Lua lua = new Lua();
            lua.DoFile(filename);

            LuaTable fx = lua.GetTable("fx");
            if (fx == null) throw new EffectException("table `fx` expected");

            string style = (string)fx["style"];
            if (style == null) throw new EffectException("string `style` expected in table `fx`");

            LuaTable fxprops = (LuaTable)fx["properties"];
            if (fxprops == null) throw new EffectException("table `properties` expected in table `fx`");

            LuaProperties properties = new LuaProperties(fxprops);

            if (style == "STYLE_BEAM")
                return BeamEffect.LoadEffect(name, properties);
            else if (style == "STYLE_COMBO")
                return ComboEffect.LoadEffect(name, properties);
            else if (style == "STYLE_HYPERSPACE")
                return HyperspaceEffect.LoadEffect(name, properties);
            else if (style == "STYLE_LENSFLARE")
                return LensFlareEffect.LoadEffect(name, properties);
            else if (style == "STYLE_LIGHT")
                return LightEffect.LoadEffect(name, properties);
            else if (style == "STYLE_RING")
                return RingEffect.LoadEffect(name, properties);
            else if (style == "STYLE_SPRAY")
                return SprayEffect.LoadEffect(name, properties);
            else if (style == "STYLE_TRAIL")
                return TrailEffect.LoadEffect(name, properties);

            throw new EffectException("unknown style `" + style + "`");
        }

        public static bool EffectLoaded(string name)
        {
            return LoadedEffects.ContainsKey(name);
        }

        public static Effect GetEffect(string name)
        {
            if (!EffectLoaded(name))
                return LoadEffect(Config.ResolvePath(@"art\fx\" + name + ".lua"));

            return LoadedEffects[name];
        }

        public abstract EffectMesh CreateMesh();

    }
}
