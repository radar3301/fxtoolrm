﻿using HWShaderManifest;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWEffectLibray.Meshes
{
    internal class HyperspaceMesh
    {
        private Vector3 position = new Vector3();
        private float width = 0f;
        private float height = 0f;
        private Color4 color0 = new Color4();
        private Color4 color1 = new Color4();

        public Vector3 Position
        {
            get { return position; }
            set
            {
                if (position == value) return;
                position = value;
                UpdateVertexData();
            }
        }

        public float Width
        {
            get { return width; }
            set
            {
                if (width == value) return;
                width = value;
                UpdateVertexData();
            }
        }

        public float Height
        {
            get { return height; }
            set
            {
                if (height == value) return;
                height = value;
                UpdateVertexData();
            }
        }

        public Color4 WindowColor
        {
            get { return color0; }
            set
            {
                if (color0 == value) return;
                color0 = value;
                UpdateColor0Data();
            }
        }

        public Color4 EdgeColor
        {
            get { return color1; }
            set
            {
                if (color1 == value) return;
                color1 = value;
                UpdateColor1Data();
            }
        }

        public float LineWidth { get; set; } = 1f;

        public Texture WindowTexture { get; set; }
        public Texture EdgeTexture { get; set; }

        private Surface surface;
        private int pos_buffer_id;
        private int col0_buffer_id;
        private int col1_buffer_id;
        private int uv0_buffer_id;
        private int ind_buffer_id;

        public HyperspaceMesh()
        {
            surface = Manifest.UseSurface("fx_hyperspace");
            pos_buffer_id = GL.GenBuffer();
            col0_buffer_id = GL.GenBuffer();
            col1_buffer_id = GL.GenBuffer();
            uv0_buffer_id = GL.GenBuffer();
            ind_buffer_id = GL.GenBuffer();

            UpdateVertexData();
            UpdateColor0Data();
            UpdateColor1Data();

            float[] uvs = new float[] {
                0, 1, 1, 1, 1, 0, 0, 0
            };

            GL.BindBuffer(BufferTarget.ArrayBuffer, uv0_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, uvs.Length * sizeof(float), uvs, BufferUsageHint.StaticDraw);

            ushort[] indices = new ushort[] {
                0, 1, 2,
                3, 0, 2
            };

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(ushort), indices, BufferUsageHint.StaticDraw);
        }

        private void UpdateVertexData()
        {
            float[] vertices = new float[] {
                -width / 2 + position.X, -height / 2 + position.Y, position.Z,
                 width / 2 + position.X, -height / 2 + position.Y, position.Z,
                 width / 2 + position.X,  height / 2 + position.Y, position.Z,
                -width / 2 + position.X,  height / 2 + position.Y, position.Z
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, pos_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StreamDraw);
        }

        private void UpdateColor0Data()
        {
            float[] colors = new float[] {
                color0.R, color0.G, color0.B, color0.A,
                color0.R, color0.G, color0.B, color0.A,
                color0.R, color0.G, color0.B, color0.A,
                color0.R, color0.G, color0.B, color0.A
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, col0_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, colors.Length * sizeof(float), colors, BufferUsageHint.StreamDraw);
        }

        private void UpdateColor1Data()
        {
            float[] colors = new float[] {
                color1.R, color1.G, color1.B, color1.A,
                color1.R, color1.G, color1.B, color1.A,
                color1.R, color1.G, color1.B, color1.A,
                color1.R, color1.G, color1.B, color1.A
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, col1_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, colors.Length * sizeof(float), colors, BufferUsageHint.StreamDraw);
        }

        public void Draw()
        {
            // Draw window
            surface.LinkAttrib(pos_buffer_id, "inPos", 3, false);
            surface.LinkAttrib(col0_buffer_id, "inCol0", 4, false);
            surface.LinkAttrib(uv0_buffer_id, "inUV0", 2, false);
            
            if (WindowTexture != null)
                surface.AssignTexture("inTexHyper", WindowTexture.Filename, WindowTexture.ID);
            else
                surface.AssignTexture("inTexHyper", "", 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            surface.Draw(BeginMode.Triangles, 6, DrawElementsType.UnsignedShort, 0);

            // Draw edge
            surface.LinkAttrib(col1_buffer_id, "inCol0", 4, false);

            if (WindowTexture != null)
                surface.AssignTexture("inTexHyper", EdgeTexture.Filename, EdgeTexture.ID);
            else
                surface.AssignTexture("inTexHyper", "", 0);

            float line_width = GL.GetFloat(GetPName.LineWidth);
            GL.LineWidth(Math.Max(LineWidth, 0.001f));

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            surface.Draw(BeginMode.LineLoop, 4, DrawElementsType.UnsignedShort, 0);

            GL.LineWidth(line_width);
        }
}
}
