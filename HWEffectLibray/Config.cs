﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWEffectLibray
{
    public static class Config
    {
        public static List<string> DataPaths = new List<string>();

        internal static string ResolvePath(string filename)
        {
            string file = "";
            foreach (string datapath in DataPaths)
            {
                string datafile = Path.Combine(datapath, filename);
                if (File.Exists(datafile))
                    file = datafile;
            }
            return file;
        }
    }
}
