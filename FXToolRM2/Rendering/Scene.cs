﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using HWShaderManifest;
using FXToolRM.Meshes;
using static FXToolRM.Rendering.Utility;

namespace FXToolRM.Rendering
{
    public class Scene
    {
        public static Camera Camera { get; private set; } = new Camera();
        public static Shader EditorShader { get; private set; } = new EditorShader();

        private static GridMesh floor = new GridMesh();
        private static AxesMesh axes = new AxesMesh();

        public static void Draw()
        {
            GetGLError("Scene PreDraw");
            float Width = Program.GLControl.Width;
            float Height = Program.GLControl.Height;
            float Near = 0.01f;
            float Far = 1000f;

            Matrix4 camera = Camera.ViewMatrix;
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView((float)(Math.PI / 2), Width / Height, Near, Far);

            GL.UseProgram(EditorShader.ProgramID);
            EditorShader.SetUniform("inMatV", camera);
            EditorShader.SetUniform("inMatP", projection);
            axes.Draw();

            ManifestConfig.SetValue("CFG_DepthBias_FX", 0);
            ShaderManifest.SetGlobalUniform("windowInfo", new float[] { Width, Height, 1, 1 });
            ShaderManifest.SetGlobalUniform("depthInfo", new float[] { Near, Far, 1, 1 });
            ShaderManifest.SetGlobalUniform("cameraInfo", new float[] { Camera.Position.X, Camera.Position.Y, Camera.Position.Z, 1 });

            ShaderManifest.SetGlobalUniform("camera", camera);
            ShaderManifest.SetGlobalUniform("projection", projection);

            RenderManager.Render();
            GetGLError("Scene PostDraw");
        }
    }
}
