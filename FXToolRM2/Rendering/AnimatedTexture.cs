﻿using System;
using System.Collections.Generic;
using System.IO;
using DevILSharp;
using OpenTK.Graphics.OpenGL;
using NLua;
using System.Runtime.InteropServices;
using OpenTK;
using System.Drawing;

namespace FXToolRM.Rendering
{
    public class AnimatedTexture : Texture
    {
        public class TextureFrame
        {
            public Texture Texture { get; }
            public float[] UVData { get; }

            public TextureFrame(Texture t, int r, int c, int nr, int nc)
            {
                //Vector3 v0 = rotMat * new Vector3(-scale / 2, -scale / 2, 0);
                //Vector3 v1 = rotMat * new Vector3(scale / 2, -scale / 2, 0);
                //Vector3 v2 = rotMat * new Vector3(scale / 2, scale / 2, 0);
                //Vector3 v3 = rotMat * new Vector3(-scale / 2, scale / 2, 0);
                //float[] uvs = new float[] { 0, 0, 1, 0, 1, 1, 0, 1 };
                Texture = t;
                float x0 = (float)(c + 0) / nc;
                float x1 = (float)(c + 1) / nc;
                float y0 = (float)(nr - (r + 1)) / nr;
                float y1 = (float)(nr - (r + 0)) / nr;
                UVData = new float[] {
                    x1, y0, x0, y0, x0, y1, x1, y1
                };
            }
        }

        public int FPS { get; private set; }
        public int StartFrame { get; private set; }
        public int LoopCount { get; private set; }
        public int Rows { get; private set; }
        public int Cols { get; private set; }

        private List<TextureFrame> frames = new List<TextureFrame>();

        public TextureFrame GetFrame(int frame)
        {
            if (frame < 0) frame = 0;
            if (frame / frames.Count < LoopCount) frame %= frames.Count;
            if (frame >= frames.Count) frame = frames.Count - 1;
            return frames[frame];
        }

        public AnimatedTexture(string filename)
        {
            Filename = filename;

            Lua interpreter = new Lua();
            object[] results = interpreter.DoFile(filename);
            FPS = (int)interpreter.GetNumber("fps");
            StartFrame = (int)interpreter.GetNumber("startFrame");
            LoopCount = (int)interpreter.GetNumber("loopCount");
            LuaTable layout = interpreter.GetTable("layout");

            if (layout != null)
            {
                Rows = (int)(double)layout[1];
                Cols = (int)(double)layout[2];
                LuaTable textureLayout = interpreter.GetTable("textureLayout");
                for (int i = 1; i <= textureLayout.Keys.Count; ++i)
                {
                    LuaTable entry = (LuaTable)textureLayout[i];
                    string source = (string)entry[1];

                    string dataSource = Program.GetDataPath(
                        source.ToLower()
                        .Replace(@"data:\", "data:")
                        .Replace("data:/", "data:")
                        .Replace("data:", "")
                        .Replace('/', '\\'));
                    Texture tex = LoadSubImage(dataSource, (int)(double)entry[2], (int)(double)entry[3], Rows, Cols);
                    frames.Add(new TextureFrame(tex, 0, 0, 1, 1));
                }
            }
            else
            {
                Rows = 1;
                Cols = 1;
                LuaTable texture = interpreter.GetTable("texture");
                for (int i = 1; i <= texture.Keys.Count; ++i)
                {
                    string entry = (string)texture[i];

                    string dataSource = Program.GetDataPath(
                        entry.ToLower()
                        .Replace(@"data:\", "")
                        .Replace("data:/", "")
                        .Replace("data:", "")
                        .Replace('/', '\\'));
                    Texture tex = LoadTexture(dataSource);
                    frames.Add(new TextureFrame(tex, 1, 1, Rows, Cols));
                }
            }
        }

        private static Texture LoadSubImage(string filename, int iRow, int iCol, int nRows, int nCols)
        {
            if (!ILInit) Init();

            bool flip = false;
            if (Path.GetExtension(filename).ToLower() != ".tga")
                flip = true;

            if (File.Exists(filename))
            {
                int width = 0;
                int height = 0;
                PixelFormat pixelFormat = PixelFormat.Rgba;

                int img = IL.GenImage();
                IL.BindImage(img);
                IL.LoadImage(filename);
                if (flip) ILU.FlipImage();
                IL.ConvertImage(ChannelFormat.RGBA, ChannelType.UnsignedByte);
                width = IL.GetInteger(IntName.ImageWidth);
                height = IL.GetInteger(IntName.ImageHeight);
                pixelFormat = (PixelFormat)IL.GetInteger(IntName.ImageFormat);

                int subImageWidth = width / nCols;
                int subImageHeight = height / nRows;

                byte[] data = new byte[subImageHeight * subImageWidth * 4];
                GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
                IntPtr ptr = handle.AddrOfPinnedObject();

                int x0 = width * iCol / nCols;
                int y0 = height * (nRows - (iRow + 1)) / nRows;
                int result = IL.CopyPixels(x0, y0, 0, subImageWidth, subImageHeight, 1, ChannelFormat.RGBA, ChannelType.UnsignedByte, ptr);
                handle.Free();

                IL.DeleteImage(img);
                IL.BindImage(0);

                int texID = glLoadImage(subImageWidth, subImageHeight, pixelFormat, PixelType.UnsignedByte, data);

                return new Texture(filename + "_" + iRow + "x" + iCol, texID);
            }

            return DefaultTexture;
        }

        private static int glLoadImage(int width, int height, PixelFormat pixelFormat, PixelType pixelType, byte[] data)
        {
            int texID = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texID);

            //Anisotropic filtering
            float maxAniso;
            GL.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out maxAniso);
            GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, maxAniso);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.SrgbAlpha, width, height, 0, pixelFormat, pixelType, data);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            return texID;
        }
    }
}