﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using OpenTK.Graphics.OpenGL;

namespace FXToolRM.Rendering
{
    public class EditorShader : Shader
    {
        public EditorShader() : base(
            "FXToolRM.Rendering.shaders.editor.vert",
            "FXToolRM.Rendering.shaders.editor.frag",
            true)
        { }

        protected override void DoGLOps()
        {
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.Zero);
            GL.CullFace(CullFaceMode.Back);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }
    }
}