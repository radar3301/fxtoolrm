﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK;
using System.Drawing;

namespace FXToolRM.Rendering
{
    public class Camera
    {
        public Camera() { }

        private float zoom = 2;
        private float distance = 4;
        private Vector3 rotation = Vector3.Zero;

        public Vector3 Position = new Vector3(0, 0, 4);
        public Vector3 Scale = Vector3.One;
        public Vector3 LookAt = Vector3.Zero;
        public Vector3 UpVector = Vector3.UnitY;
        public Vector3 Direction
        {
            get { return LookAt - Position; }
        }

        public Matrix4 ViewMatrix
        {
            get { return Matrix4.LookAt(Position, LookAt, UpVector); }
        }

        public Matrix3 RotationMatrix
        {
            get
            {
                return Matrix3.CreateRotationZ(rotation.Z) *
                    Matrix3.CreateRotationY(rotation.Y) *
                    Matrix3.CreateRotationX(rotation.X);
            }
        }
        
        public void CameraRotate(Vector2 delta)
        {
            rotation.X -= -delta.Y * 0.01f;
            rotation.Y -= -delta.X * 0.01f;

            rotation.X = (float)MathHelper.Clamp(rotation.X, -Math.PI / 2 + 0.01f, Math.PI / 2 - 0.01f);
            
            Position = RotationMatrix * Vector3.UnitZ * distance + LookAt;
            UpVector = RotationMatrix * Vector3.UnitY;
        }

        public void CameraZoom(float delta)
        {
            zoom -= delta * 5 * 0.1f;
            distance = (float)Math.Pow(2.0, zoom);
            if (distance < 1f)
            {
                distance = 1f;
                zoom = (float)(Math.Log(distance) / Math.Log(2));
            }

            if (distance < 1)
            {
                distance = 1;
                zoom = (float)(Math.Log(1) / Math.Log(2));
            }
            if (distance > 500)
            {
                distance = 500;
                zoom = (float)(Math.Log(500) / Math.Log(2));
            }

            Position = Vector3.Normalize(Position - LookAt) * distance + LookAt;
        }
    }
}
