﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DevILSharp;
using OpenTK.Graphics.OpenGL;

namespace FXToolRM.Rendering
{
    public class Texture
    {
        protected static bool ILInit = false;

        private static Dictionary<string, Texture> TextureLibrary = new Dictionary<string, Texture>();
        private static Texture missingTexture;
        public static Texture DefaultTexture { get { return missingTexture; } }

        private int id;
        private int width;
        private int height;

        public virtual int ID
        {
            get
            {
                return id;
            }
        }
        private string filename;
        public string Filename
        {
            get { return filename; }
            protected set
            {
                filename = value;
                DataPath = value;
                foreach (string path in Program.DataPaths)
                {
                    if (filename.StartsWith(path))
                        DataPath = Filename.Replace(path + Path.DirectorySeparatorChar, "data:").Replace('\\', '/');
                }
            }
        }
        public int Width { get { return width; } }
        public int Height { get { return height; } }

        public string DataPath { get; private set; }

        protected Texture() { }

        internal protected Texture(string filename, int id)
        {
            Filename = filename;
            this.id = id;
        }

        private Texture(string filename, bool loadAlpha = true)
        {
            Filename = filename;
            id = LoadImage(filename, loadAlpha, out width, out height);
        }

        private Texture(Stream stream, bool loadAlpha = true)
        {
            Filename = stream.ToString();
            id = LoadImage(stream, loadAlpha, out width, out height);
        }

        public static Texture LoadTexture(string filename, bool loadAlpha = true)
        {
            if (TextureLibrary.ContainsKey(filename))
                return TextureLibrary[filename];

            Texture tex = null;
            if (Path.GetExtension(filename).ToLower() == ".anim")
                tex = new AnimatedTexture(filename);
            else
                tex = new Texture(filename, loadAlpha);
            TextureLibrary.Add(filename, tex);
            return tex;
        }

        public static Texture LoadTexture(Stream stream, bool loadAlpha = true)
        {
            return new Texture(stream, loadAlpha);
        }

        public void Attach()
        {
            GL.BindTexture(TextureTarget.Texture2D, ID);
        }

        private static int RawLoadImage(int width, int height, PixelFormat pixelFormat, PixelType pixeltype, IntPtr ptr, bool loadAlpha)
        {
            int texID = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texID);

            //Anisotropic filtering
            float maxAniso;
            GL.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out maxAniso);
            GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, maxAniso);
            PixelInternalFormat pxIntFormat = loadAlpha ? PixelInternalFormat.SrgbAlpha : PixelInternalFormat.Srgb;
            GL.TexImage2D(TextureTarget.Texture2D, 0, pxIntFormat, width, height, 0, pixelFormat, pixeltype, ptr);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            return texID;
        }

        private static int LoadImage(string filename, bool loadAlpha, out int width, out int height)
        {
            if (!ILInit) Init();

            width = height = 0;

            bool exists = File.Exists(filename);
            bool flip = false;

            if (!exists)
                return DefaultTexture.ID;

            if (Path.GetExtension(filename).ToLower() != ".tga")
                flip = true;

            int img = IL.GenImage();
            IL.BindImage(img);
            IL.LoadImage(filename);

            if (flip)
                ILU.FlipImage();

            IL.ConvertImage(ChannelFormat.RGBA, ChannelType.UnsignedByte);

            width = IL.GetInteger(IntName.ImageWidth);
            height = IL.GetInteger(IntName.ImageHeight);

            int texID = RawLoadImage(width, height, (PixelFormat)IL.GetInteger(IntName.ImageFormat), PixelType.UnsignedByte, IL.GetData(), loadAlpha);

            IL.DeleteImage(img);
            IL.BindImage(0);

            return texID;
        }

        private static int LoadImage(Stream stream, bool loadAlpha, out int width, out int height)
        {
            if (!ILInit) Init();
            width = height = 0;

            bool flip = false;

            int img = IL.GenImage();
            IL.BindImage(img);
            IL.LoadStream(stream);

            if (flip)
                ILU.FlipImage();

            IL.ConvertImage(ChannelFormat.RGBA, ChannelType.UnsignedByte);

            width = IL.GetInteger(IntName.ImageWidth);
            height = IL.GetInteger(IntName.ImageHeight);

            int texID = RawLoadImage(width, height, (PixelFormat)IL.GetInteger(IntName.ImageFormat), PixelType.UnsignedByte, IL.GetData(), loadAlpha);

            IL.DeleteImage(img);
            IL.BindImage(0);

            return texID;
        }

        public static void Init()
        {
            IL.Init();
            ILInit = true;
            missingTexture = new Texture(
                Assembly.GetExecutingAssembly().GetManifestResourceStream("FXToolRM.Rendering.resources.missing.tga"));
        }

        public static void Close()
        {
            IL.ShutDown();
        }
    }
}