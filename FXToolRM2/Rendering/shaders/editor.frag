﻿#version 330

smooth in vec2 outUV;
smooth in vec4 outCol;

uniform int useTexture;
uniform sampler2D inTex;

out vec4 finalColor;

void main()
{
	vec4 vtxColor = vec4(outCol);
	vec4 texColor = vec4(1.0);

	if (useTexture == 1)
		texColor = vec4(texture(inTex, outUV).xyz, 1.0);
	
	finalColor = vtxColor * texColor;
}