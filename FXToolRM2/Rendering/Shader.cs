﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using static FXToolRM.Rendering.Utility;
using System.Reflection;

namespace FXToolRM.Rendering
{
    public class Shader
    {
        public int ProgramID = -1;
        public int VShaderID = -1;
        public int GShaderID = -1;
        public int FShaderID = -1;
        public int AttributeCount = 0;
        public int UniformCount = 0;

        public Dictionary<string, AttributeInfo> Attributes = new Dictionary<string, AttributeInfo>();
        public Dictionary<string, UniformInfo> Uniforms = new Dictionary<string, UniformInfo>();
        public Dictionary<string, uint> Buffers = new Dictionary<string, uint>();

        private string vshader;
        private string gshader;
        private string fshader;
        private bool fromFile = false;

        public Shader(string vshader, string gshader, string fshader, bool fromFile = false)
        {
            this.vshader = vshader;
            this.gshader = gshader;
            this.fshader = fshader;
            this.fromFile = fromFile;

            Reload();
        }

        public Shader(string vshader, string fshader, bool fromFile = false)
            : this(vshader, "", fshader, fromFile)
        { }

        public void Reload()
        {
            int pID, vsID, gsID, fsID;

            if (CreateProgram(out pID, out vsID, out gsID, out fsID))
            {
                Delete();
                Apply(pID, vsID, gsID, fsID);
                Link();
            }
        }

        private void Apply(int pID, int vsID, int gsID, int fsID)
        {
            ProgramID = pID;
            VShaderID = vsID;
            GShaderID = gsID;
            FShaderID = fsID;
        }

        private void Delete()
        {
            if (ProgramID == -1) return;
            if (VShaderID != 0) GL.DetachShader(ProgramID, VShaderID);
            if (GShaderID != 0) GL.DetachShader(ProgramID, GShaderID);
            if (FShaderID != 0) GL.DetachShader(ProgramID, FShaderID);
            GL.DeleteProgram(ProgramID);
            if (VShaderID != 0) GL.DeleteShader(VShaderID);
            if (GShaderID != 0) GL.DeleteShader(GShaderID);
            if (FShaderID != 0) GL.DeleteShader(FShaderID);
        }

        private int LoadShader(string code, ShaderType type)
        {
            int shaderID = GL.CreateShader(type);
            GL.ShaderSource(shaderID, code);
            GL.CompileShader(shaderID);

            int shader_ok;
            GL.GetShader(shaderID, ShaderParameter.CompileStatus, out shader_ok);

            if (shader_ok == 0)
            {
                Console.WriteLine("Failed to compile shader:");
                Console.WriteLine(GL.GetShaderInfoLog(shaderID));
                GL.DeleteShader(shaderID);
                return 0;
            }

            return shaderID;
        }

        private int LoadShaderFromString(string code, ShaderType type)
        {
            if (string.IsNullOrWhiteSpace(code)) return 0;

            int shaderID = 0;
            if (type == ShaderType.VertexShader ||
                type == ShaderType.GeometryShader ||
                type == ShaderType.FragmentShader)
                shaderID = LoadShader(code, type);
            return shaderID;
        }

        private int LoadShaderFromFile(string filename, ShaderType type)
        {
            if (string.IsNullOrWhiteSpace(filename)) return 0;

            StreamReader sr;
            if (File.Exists("shaders\\" + filename))
                sr = new StreamReader(new FileStream("shaders\\" + filename, FileMode.Open));
            else
                sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(filename));
            if (sr == null) return 0;
            using (sr)
            {
                if (type == ShaderType.VertexShader ||
                    type == ShaderType.GeometryShader ||
                    type == ShaderType.FragmentShader)
                    return LoadShader(sr.ReadToEnd(), type);
            }
            return 0;
        }

        private bool CreateProgram(out int pID, out int vsID, out int gsID, out int fsID)
        {
            pID = 0;
            vsID = 0;
            gsID = 0;
            fsID = 0;

            int programID = 0, vShaderID = 0, gShaderID = 0, fShaderID = 0;

            if (fromFile)
            {
                vShaderID = LoadShaderFromFile(vshader, ShaderType.VertexShader);
                gShaderID = LoadShaderFromFile(gshader, ShaderType.GeometryShader);
                fShaderID = LoadShaderFromFile(fshader, ShaderType.FragmentShader);
            }
            else
            {
                vShaderID = LoadShaderFromString(vshader, ShaderType.VertexShader);
                gShaderID = LoadShaderFromString(gshader, ShaderType.GeometryShader);
                fShaderID = LoadShaderFromString(fshader, ShaderType.FragmentShader);
            }
            if (vShaderID == 0) return false;
            if (fShaderID == 0) return false;

            programID = GL.CreateProgram();
            GL.AttachShader(programID, vShaderID);
            if (gShaderID != 0) GL.AttachShader(programID, gShaderID);
            GL.AttachShader(programID, fShaderID);
            GL.LinkProgram(programID);

            int program_ok;
            GL.GetProgram(programID, GetProgramParameterName.LinkStatus, out program_ok);
            if (program_ok == 0)
            {
                Console.WriteLine("Failed to link shader program:");
                Console.WriteLine(GL.GetProgramInfoLog(programID));
                GL.DeleteProgram(programID);
                return false;
            }

            pID = programID;
            vsID = vShaderID;
            gsID = gShaderID;
            fsID = fShaderID;

            return true;
        }

        public void SetUniform(string name, Matrix4 v0)
        {
            GL.UniformMatrix4(GetUniform(name), false, ref v0);
            GetGLError("Shader SetUniform");
        }

        public void SetUniform(string name, Color4 v0)
        {
            GL.Uniform4(GetUniform(name), v0);
            GetGLError("Shader SetUniform");
        }

        public void SetUniform(string name, float v0)
        {
            GL.Uniform1(GetUniform(name), v0);
            GetGLError("Shader SetUniform:" + name);
        }

        public void SetUniform(string name, float v0, float v1)
        {
            GL.Uniform2(GetUniform(name), v0, v1);
            GetGLError("Shader SetUniform");
        }

        public void SetUniform(string name, bool v0)
        {
            GL.Uniform1(GetUniform(name), v0 ? 1 : 0);
            GetGLError("Shader SetUniform");
        }

        public void SetUniform(string name, int v0)
        {
            GL.Uniform1(GetUniform(name), v0);
            GetGLError("Shader SetUniform");
        }

        public void SetUniform(string name, int v0, int v1)
        {
            GL.Uniform2(GetUniform(name), v0, v1);
            GetGLError("Shader SetUniform");
        }

        public void SetUniform(string name, float v0, float v1, float v2)
        {
            GL.Uniform3(GetUniform(name), v0, v1, v2);
            GetGLError("Shader SetUniform");
        }

        public void SetUniform(string name, ref Matrix4 mat)
        {
            GL.UniformMatrix4(GetUniform(name), false, ref mat);
            GetGLError("Shader SetUniform");
        }

        public void LinkAttrib(int buffer, string attrname, int size, bool normalized)
        {
            int attr = GetAttribute(attrname);
            if (attr == -1)
                return;
            GL.BindBuffer(BufferTarget.ArrayBuffer, buffer);
            GL.VertexAttribPointer(attr, size, VertexAttribPointerType.Float, normalized, 0, 0);
            GL.EnableVertexAttribArray(attr);
            GetGLError("Shader LinkAttrib");
        }

        private void Link()
        {
            Attributes.Clear();
            Uniforms.Clear();

            GL.GetProgram(ProgramID, GetProgramParameterName.ActiveAttributes, out AttributeCount);
            GL.GetProgram(ProgramID, GetProgramParameterName.ActiveUniforms, out UniformCount);

            for (int i = 0; i < AttributeCount; i++)
            {
                AttributeInfo info = new AttributeInfo();
                int length = 0;

                StringBuilder name = new StringBuilder(32);

                GL.GetActiveAttrib(ProgramID, i, 256, out length, out info.size, out info.type, name);

                info.name = name.ToString();
                info.address = GL.GetAttribLocation(ProgramID, info.name);
                Attributes.Add(name.ToString(), info);
            }

            for (int i = 0; i < UniformCount; i++)
            {
                UniformInfo info = new UniformInfo();
                int length = 0;

                StringBuilder name = new StringBuilder(128);

                GL.GetActiveUniform(ProgramID, i, 256, out length, out info.size, out info.type, name);

                info.name = name.ToString();
                info.address = GL.GetUniformLocation(ProgramID, info.name);
                Uniforms.Add(name.ToString(), info);
            }

            //for (int i = 0; i < Attributes.Count; i++)
            //{
            //    uint buffer = 0;
            //    GL.GenBuffers(1, out buffer);

            //    Buffers.Add(Attributes.Values.ElementAt(i).name, buffer);
            //}

            //for (int i = 0; i < Uniforms.Count; i++)
            //{
            //    uint buffer = 0;
            //    GL.GenBuffers(1, out buffer);

            //    Buffers.Add(Uniforms.Values.ElementAt(i).name, buffer);
            //}
            GetGLError("Link");
        }

        public void EnableVertexAttribArrays()
        {
            for (int i = 0; i < Attributes.Count; i++)
            {
                AttributeInfo info = Attributes.Values.ElementAt(i);
                GL.EnableVertexAttribArray(info.address);
            }
        }

        public void DisableVertexAttribArrays()
        {
            for (int i = 0; i < Attributes.Count; i++)
            {
                AttributeInfo info = Attributes.Values.ElementAt(i);
                GL.DisableVertexAttribArray(info.address);
            }
        }

        public int GetAttribute(string name)
        {
            return Attributes.ContainsKey(name) ? Attributes[name].address : -1;
        }

        public int GetUniform(string name)
        {
            return Uniforms.ContainsKey(name) ?
                Uniforms[name].address :
                -1;
        }

        public uint GetBuffer(string name)
        {
            return Buffers.ContainsKey(name) ? Buffers[name] : 0;
        }

        protected virtual void BeginProtect()
        {

        }

        protected virtual void DoGLOps()
        {

        }

        public void Draw(PrimitiveType mode, int count, DrawElementsType type, int indices)
        {
            BeginProtect();
            DoGLOps();
            GL.DrawElements(mode, count, type, indices);
            GetGLError("SurfaceLayer Draw");
            EndProtect();
        }

        protected virtual void EndProtect()
        {

        }
    }

    public class AttributeInfo
    {
        public string name = "";
        public int address = -1;
        public int size = 0;
        public ActiveAttribType type;
    }

    public class UniformInfo
    {
        public string name = "";
        public int address = -1;
        public int size = 0;
        public ActiveUniformType type;
    }
}