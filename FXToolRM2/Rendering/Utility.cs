﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace FXToolRM.Rendering
{
    static class Utility
    {
        public static void GetGLError()
        {
            ErrorCode code = GL.GetError();
            if (code != ErrorCode.NoError)
                Console.WriteLine(code);
        }

        public static void GetGLError(string type)
        {
            ErrorCode code = GL.GetError();
            if (code != ErrorCode.NoError)
                Console.WriteLine(type + ": " + code);
        }
    }
}
