﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FXToolRM.FX
{
    public enum EffectBlending
    {
        NoBlending,
        AlphaBlend,
        AdditiveBlend,
        MultiplyBlend,
        AdditiveAlphaBlend
    }

    public enum EmitterDirection
    {
        Default = 0,
        Up,
        Down
    }

    public enum ParticleDynamics
    {
        /// <summary>
        /// Particles travel at a constant velocity and trajectory for the duration of the effect.
        /// </summary>
        [Description("Particles travel at a constant velocity and trajectory for the duration of the effect.")]
        Default = 0,

        /// <summary>
        /// Particles are affected by gravity but disappear when they collide with the ground.
        /// </summary>
        [Description("Particles are affected by gravity but disappear when they collide with the ground.")]
        Gravity_GroundDisappear,

        /// <summary>
        /// Particles are affected by gravity but will bounce off the ground as well.
        /// </summary>
        [Description("Particles are affected by gravity but will bounce off the ground.")]
        Gravity_GroundBounce,

        /// <summary>
        /// Particles are affected by gravity but will stick to the ground and stay in place.
        /// </summary>
        [Description("Particles are affected by gravity but will stick to the ground and stay in place.")]
        Gravity_GroundStick,

        /// <summary>
        /// Particles are affected by wind, according to the specified wind vectors.
        /// </summary>
        [Description("Particles are affected by wind.")]
        Wind,

        /// <summary>
        /// Particles will align themselves to the surface of the water plane in the game.
        /// These will instantly appear on the water.
        /// Given a particle speed, will travel outwards from the center of the effect.
        /// </summary>
        [Description("Particles will align themselves to the surface of the water plane, and if given a speed, will travel outwards from the center of the effect.")]
        Water,

        /// <summary>
        /// All particles created with this dynamic stay local to an emitter’s orientation and translation.
        /// </summary>
        [Description("All particles created will stay local to an emitter’s orientation and translation.")]
        StayLocal,

        /// <summary>
        /// Particles emitted will align themselves to the surface of the ground plane,
        /// and if given a particle speed, will follow the contours of the landscape.
        /// </summary>
        [Description("Particles will align themselves to the surface of the ground plane, and if given a speed, will follow the contours of the landscape.")]
        StayOnGround,

        /// <summary>
        /// Particles are affected by gravity but will ignore the ground plane.
        /// </summary>
        [Description("Particles are affected by gravity but will ignore the ground plane.")]
        Gravity_IgnoreGround,

        /// <summary>
        /// Particles will align itself to the highest surface.
        /// </summary>
        [Description("Particles will align itself to the highest surface.")]
        Elevate,

        /// <summary>
        /// Particles will be attracted back on to the emitter.
        /// </summary>
        [Description("Particles will be attracted back to the emitter.")]
        EmitterAttract
    }
}
