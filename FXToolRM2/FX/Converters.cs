﻿using FXToolRM.Rendering;
using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    internal class EffectConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || sourceType.IsSubclassOf(typeof(Effect));
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value.GetType() == typeof(string))
            {
                EffectReference reference = (EffectReference)context.PropertyDescriptor.GetValue(context.Instance);
                reference.Name = (string)value;
                return reference;
            }
            throw new NotImplementedException();
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            EffectReference reference = (EffectReference)value;
            return base.GetProperties(context, reference.IsValid ? reference.Effect : null, attributes); ;
        }
    }

    internal class TextureConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || sourceType.IsSubclassOf(typeof(Texture));
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value.GetType() == typeof(string))
            {
                TextureReference reference = (TextureReference)context.PropertyDescriptor.GetValue(context.Instance);
                reference.DataPath = (string)value;
                return reference;
            }
            throw new NotImplementedException();
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            TextureReference reference = (TextureReference)value;
            return base.GetProperties(context, reference.IsValid ? reference.Texture : null, attributes); ;
        }
    }
}