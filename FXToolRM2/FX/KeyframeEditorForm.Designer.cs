﻿using System.Drawing;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    partial class KeyframeEditorForm<T> : Form
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnKeyframeRemove = new System.Windows.Forms.Button();
            this.btnKeyframeAdd = new System.Windows.Forms.Button();
            this.trkKeyframeIndex = new System.Windows.Forms.TrackBar();
            this.pgridKeyframe = new System.Windows.Forms.PropertyGrid();
            ((System.ComponentModel.ISupportInitialize)(this.trkKeyframeIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // btnKeyframeRemove
            // 
            this.btnKeyframeRemove.Location = new System.Drawing.Point(136, 64);
            this.btnKeyframeRemove.Name = "btnKeyframeRemove";
            this.btnKeyframeRemove.Size = new System.Drawing.Size(104, 24);
            this.btnKeyframeRemove.TabIndex = 39;
            this.btnKeyframeRemove.Text = "Remove Keyframe";
            this.btnKeyframeRemove.UseVisualStyleBackColor = true;
            this.btnKeyframeRemove.Click += new System.EventHandler(this.btnKeyframeRemove_Click);
            // 
            // btnKeyframeAdd
            // 
            this.btnKeyframeAdd.Location = new System.Drawing.Point(16, 64);
            this.btnKeyframeAdd.Name = "btnKeyframeAdd";
            this.btnKeyframeAdd.Size = new System.Drawing.Size(104, 24);
            this.btnKeyframeAdd.TabIndex = 38;
            this.btnKeyframeAdd.Text = "Add Keyframe";
            this.btnKeyframeAdd.UseVisualStyleBackColor = true;
            this.btnKeyframeAdd.Click += new System.EventHandler(this.btnKeyframeAdd_Click);
            // 
            // trkKeyframeIndex
            // 
            this.trkKeyframeIndex.AutoSize = false;
            this.trkKeyframeIndex.LargeChange = 1;
            this.trkKeyframeIndex.Location = new System.Drawing.Point(8, 8);
            this.trkKeyframeIndex.Maximum = 0;
            this.trkKeyframeIndex.Name = "trkKeyframeIndex";
            this.trkKeyframeIndex.Size = new System.Drawing.Size(240, 45);
            this.trkKeyframeIndex.TabIndex = 20;
            this.trkKeyframeIndex.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trkKeyframeIndex.ValueChanged += new System.EventHandler(this.trkKeyframeIndex_ValueChanged);
            // 
            // pgridKeyframe
            // 
            this.pgridKeyframe.Location = new System.Drawing.Point(8, 96);
            this.pgridKeyframe.Name = "pgridKeyframe";
            this.pgridKeyframe.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.pgridKeyframe.Size = new System.Drawing.Size(240, 152);
            this.pgridKeyframe.TabIndex = 40;
            this.pgridKeyframe.ToolbarVisible = false;
            this.pgridKeyframe.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.pgridKeyframe_PropertyValueChanged);
            // 
            // KeyframeEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(256, 256);
            this.Controls.Add(this.pgridKeyframe);
            this.Controls.Add(this.btnKeyframeRemove);
            this.Controls.Add(this.trkKeyframeIndex);
            this.Controls.Add(this.btnKeyframeAdd);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(272, 295);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(272, 295);
            this.Name = "KeyframeEditorForm";
            this.Text = "Keyframe Editor";
            ((System.ComponentModel.ISupportInitialize)(this.trkKeyframeIndex)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnKeyframeRemove;
        private System.Windows.Forms.Button btnKeyframeAdd;
        private System.Windows.Forms.TrackBar trkKeyframeIndex;
        private System.Windows.Forms.PropertyGrid pgridKeyframe;
    }
}
