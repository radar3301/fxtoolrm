﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using FXToolRM.FX;
using FXToolRM.Rendering;
using FXToolRM.Meshes;
using static FXToolRM.Utils;
using HWShaderManifest;

namespace FXToolRM
{
    public class BeamRenderer : Renderer
    {
        private BeamEffect effect;
        private BeamMesh BeamMesh;

        public override Effect Effect => effect;

        public BeamRenderer(BeamEffect e)
        {
            effect = e;
            CreationTime = RenderManager.SystemTime;
            BeamMesh = new BeamMesh();
            
            BeamMesh.Texture = e.Texture.Texture;

            e.AfterUpdate += Effect_AfterUpdate;
        }

        private void Effect_AfterUpdate(Effect effect)
        {
            BeamMesh.Texture = this.effect.Texture.Texture;
        }

        public override void Render()
        {
            float CurrentTime = (RenderManager.SystemTime - CreationTime) / effect.Duration;
            if (CurrentTime > 1)
            {
                //if (effect.Loop)
                //    CreationTime = RenderManager.SystemTime;
                //else
                    return;
            }

            Surface s = ShaderManifest.GetSurface("fx_beam_basic");

            BeamMesh.Color = effect.Color[CurrentTime];
            BeamMesh.Length = effect.Length[CurrentTime];
            BeamMesh.Width = effect.Width[CurrentTime];
            BeamMesh.Texture_U_Offset = effect.Texture_U_Offset[CurrentTime];
            BeamMesh.Texture_U_Repeat = effect.Texture_U_Repeat[CurrentTime];
            BeamMesh.Position = effect.Offset[CurrentTime] + Position;

            ApplyBlending(effect.Blending);

            if (BeamMesh.Texture is AnimatedTexture)
            {
                AnimatedTexture t = (AnimatedTexture)BeamMesh.Texture;
                BeamMesh.TextureFrame = (int)Math.Floor(t.FPS * CurrentTime);
            }

            BeamMesh.Draw();
        }

        public override void Reset()
        {

        }

        public override void Cleanup()
        {
            BeamMesh.Texture = null;
            BeamMesh = null;
        }
    }
}
