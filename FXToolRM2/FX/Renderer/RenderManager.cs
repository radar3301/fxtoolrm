﻿using HWShaderManifest;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using FXToolRM.Rendering;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace FXToolRM
{
    public static class RenderManager
    {
        private static Stopwatch DeltaCounter = new Stopwatch();
        internal static Random Random = new Random();

        private static List<Renderer> RenderList = new List<Renderer>();

        internal static float SystemTime;
        internal static float DeltaSystemTime;

        public static bool IsRunning { get; private set; }

        public static void AddRenderer(Renderer renderer)
        {
            RenderList.Add(renderer);
        }

        public static void RemoveRenderer(Renderer renderer)
        {
            RenderList.Remove(renderer);
            renderer.Cleanup();
        }

        public static void ClearRenderers()
        {
            while (RenderList.Count > 0)
                RemoveRenderer(RenderList[0]);
        }

        public static void Start()
        {
            DeltaCounter.Start();
            IsRunning = true;
        }

        public static void Stop()
        {
            DeltaCounter.Stop();
            IsRunning = false;
        }

        public static void Reset()
        {
            SystemTime = 0f;
            DeltaCounter.Stop();
            DeltaCounter.Reset();
            IsRunning = false;
            foreach (Renderer r in RenderList)
                r.Reset();
        }

        public static void Update()
        {
            DeltaCounter.Stop();
            if (IsRunning)
            { 
                DeltaSystemTime = (float)(DeltaCounter.Elapsed.TotalSeconds * (float)Program.frmMain.numSpeed.Value);
                SystemTime += DeltaSystemTime;
            }

            foreach (Renderer ps in RenderList)
                ps.Update();

            DeltaCounter.Reset();
            DeltaCounter.Start();
        }

        public static void Render()
        {
            foreach (Renderer ps in RenderList)
                ps.Render();
        }

        public static void Restart()
        {
            Reset();
            Start();
        }
    }
}
