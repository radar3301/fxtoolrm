﻿using System;
using System.Drawing;
using OpenTK.Graphics.OpenGL;
using FXToolRM.FX;
using FXToolRM.Meshes;
using HWShaderManifest;
using OpenTK.Graphics;
using FXToolRM.Rendering;
using static FXToolRM.Utils;

namespace FXToolRM
{
    public class HyperspaceRenderer : Renderer
    {
        private HyperspaceEffect effect;
        private HyperspaceWindowMesh WindowMesh;
        private HyperspaceEdgeMesh EdgeMesh;
        
        public override Effect Effect => effect;

        public HyperspaceRenderer(HyperspaceEffect e)
        {
            effect = e;
            WindowMesh = new HyperspaceWindowMesh();
            WindowMesh.Texture = e.Texture.Texture;

            EdgeMesh = new HyperspaceEdgeMesh();
            EdgeMesh.Texture = e.EdgeTexture.Texture;
            
            e.AfterUpdate += Effect_AfterUpdate;
        }

        private void Effect_AfterUpdate(Effect effect)
        {
            WindowMesh.Texture = this.effect.Texture.Texture;
            EdgeMesh.Texture = this.effect.EdgeTexture.Texture;
        }

        public override void Render()
        {
            float CurrentTime = RenderManager.SystemTime / effect.Duration;
            if (CurrentTime > 1) return;
            
            WindowMesh.Color = effect.Color[CurrentTime];
            EdgeMesh.Color = WindowMesh.Color;
            WindowMesh.Width = EdgeMesh.Width = effect.Width[CurrentTime];
            WindowMesh.Height = EdgeMesh.Height = effect.Height[CurrentTime];
            WindowMesh.Position = EdgeMesh.Position = effect.Offset[CurrentTime] + Position;

            ApplyBlending(effect.Blending);

            WindowMesh.Draw();
            float LineWidth = GL.GetFloat(GetPName.LineWidth);
            GL.LineWidth(Math.Max(effect.LineWidth[CurrentTime], 0.001f));
            EdgeMesh.Draw();
            GL.LineWidth(LineWidth);
        }

        public override void Reset()
        {

        }

        public override void Cleanup()
        {

        }
    }
}
