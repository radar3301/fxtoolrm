﻿using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using FXToolRM.FX;
using FXToolRM.Rendering;
using static FXToolRM.Utils;

namespace FXToolRM
{
    public class SprayRenderer : Renderer
    {
        private SprayEffect effect;
        private Effect ParticleEffect;
        private List<Renderer> RenderList = new List<Renderer>();

        public override Effect Effect => effect;

        public SprayRenderer(SprayEffect e)
        {
            effect = e;
            effect.BeforeUpdate += Effect_BeforeUpdate;
            effect.AfterUpdate += Effect_AfterUpdate;

            CreationTime = RenderManager.SystemTime;
            ParticleEffect = effect.ParticleFX.Effect;
        }

        private void Effect_BeforeUpdate(Effect effect)
        {
            for (int i = RenderList.Count - 1; i >= 0; --i)
            {
                if (RenderList[i].Effect == effect)
                    RenderList.RemoveAt(i);
            }
        }

        private void Effect_AfterUpdate(Effect effect)
        {
            ParticleEffect = effect;
        }

        public override void Render()
        {
            foreach (Renderer r in RenderList)
                r.Render();
        }

        private float LastEmit = 0f;

        public override void Reset()
        {
            LastEmit = 0f;
            Cleanup();
        }

        public override void Update()
        {
            base.Update();

            float CurrentTime = (RenderManager.SystemTime - CreationTime) / effect.EmitterDuration;
            if (CurrentTime > 1) return;

            float rate = 1 / effect.EmitterRate[CurrentTime];
            if (CurrentTime - LastEmit >= rate)
            {
                float delta = CurrentTime - LastEmit;
                if (delta > rate)
                {
                    RenderManager.SystemTime = LastEmit * effect.EmitterDuration + CreationTime;
                    for (float k = LastEmit + rate; k < CurrentTime;  k += rate)
                    {
                        Renderer r = GetRenderer(ParticleEffect);
                        if (r != null)
                        {
                            RenderList.Add(r);
                            r.Position = effect.EmitterOffset[k];

                            Vector3 dir = Vector3.Zero;
                            if (effect.EmitterDirection == EmitterDirection.Default)
                                dir = Vector3.UnitZ;
                            else if (effect.EmitterDirection == EmitterDirection.Up)
                                dir = Vector3.UnitY;
                            else if (effect.EmitterDirection == EmitterDirection.Down)
                                dir = -Vector3.UnitY;

                            r.Velocity = dir * effect.ParticleSpeed[k];
                            r.Drag = effect.EmitterDrag;
                        }
                        LastEmit = k;
                        RenderManager.SystemTime = LastEmit * effect.EmitterDuration + CreationTime;
                    }
                    RenderManager.SystemTime = CurrentTime * effect.EmitterDuration + CreationTime;
                }
                else
                {
                    Renderer r = GetRenderer(ParticleEffect);
                    if (r != null)
                    {
                        RenderList.Add(r);
                        r.Position = effect.EmitterOffset[CurrentTime];

                        Vector3 dir = Vector3.Zero;
                        if (effect.EmitterDirection == EmitterDirection.Default)
                            dir = Vector3.UnitZ;
                        else if (effect.EmitterDirection == EmitterDirection.Up)
                            dir = Vector3.UnitY;
                        else if (effect.EmitterDirection == EmitterDirection.Down)
                            dir = -Vector3.UnitY;

                        r.Velocity = dir * effect.ParticleSpeed[CurrentTime];
                        r.Drag = effect.EmitterDrag;
                    }
                    LastEmit = CurrentTime;
                }
            }

            foreach (Renderer r in RenderList)
                r.Update();
        }

        public override void Cleanup()
        {
            foreach (Renderer r in RenderList)
                r.Cleanup();
            RenderList.Clear();
        }
    }
}
