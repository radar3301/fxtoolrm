﻿using System;
using System.Drawing;
using OpenTK.Graphics.OpenGL;
using FXToolRM.FX;
using FXToolRM.Rendering;

namespace FXToolRM
{
    public class TrailRenderer : Renderer
    {
        public TrailEffect effect;

        public override Effect Effect => effect;

        public TrailRenderer(TrailEffect e)
        {
            effect = e;

            //e.AfterUpdate += Effect_AfterUpdate;
        }

        //private void Effect_AfterUpdate(Effect effect)
        //{
        //    RingMesh.Texture = this.effect.Texture.Texture;
        //}

        public override void Render()
        {
            float CurrentTime = RenderManager.SystemTime / effect.Duration;
        }

        public override void Reset()
        {

        }

        public override void Cleanup()
        {
        }
    }
}
