﻿using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using FXToolRM.FX;
using FXToolRM.Rendering;

namespace FXToolRM
{
    public class ComboRenderer : Renderer
    {
        private ComboEffect effect;
        private List<Renderer> RenderList = new List<Renderer>();

        public override Effect Effect => effect;

        public ComboRenderer(ComboEffect e)
        {
            effect = e;
            CreationTime = RenderManager.SystemTime;

            Renderer r;
            r = Utils.GetRenderer(effect.Fx1.Effect); if (r != null) RenderList.Add(r);
            r = Utils.GetRenderer(effect.Fx2.Effect); if (r != null) RenderList.Add(r);
            r = Utils.GetRenderer(effect.Fx3.Effect); if (r != null) RenderList.Add(r);
            r = Utils.GetRenderer(effect.Fx4.Effect); if (r != null) RenderList.Add(r);
            r = Utils.GetRenderer(effect.Fx5.Effect); if (r != null) RenderList.Add(r);
            r = Utils.GetRenderer(effect.Fx6.Effect); if (r != null) RenderList.Add(r);
            r = Utils.GetRenderer(effect.Fx7.Effect); if (r != null) RenderList.Add(r);
            r = Utils.GetRenderer(effect.Fx8.Effect); if (r != null) RenderList.Add(r);

            e.BeforeUpdate += Effect_BeforeUpdate;
            e.AfterUpdate += Effect_AfterUpdate;
        }

        private void Effect_BeforeUpdate(Effect effect)
        {
            for (int i = RenderList.Count - 1; i >= 0; --i)
            {
                if (RenderList[i].Effect == effect)
                    RenderList.RemoveAt(i);
            }
        }

        private void Effect_AfterUpdate(Effect effect)
        {
            Renderer r = Utils.GetRenderer(effect);
            if (r != null)
                RenderList.Add(r);
        }

        public override void Render()
        {
            foreach (Renderer r in RenderList)
                r.Render();
        }
        
        public override void Reset()
        {
            foreach (Renderer r in RenderList)
                r.Reset();
        }

        public override void Update()
        {
            base.Update();
            foreach (Renderer r in RenderList)
            {
                r.Position = Position;
                r.Update();
            }
        }

        public override void Cleanup()
        {
            foreach (Renderer r in RenderList)
                r.Cleanup();
        }
    }
}
