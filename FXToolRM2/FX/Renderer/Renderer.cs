﻿using FXToolRM.FX;
using OpenTK;

namespace FXToolRM
{
    public abstract class Renderer
    {
        public abstract Effect Effect { get; }

        public abstract void Reset();
        public virtual void Update()
        {
            Position += Velocity * RenderManager.DeltaSystemTime;
            Velocity *= Drag * RenderManager.DeltaSystemTime;
        }
        public abstract void Render();
        public abstract void Cleanup();

        public float CreationTime { get; set; } = 0;
        public Vector3 Position { get; set; } = Vector3.Zero;
        public Vector3 Velocity { get; set; } = Vector3.Zero;
        public float Drag { get; set; } = 1;
    }
}
