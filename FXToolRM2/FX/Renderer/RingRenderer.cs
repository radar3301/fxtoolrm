﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using FXToolRM.FX;
using FXToolRM.Rendering;
using FXToolRM.Meshes;
using static FXToolRM.Utils;
using HWShaderManifest;

namespace FXToolRM
{
    public class RingRenderer : Renderer
    {
        private RingEffect effect;
        private RingMesh RingMesh;

        public override Effect Effect => effect;

        public RingRenderer(RingEffect e)
        {
            effect = e;
            CreationTime = RenderManager.SystemTime;
            RingMesh = new RingMesh();
            
            RingMesh.Texture = e.Texture.Texture;
            RingMesh.Billboard = effect.Billboard;
            RingMesh.DepthOffset = effect.DepthOffset;
            
            e.AfterUpdate += Effect_AfterUpdate;
        }

        private void Effect_AfterUpdate(Effect effect)
        {
            RingMesh.Texture = this.effect.Texture.Texture;
        }

        public override void Render()
        {
            float CurrentTime = (RenderManager.SystemTime - CreationTime) / effect.Duration;
            if (CurrentTime > 1)
            {
                //if (effect.Loop)
                //    CreationTime = RenderManager.SystemTime;
                //else
                    return;
            }

            Surface s = ShaderManifest.GetSurface("fx_ring_basic");

            RingMesh.Billboard = effect.Billboard;
            RingMesh.Scale = effect.Radius[CurrentTime];
            RingMesh.Color = effect.Colour[CurrentTime];
            RingMesh.Position = effect.Offset[CurrentTime] + Position;
            RingMesh.Rotation = new Vector3(
                MathHelper.DegreesToRadians(Integrate(CurrentTime, effect.SpinX)),
                MathHelper.DegreesToRadians(Integrate(CurrentTime, effect.SpinY)),
                MathHelper.DegreesToRadians(Integrate(CurrentTime, effect.SpinZ)));
            
            ApplyBlending(effect.Blending);

            if (RingMesh.Texture is AnimatedTexture)
            {
                AnimatedTexture t = (AnimatedTexture)RingMesh.Texture;
                RingMesh.TextureFrame = (int)Math.Floor(t.FPS * CurrentTime);
            }

            RingMesh.Draw();
        }

        public override void Reset()
        {

        }

        public override void Cleanup()
        {
            RingMesh.Texture = null;
            RingMesh = null;
        }
    }
}
