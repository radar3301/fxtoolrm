﻿using NLua;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;

namespace FXToolRM.FX
{
    internal class EffectProperties : Dictionary<string, dynamic>
    {
        public EffectProperties(LuaTable table) : base()
        {
            if (table == null) return;
            foreach (string key in table.Keys)
            {
                LuaTable property = (LuaTable)table[key];
                string name = ((string)property["name"]).ToLower();
                string type = ((string)property["type"]).ToUpper();
                object value = property["value"];

                if (type == "VARTYPE_FLOAT")
                    this[name] = (float)(double)value;
                else if (type == "VARTYPE_BOOL")
                    this[name] = (double)value != 0;
                else if (type == "VARTYPE_INT")
                    this[name] = (int)(double)value;
                else if (type == "VARTYPE_STRING")
                    this[name] = (string)value;
                else if (type == "VARTYPE_COLOUR")
                    this[name] = ParseColor((LuaTable)value);
                else if (type == "VARTYPE_VECTOR3")
                    this[name] = ParseVector((LuaTable)value);
                else if (type == "VARTYPE_ARRAY_TIMEFLOAT")
                    this[name] = ParseTimeFloatArray((LuaTable)value);
                else if (type == "VARTYPE_ARRAY_TIMECOLOUR")
                    this[name] = ParseTimeColorArray((LuaTable)value);
                else if (type == "VARTYPE_ARRAY_TIMEVECTOR3")
                    this[name] = ParseTimeVectorArray((LuaTable)value);
                else
                    throw new NotImplementedException(type);
            }
        }

        private Color ParseColor(LuaTable table)
        {
            byte r = (byte)((double)table[1] * 255);
            byte g = (byte)((double)table[2] * 255);
            byte b = (byte)((double)table[3] * 255);
            byte a = (byte)((double)table[4] * 255);

            return Color.FromArgb(a, r, g, b);
        }

        private Vector3 ParseVector(LuaTable table)
        {
            float x = (float)(double)table[1];
            float y = (float)(double)table[2];
            float z = (float)(double)table[3];

            return new Vector3(x, y, z);
        }

        private FloatKeyframes ParseTimeFloatArray(LuaTable table)
        {
            FloatKeyframes keyframes = new FloatKeyframes();

            for (int i = 1; i <= table.Keys.Count; i += 2)
            {
                keyframes.Add((float)(double)table[i], (float)(double)table[i + 1]);
            }

            return keyframes;
        }

        private ColorKeyframes ParseTimeColorArray(LuaTable table)
        {
            ColorKeyframes keyframes = new ColorKeyframes();

            foreach (string key in table.Keys)
            {
                LuaTable entry = (LuaTable)table[key];
                float time = (float)(double)entry[1];
                byte r = (byte)((double)entry[2] * 255);
                byte g = (byte)((double)entry[3] * 255);
                byte b = (byte)((double)entry[4] * 255);
                byte a = (byte)((double)entry[5] * 255);
                keyframes.Add(time, Color.FromArgb(a, r, g, b));
            }

            return keyframes;
        }

        private VectorKeyframes ParseTimeVectorArray(LuaTable table)
        {
            VectorKeyframes keyframes = new VectorKeyframes();

            foreach (string key in table.Keys)
            {
                LuaTable entry = (LuaTable)table[key];
                float time = (float)(double)entry[1];
                float x = (float)(double)entry[2];
                float y = (float)(double)entry[3];
                float z = (float)(double)entry[4];
                keyframes.Add(time, new Vector3(x, y, z));
            }

            return keyframes;
        }
    }
}