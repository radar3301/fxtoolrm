﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLua;
using System.Windows.Forms;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using System.ComponentModel;
using System.Globalization;
using System.Collections;

namespace FXToolRM.FX
{
    public delegate void EffectUpdate(Effect effect);

    public abstract class Effect
    {
        public string Filename { get; internal set; }
        public abstract EffectStyle Style { get; }
        public string Name { get; }
        internal EffectProperties Properties;

        protected Effect(string filename, string name, LuaTable table)
        {
            Filename = filename;
            Name = name;
            Properties = new EffectProperties(table);
        }

        public static Effect Load(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename)) return null;
            if (!File.Exists(filename))
                return null;

            string name = Path.GetFileNameWithoutExtension(filename).ToLower();

            Lua interpreter = new Lua();
            object[] results = interpreter.DoFile(filename);
            LuaTable fx = interpreter.GetTable("fx");
            string style = (string)fx["style"];
            LuaTable properties = (LuaTable)fx["properties"];

            if (style == "STYLE_BEAM")
                return new BeamEffect(filename, name, properties);
            else if (style == "STYLE_COMBO")
                return new ComboEffect(filename, name, properties);
            else if (style == "STYLE_HYPERSPACE")
                return new HyperspaceEffect(filename, name, properties);
            else if (style == "STYLE_LENSFLARE")
                return new LensFlareEffect(filename, name, properties);
            else if (style == "STYLE_LIGHT")
                return new LightEffect(filename, name, properties);
            else if (style == "STYLE_RING")
                return new RingEffect(filename, name, properties);
            else if (style == "STYLE_SPRAY")
                return new SprayEffect(filename, name, properties);
            else if (style == "STYLE_TRAIL")
                return new TrailEffect(filename, name, properties);

            return new InvalidEffect(name, properties);
        }
        protected T GetPropertyValue<T>(string name, T defaultValue)
        {
            name = name.ToLower();
            if (Properties.ContainsKey(name))
                return Properties[name];
            return defaultValue;
        }

        protected T GetPropertyValue<T>(string name) where T : new()
        {
            name = name.ToLower();
            if (Properties.ContainsKey(name))
                return Properties[name];
            return new T();
        }

        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Saves the effect.
        /// </summary>
        public void Save()
        {
            if (Filename == "")
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.FileName = "";
                if (sfd.ShowDialog() != DialogResult.OK) return;
                Filename = sfd.FileName;
            }
            Save(Filename);
        }

        /// <summary>
        /// Saves the effect to the specified location.
        /// </summary>
        public void Save(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("fx = {");
            sw.WriteLine("\tstyle = \"STYLE_" + Style.ToString().ToUpper() + "\",");
            sw.WriteLine("\tproperties = {");
            WriteProperties(sw);
            sw.WriteLine("\t}");
            sw.WriteLine("}");
            sw.Flush();
            sw.Close();
            sw.Dispose();
            fs.Close();
            fs.Dispose();
            sw = null;
            fs = null;
        }

        protected abstract void WriteProperties(StreamWriter sw);

        internal void WriteProperty(StreamWriter sw, int idx, string name, object value)
        {
            string type;
            if (value is EffectReference) value = ((EffectReference)value).Name;
            if (value is TextureReference) value = ((TextureReference)value).DataPath;
            if (value is float) type = "VARTYPE_FLOAT";
            else if (value is bool) type = "VARTYPE_BOOL";
            else if (value is int) type = "VARTYPE_INT";
            else if (value is string) type = "VARTYPE_STRING";
            else if (value is Color) type = "VARTYPE_COLOUR";
            else if (value is Vector3) type = "VARTYPE_VECTOR3";
            else if (value is FloatKeyframes) type = "VARTYPE_ARRAY_TIMEFLOAT";
            else if (value is ColorKeyframes) type = "VARTYPE_ARRAY_TIMECOLOUR";
            else if (value is VectorKeyframes) type = "VARTYPE_ARRAY_TIMEVECTOR3";
            else return;

            sw.WriteLine("\t\tproperty_" + idx.ToString("D2") + " = {");

            sw.WriteLine("\t\t\tname = \"" + name + "\",");
            sw.WriteLine("\t\t\ttype = \"" + type + "\",");
            switch (type)
            {
                case "VARTYPE_BOOL":
                    sw.WriteLine("\t\t\tvalue = " + ((bool)value ? "1" : "0") + ",");
                    break;
                case "VARTYPE_COLOUR":
                    sw.Write("\t\t\tvalue = { ");
                    WriteColor(sw, (Color)value);
                    sw.WriteLine(" },");
                    break;
                case "VARTYPE_FLOAT":
                    sw.WriteLine("\t\t\tvalue = " + value.ToString() + ",");
                    break;
                case "VARTYPE_INT":
                    sw.WriteLine("\t\t\tvalue = " + value.ToString() + ",");
                    break;
                case "VARTYPE_STRING":
                    sw.WriteLine("\t\t\tvalue = \"" + value.ToString() + "\",");
                    break;
                case "VARTYPE_VECTOR3":
                    sw.Write("\t\t\tvalue = { ");
                    WriteVector(sw, (Vector3)value);
                    sw.WriteLine(" },");
                    break;
                case "VARTYPE_ARRAY_TIMEFLOAT":
                    WriteTimeFloatArray(sw, (FloatKeyframes)value);
                    break;
                case "VARTYPE_ARRAY_TIMECOLOUR":
                    WriteTimeColorArray(sw, (ColorKeyframes)value);
                    break;
                case "VARTYPE_ARRAY_TIMEVECTOR3":
                    WriteTimeVectorArray(sw, (VectorKeyframes)value);
                    break;
            }
            sw.WriteLine("\t\t},");
        }

        private void WriteColor(StreamWriter sw, Color4 value)
        {
            sw.Write(value.R + ", " + value.G + ", " + value.B + ", " + value.A);
        }

        private void WriteVector(StreamWriter sw, Vector3 value)
        {
            sw.Write(value.X + ", " + value.Y + ", " + value.Z);
        }

        private void WriteTimeFloatArray(StreamWriter sw, FloatKeyframes value)
        {
            sw.WriteLine("\t\t\tvalue = {");
            if (value.Count == 0)
            {
                value.Add(0, 0);
                value.Add(1, 0);
            }
            for (int i = 0; i < value.Count; i++)
                sw.WriteLine("\t\t\t\t" + value.GetTimeAtIndex(i) + ", " + value.GetValueAtIndex(i) + ",");
            sw.WriteLine("\t\t\t},");
        }

        private void WriteTimeColorArray(StreamWriter sw, ColorKeyframes value)
        {
            sw.WriteLine("\t\t\tvalue = {");
            if (value.Count == 0)
            {
                value.Add(0, Color.White);
                value.Add(1, Color.White);
            }
            for (int i = 0; i < value.Count; ++i)
            {
                sw.Write("\t\t\t\tentry_" + i.ToString("D2") + " = { ");
                sw.Write(value.GetTimeAtIndex(i) + ", ");
                WriteColor(sw, value.GetValueAtIndex(i));
                sw.WriteLine(" },");
            }
            sw.WriteLine("\t\t\t},");
        }

        private void WriteTimeVectorArray(StreamWriter sw, VectorKeyframes value)
        {
            sw.WriteLine("\t\t\tvalue = {");
            if (value.Count == 0)
            {
                value.Add(0, Vector3.Zero);
                value.Add(1, Vector3.Zero);
            }
            for (int i = 0; i < value.Count; ++i)
            {
                sw.Write("\t\t\t\tentry_" + i.ToString("D2") + " = { ");
                sw.Write(value.GetTimeAtIndex(i) + ", ");
                WriteVector(sw, value.GetValueAtIndex(i));
                sw.WriteLine(" },");
            }
            sw.WriteLine("\t\t\t},");
        }
    }

    public class InvalidEffect : Effect
    {
        public override EffectStyle Style { get; } = EffectStyle.Invalid;

        internal protected InvalidEffect(string name, LuaTable table) : base("", name, table) { }

        public new void Save(string filename)
        {
            MessageBox.Show("You cannot save an invalid effect.");
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            throw new NotImplementedException();
        }
    }
}
