﻿using NLua;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System;
using System.IO;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    public class RingEffect : Effect
    {
        public event EffectUpdate BeforeUpdate;
        public event EffectUpdate AfterUpdate;

        public override EffectStyle Style { get; } = EffectStyle.Ring;

        public bool AlternateFOV { get; set; }
        public bool Billboard { get; set; }
        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference BirthspawnFX { get; set; } = new EffectReference();
        public EffectBlending Blending { get; set; }
        public ColorKeyframes Colour { get; }
        public float DepthOffset { get; set; }
        public float Duration { get; set; }
        public bool Loop { get; set; }
        public string Mesh { get; set; }
        public VectorKeyframes Offset { get; }
        public float PointDistance { get; set; }
        public FloatKeyframes Radius { get; }
        public float RadiusEpsilon { get; set; }
        public bool RandomBitmap { get; set; }
        public float RateEpsilon { get; set; }
        public bool SelfIlluminated { get; set; }
        public int SortOrder { get; set; }
        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference SpawnFX { get; set; } = new EffectReference();
        public FloatKeyframes SpinX { get; }
        public FloatKeyframes SpinY { get; }
        public FloatKeyframes SpinZ { get; }
        public float SpinEpsilonX { get; set; }
        public float SpinEpsilonY { get; set; }
        public float SpinEpsilonZ { get; set; }
        public bool SpinRandom { get; set; }
        [Editor(typeof(TextureReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(TextureConverter))]
        public TextureReference Texture { get; set; } = new TextureReference();
        public bool UseDepthSort { get; set; }
        public bool UseDepthTest { get; set; }
        public bool UseMesh { get; set; }

        internal protected RingEffect(string filename, string name, LuaTable table) : base(filename, name, table)
        {
            AlternateFOV = GetPropertyValue("AlternateFOV", false);
            Billboard = GetPropertyValue("Billboard", false);
            BirthspawnFX.Name = GetPropertyValue("Birthspawn_FX", "");
            Blending = (EffectBlending)GetPropertyValue("Blending", 0);
            Colour = GetPropertyValue("Colour", new ColorKeyframes());
            DepthOffset = GetPropertyValue("DepthOffset", 0f);
            Duration = GetPropertyValue("Duration", 0f);
            Loop = GetPropertyValue("Loop", false);
            Mesh = GetPropertyValue("Mesh", "");
            Offset = GetPropertyValue("Offset", new VectorKeyframes());
            PointDistance = GetPropertyValue("PointDistance", 0f);
            Radius = GetPropertyValue("Radius", new FloatKeyframes());
            RadiusEpsilon = GetPropertyValue("RadiusEpsilon", 0f);
            RandomBitmap = GetPropertyValue("RandomBitmap", false);
            RateEpsilon = GetPropertyValue("RateEpsilon", 0f);
            SelfIlluminated = GetPropertyValue("SelfIlluminated", false);
            SortOrder = GetPropertyValue("SortOrder", 0);
            SpawnFX.Name = GetPropertyValue("Spawn_FX", "");
            SpinX = GetPropertyValue("SpinX", new FloatKeyframes());
            SpinY = GetPropertyValue("SpinY", new FloatKeyframes());
            SpinZ = GetPropertyValue("SpinZ", new FloatKeyframes());
            float SpinEpsilon = GetPropertyValue("SpinEpsilon", 0f);
            SpinEpsilonX = GetPropertyValue("SpinEpsilonX", SpinEpsilon);
            SpinEpsilonY = GetPropertyValue("SpinEpsilonY", SpinEpsilon);
            SpinEpsilonZ = GetPropertyValue("SpinEpsilonZ", SpinEpsilon);
            SpinRandom = GetPropertyValue("SpinRandom", false);
            Texture.DataPath = GetPropertyValue("Texture", "");
            UseDepthSort = GetPropertyValue("UseDepthSort", false);
            UseDepthTest = GetPropertyValue("UseDepthTest", false);
            UseMesh = GetPropertyValue("UseMesh", false);

            Texture.BeforeUpdate += TextureReference_BeforeUpdate;
            Texture.AfterUpdate += TextureReference_AfterUpdate;
        }

        private void TextureReference_BeforeUpdate(TextureReference reference)
        {
            if (BeforeUpdate != null) BeforeUpdate.Invoke(this);
        }

        private void TextureReference_AfterUpdate(TextureReference reference)
        {
            if (!reference.IsValid)
                MessageBox.Show("The referenced texture \"" + reference.DataPath + "\" does not exist in the current data paths.");
            if (AfterUpdate != null) AfterUpdate.Invoke(this);
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            int idx = 0;
            WriteProperty(sw, idx++, "Radius", Radius);
            WriteProperty(sw, idx++, "Offset", Offset);
            WriteProperty(sw, idx++, "SpinX", SpinX);
            WriteProperty(sw, idx++, "SpinY", SpinY);
            WriteProperty(sw, idx++, "SpinZ", SpinZ);
            WriteProperty(sw, idx++, "Colour", Colour);
            WriteProperty(sw, idx++, "RadiusEpsilon", RadiusEpsilon);
            WriteProperty(sw, idx++, "RateEpsilon", RateEpsilon);
            WriteProperty(sw, idx++, "SpinEpsilonX", SpinEpsilonX);
            WriteProperty(sw, idx++, "SpinEpsilonY", SpinEpsilonY);
            WriteProperty(sw, idx++, "SpinEpsilonZ", SpinEpsilonZ);
            WriteProperty(sw, idx++, "SpinRandom", SpinRandom);
            WriteProperty(sw, idx++, "Duration", Duration);
            WriteProperty(sw, idx++, "UseMesh", UseMesh);
            WriteProperty(sw, idx++, "UseDepthTest", UseDepthTest);
            WriteProperty(sw, idx++, "UseDepthSort", UseDepthSort);
            WriteProperty(sw, idx++, "Loop", Loop);
            WriteProperty(sw, idx++, "AlternateFOV", AlternateFOV);
            WriteProperty(sw, idx++, "Billboard", Billboard);
            WriteProperty(sw, idx++, "SelfIlluminated", SelfIlluminated);
            WriteProperty(sw, idx++, "Blending", (int)Blending);
            WriteProperty(sw, idx++, "Texture", Texture);
            WriteProperty(sw, idx++, "Mesh", Mesh);
            WriteProperty(sw, idx++, "Spawn_Fx", SpawnFX.Name);
            WriteProperty(sw, idx++, "BirthSpawn_Fx", BirthspawnFX.Name);
            WriteProperty(sw, idx++, "RandomBitmap", RandomBitmap);
            WriteProperty(sw, idx++, "SortOrder", SortOrder);
            WriteProperty(sw, idx++, "DepthOffset", DepthOffset);
            WriteProperty(sw, idx++, "PointDistance", PointDistance);
        }
    }
}