﻿using System;
using System.IO;
using NLua;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    public class ComboEffect : Effect
    {
        public event EffectUpdate BeforeUpdate;
        public event EffectUpdate AfterUpdate;

        public override EffectStyle Style { get; } = EffectStyle.Combo;
        
        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference Fx1 { get; set; } = new EffectReference();

        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference Fx2 { get; set; } = new EffectReference();

        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference Fx3 { get; set; } = new EffectReference();

        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference Fx4 { get; set; } = new EffectReference();

        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference Fx5 { get; set; } = new EffectReference();

        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference Fx6 { get; set; } = new EffectReference();

        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference Fx7 { get; set; } = new EffectReference();

        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference Fx8 { get; set; } = new EffectReference();

        internal protected ComboEffect(string filename, string name, LuaTable table) : base(filename, name, table)
        {
            Fx1.Name = GetPropertyValue("fx1", "");
            Fx2.Name = GetPropertyValue("fx2", "");
            Fx3.Name = GetPropertyValue("fx3", "");
            Fx4.Name = GetPropertyValue("fx4", "");
            Fx5.Name = GetPropertyValue("fx5", "");
            Fx6.Name = GetPropertyValue("fx6", "");
            Fx7.Name = GetPropertyValue("fx7", "");
            Fx8.Name = GetPropertyValue("fx8", "");

            Fx1.BeforeUpdate += Reference_BeforeUpdate;
            Fx2.BeforeUpdate += Reference_BeforeUpdate;
            Fx3.BeforeUpdate += Reference_BeforeUpdate;
            Fx4.BeforeUpdate += Reference_BeforeUpdate;
            Fx5.BeforeUpdate += Reference_BeforeUpdate;
            Fx6.BeforeUpdate += Reference_BeforeUpdate;
            Fx7.BeforeUpdate += Reference_BeforeUpdate;
            Fx8.BeforeUpdate += Reference_BeforeUpdate;

            Fx1.AfterUpdate += Reference_AfterUpdate;
            Fx2.AfterUpdate += Reference_AfterUpdate;
            Fx3.AfterUpdate += Reference_AfterUpdate;
            Fx4.AfterUpdate += Reference_AfterUpdate;
            Fx5.AfterUpdate += Reference_AfterUpdate;
            Fx6.AfterUpdate += Reference_AfterUpdate;
            Fx7.AfterUpdate += Reference_AfterUpdate;
            Fx8.AfterUpdate += Reference_AfterUpdate;
        }

        private void Reference_BeforeUpdate(EffectReference reference)
        {
            if (BeforeUpdate != null) BeforeUpdate.Invoke(reference.Effect);
        }

        private void Reference_AfterUpdate(EffectReference reference)
        {
            if (!reference.IsValid)
                MessageBox.Show("The referenced effect \"" + reference.Name + "\" does not exist in the current data paths.");
            if (AfterUpdate != null) AfterUpdate.Invoke(reference.Effect);
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            int idx = 0;
            WriteProperty(sw, idx++, "Fx1", Fx1.Name);
            WriteProperty(sw, idx++, "Fx2", Fx2.Name);
            WriteProperty(sw, idx++, "Fx3", Fx3.Name);
            WriteProperty(sw, idx++, "Fx4", Fx4.Name);
            WriteProperty(sw, idx++, "Fx5", Fx5.Name);
            WriteProperty(sw, idx++, "Fx6", Fx6.Name);
            WriteProperty(sw, idx++, "Fx7", Fx7.Name);
            WriteProperty(sw, idx++, "Fx8", Fx8.Name);
        }
    }
}
