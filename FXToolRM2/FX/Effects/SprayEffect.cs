﻿using System;
using System.IO;
using NLua;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    public class SprayEffect : Effect
    {
        public event EffectUpdate BeforeUpdate;
        public event EffectUpdate AfterUpdate;

        public override EffectStyle Style { get; } = EffectStyle.Spray;

        public FloatKeyframes EmitterDeviation { get; }
        public EmitterDirection EmitterDirection { get; set; }
        public float EmitterDrag { get; set; }
        public float EmitterDuration { get; set; }
        public float EmitterInheritVelocity { get; set; }
        public bool EmitterLoop { get; set; }
        public VectorKeyframes EmitterOffset { get; }
        public FloatKeyframes EmitterRate { get; }
        public FloatKeyframes EmitterRateLODPercent { get; }
        public FloatKeyframes EmitterRotationRate { get; }
        public bool EmitterScaleSpeed { get; set; }
        public VectorKeyframes EmitterVolume { get; }
        public FloatKeyframes GravwellStrength { get; }
        public FloatKeyframes ParticleScale { get; }
        public ParticleDynamics ParticleDynamics { get; set; }

        [Editor(typeof(EffectReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(EffectConverter))]
        public EffectReference ParticleFX { get; set; } = new EffectReference();

        public FloatKeyframes ParticleSpeed { get; set; }
        public bool ForceFirst { get; set; }

        internal protected SprayEffect(string filename, string name, LuaTable table) : base(filename, name, table)
        {
            EmitterDeviation = GetPropertyValue("emitter_deviation", new FloatKeyframes());
            EmitterDirection = (EmitterDirection)GetPropertyValue("emitter_direction", 0);
            EmitterDrag = GetPropertyValue("emitter_drag", 0f);
            EmitterDuration = GetPropertyValue("emitter_duration", 0f);
            EmitterInheritVelocity = GetPropertyValue("emitter_inheritvelocity", 0f);
            EmitterLoop = GetPropertyValue("emitter_loop", false);
            EmitterOffset = GetPropertyValue("emitter_offset", new VectorKeyframes());
            EmitterRate = GetPropertyValue("emitter_rate", new FloatKeyframes());
            EmitterRateLODPercent = GetPropertyValue("emitter_rate_lod%", new FloatKeyframes());
            EmitterRotationRate = GetPropertyValue("emitter_rotrate", new FloatKeyframes());
            EmitterScaleSpeed = GetPropertyValue("emitter_scalespeed", false);
            EmitterVolume = GetPropertyValue("emitter_volume", new VectorKeyframes());
            GravwellStrength = GetPropertyValue("gravwell_strength", new FloatKeyframes());
            ParticleScale = GetPropertyValue("particle_scale", new FloatKeyframes());
            ParticleDynamics = (ParticleDynamics)GetPropertyValue("particle_dynamics", 0);
            ParticleFX.Name = GetPropertyValue("particle_fx", "");
            ParticleSpeed = GetPropertyValue("particle_speed", new FloatKeyframes());
            ForceFirst = GetPropertyValue("force_first", false);

            ParticleFX.BeforeUpdate += EffectReference_BeforeUpdate;
            ParticleFX.AfterUpdate += EffectReference_AfterUpdate;
        }

        private void EffectReference_BeforeUpdate(EffectReference reference)
        {
            if (BeforeUpdate != null) BeforeUpdate.Invoke(reference.Effect);
        }

        private void EffectReference_AfterUpdate(EffectReference reference)
        {
            if (!reference.IsValid)
                MessageBox.Show("The referenced effect \"" + reference.Name + "\" does not exist in the current data paths.");
            if (AfterUpdate != null) AfterUpdate.Invoke(reference.Effect);
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            int idx = 0;
            WriteProperty(sw, idx++, "Emitter_Duration", EmitterDuration);
            WriteProperty(sw, idx++, "Emitter_Loop", EmitterLoop);
            WriteProperty(sw, idx++, "Emitter_ScaleSpeed", EmitterScaleSpeed);
            WriteProperty(sw, idx++, "Emitter_Direction", (int)EmitterDirection);
            WriteProperty(sw, idx++, "Emitter_InheritVelocity", EmitterInheritVelocity);
            WriteProperty(sw, idx++, "Emitter_Drag", EmitterDrag);
            WriteProperty(sw, idx++, "Emitter_Rate", EmitterRate);
            WriteProperty(sw, idx++, "Emitter_Rate_LOD%", EmitterRateLODPercent);
            WriteProperty(sw, idx++, "Emitter_Deviation", EmitterDeviation);
            WriteProperty(sw, idx++, "Emitter_Volume", EmitterVolume);
            WriteProperty(sw, idx++, "Emitter_Offset", EmitterOffset);
            WriteProperty(sw, idx++, "Particle_Speed", ParticleSpeed);
            WriteProperty(sw, idx++, "Emitter_RotRate", EmitterRotationRate);
            WriteProperty(sw, idx++, "Particle_Fx", ParticleFX.Name);
            WriteProperty(sw, idx++, "Particle_Dynamics", (int)ParticleDynamics);
            WriteProperty(sw, idx++, "Gravwell_Strength", GravwellStrength);
            WriteProperty(sw, idx++, "Particle_Scale", ParticleScale);
            WriteProperty(sw, idx++, "Force_First", ForceFirst);
        }
    }
}