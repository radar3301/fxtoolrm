﻿using NLua;
using System;
using System.IO;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    public enum LightingPriority
    {
        High = 0,
        Low = 1
    }

    public class LightEffect : Effect
    {
        public override EffectStyle Style { get; } = EffectStyle.Light;

        public ColorKeyframes Color { get; } = new ColorKeyframes();
        public float Duration { get; set; }
        public bool Loop { get; set; }
        public VectorKeyframes Offset { get; set; }
        public LightingPriority Priority { get; set; }
        public FloatKeyframes Radius { get; }

        internal protected LightEffect(string filename, string name, LuaTable table) : base(filename, name, table)
        {
            Color = GetPropertyValue("Colour", new ColorKeyframes());
            Duration = GetPropertyValue("Duration", 1f);
            Loop = GetPropertyValue("Loop", false);
            Offset = GetPropertyValue("Offset", new VectorKeyframes());
            Radius = GetPropertyValue("Radius", new FloatKeyframes());
            Priority = (LightingPriority)GetPropertyValue("Priority", 1);
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            int idx = 0;
            WriteProperty(sw, idx++, "Radius", Radius);
            WriteProperty(sw, idx++, "Colour", Color);
            WriteProperty(sw, idx++, "Offset", Offset);
            WriteProperty(sw, idx++, "Loop", Loop);
            WriteProperty(sw, idx++, "Duration", Duration);
            WriteProperty(sw, idx++, "Priority", (int)Priority);
        }
    }
}