﻿using NLua;
using System.ComponentModel;
using System.Drawing.Design;
using System.IO;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    public class BeamEffect : Effect
    {
        public event EffectUpdate BeforeUpdate;
        public event EffectUpdate AfterUpdate;

        public override EffectStyle Style { get; } = EffectStyle.Beam;

        public VectorKeyframes Arc { get; }
        public EffectBlending Blending { get; set; }
        public ColorKeyframes Color { get; }
        public float Duration { get; set; }
        public FloatKeyframes Length { get; set; }
        public float LengthEpsilon { get; set; }
        public float LineDistance { get; set; }
        public bool Loop { get; set; }
        public VectorKeyframes Offset { get; set; }
        public FloatKeyframes Noise { get; }
        public FloatKeyframes Segments { get; }
        public bool SelfIlluminated { get; set; }
        [Editor(typeof(TextureReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(TextureConverter))]
        public TextureReference Texture { get; set; } = new TextureReference();
        public FloatKeyframes Texture_U_Offset { get; }
        public FloatKeyframes Texture_U_Repeat { get; }
        public FloatKeyframes Width { get; }
        public float WidthEpsilon { get; set; }

        internal protected BeamEffect(string filename, string name, LuaTable table) : base(filename, name, table)
        {
            Arc = GetPropertyValue("Arc", new VectorKeyframes());
            Blending = (EffectBlending)GetPropertyValue("Blending", 0);
            Color = GetPropertyValue("Colour", new ColorKeyframes());
            Duration = GetPropertyValue("Duration", 1f);
            Length = GetPropertyValue("Length", new FloatKeyframes());
            LengthEpsilon = GetPropertyValue("LengthEpsilon", 0f);
            LineDistance = GetPropertyValue("LineDistance", 0f);
            Loop = GetPropertyValue("Loop", false);
            Offset = GetPropertyValue("Offset", new VectorKeyframes());
            Noise = GetPropertyValue("Noise", new FloatKeyframes());
            Segments = GetPropertyValue("Segments", new FloatKeyframes());
            SelfIlluminated = GetPropertyValue("SelfIlluminated", false);
            Texture.DataPath = GetPropertyValue("Texture", "");
            Texture_U_Offset = GetPropertyValue("Texture_U_Offset", new FloatKeyframes());
            Texture_U_Repeat = GetPropertyValue("Texture_U_Repeat", new FloatKeyframes());
            Width = GetPropertyValue("Width", new FloatKeyframes());
            WidthEpsilon = GetPropertyValue("WidthEpsilon", 0f);

            Texture.BeforeUpdate += TextureReference_BeforeUpdate;
            Texture.AfterUpdate += TextureReference_AfterUpdate;
        }

        private void TextureReference_BeforeUpdate(TextureReference reference)
        {
            if (BeforeUpdate != null) BeforeUpdate.Invoke(this);
        }

        private void TextureReference_AfterUpdate(TextureReference reference)
        {
            if (!reference.IsValid)
                MessageBox.Show("The referenced texture \"" + reference.DataPath + "\" does not exist in the current data paths.");
            if (AfterUpdate != null) AfterUpdate.Invoke(this);
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            int idx = 0;
            WriteProperty(sw, idx++, "Duration", Duration);
            WriteProperty(sw, idx++, "Loop", Loop);
            WriteProperty(sw, idx++, "Blending", (int)Blending);
            WriteProperty(sw, idx++, "Texture", Texture.DataPath);
            WriteProperty(sw, idx++, "SelfIlluminated", SelfIlluminated);
            WriteProperty(sw, idx++, "Offset", Offset);
            WriteProperty(sw, idx++, "Length", Length);
            WriteProperty(sw, idx++, "Noise", Noise);
            WriteProperty(sw, idx++, "Segments", Segments);
            WriteProperty(sw, idx++, "Texture_U_Offset", Texture_U_Offset);
            WriteProperty(sw, idx++, "Texture_U_Repeat", Texture_U_Repeat);
            WriteProperty(sw, idx++, "Width", Width);
            WriteProperty(sw, idx++, "Colour", Color);
            WriteProperty(sw, idx++, "Arc", Arc);
            WriteProperty(sw, idx++, "LengthEpsilon", LengthEpsilon);
            WriteProperty(sw, idx++, "WidthEpsilon", WidthEpsilon);
            WriteProperty(sw, idx++, "LineDistance", LineDistance);
        }
    }
}
