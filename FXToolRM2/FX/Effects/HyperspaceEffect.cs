﻿using System;
using System.IO;
using NLua;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    public class HyperspaceEffect : Effect
    {
        public event EffectUpdate BeforeUpdate;
        public event EffectUpdate AfterUpdate;

        public override EffectStyle Style { get; } = EffectStyle.Hyperspace;

        public EffectBlending Blending { get; set; } = EffectBlending.NoBlending;
        public ColorKeyframes Color { get; } = new ColorKeyframes();
        public float Duration { get; set; } = 1f;
        public ColorKeyframes EdgeColor { get; } = new ColorKeyframes();
        [Editor(typeof(TextureReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(TextureConverter))]
        public TextureReference EdgeTexture { get; set; } = new TextureReference();
        public FloatKeyframes EdgeTexture_U_Offset { get; private set; } = new FloatKeyframes();
        public FloatKeyframes EdgeTexture_U_Repeat { get; } = new FloatKeyframes();
        [Editor(typeof(TextureReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(TextureConverter))]
        public TextureReference EdgeTexture2 { get; set; } = new TextureReference();
        public FloatKeyframes EdgeTexture2_U_Offset { get; } = new FloatKeyframes();
        public FloatKeyframes EdgeTexture2_U_Repeat { get; } = new FloatKeyframes();
        public FloatKeyframes Height { get; } = new FloatKeyframes();
        public FloatKeyframes LineWidth { get; } = new FloatKeyframes();
        public VectorKeyframes Offset { get; } = new VectorKeyframes();
        [Editor(typeof(TextureReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(TextureConverter))]
        public TextureReference Texture { get; set; } = new TextureReference();
        public FloatKeyframes Width { get; } = new FloatKeyframes();

        internal protected HyperspaceEffect(string filename, string name, LuaTable table) : base(filename, name, table)
        {
            Blending = (EffectBlending)GetPropertyValue("Blending", 0);
            Color = GetPropertyValue("Colour", new ColorKeyframes());
            Duration = GetPropertyValue("Duration", 1f);
            EdgeColor = GetPropertyValue("EdgeColor", new ColorKeyframes());
            EdgeTexture.DataPath = GetPropertyValue("EdgeTexture", "");
            EdgeTexture_U_Offset = GetPropertyValue("EdgeTexture_U_Offset", new FloatKeyframes());
            EdgeTexture_U_Repeat = GetPropertyValue("EdgeTexture_U_Repeat", new FloatKeyframes());
            EdgeTexture2.DataPath = GetPropertyValue("EdgeTexture", "");
            EdgeTexture2_U_Offset = GetPropertyValue("EdgeTexture2_U_Offset", new FloatKeyframes());
            EdgeTexture2_U_Repeat = GetPropertyValue("EdgeTexture2_U_Repeat", new FloatKeyframes());
            Height = GetPropertyValue("Height", new FloatKeyframes());
            LineWidth = GetPropertyValue("LineWidth", new FloatKeyframes());
            Offset = GetPropertyValue("Offset", new VectorKeyframes());
            Texture.DataPath = GetPropertyValue("Texture", "");
            Width = GetPropertyValue("Width", new FloatKeyframes());

            Texture.BeforeUpdate += TextureReference_BeforeUpdate;
            Texture.AfterUpdate += TextureReference_AfterUpdate;
            EdgeTexture.BeforeUpdate += TextureReference_BeforeUpdate;
            EdgeTexture.AfterUpdate += TextureReference_AfterUpdate;
            EdgeTexture2.BeforeUpdate += TextureReference_BeforeUpdate;
            EdgeTexture2.AfterUpdate += TextureReference_AfterUpdate;
        }

        private void TextureReference_BeforeUpdate(TextureReference reference)
        {
            if (BeforeUpdate != null) BeforeUpdate.Invoke(this);
        }

        private void TextureReference_AfterUpdate(TextureReference reference)
        {
            if (!reference.IsValid)
                MessageBox.Show("The referenced texture \"" + reference.DataPath + "\" does not exist in the current data paths.");
            if (AfterUpdate != null) AfterUpdate.Invoke(this);
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            int idx = 0;
            WriteProperty(sw, idx++, "Width", Width);
            WriteProperty(sw, idx++, "Height", Height);
            WriteProperty(sw, idx++, "Offset", Offset);
            WriteProperty(sw, idx++, "LineWidth", LineWidth);
            WriteProperty(sw, idx++, "EdgeTexture_U_Offset", EdgeTexture_U_Offset);
            WriteProperty(sw, idx++, "EdgeTexture_U_Repeat", EdgeTexture_U_Repeat);
            WriteProperty(sw, idx++, "EdgeTexture2_U_Offset", EdgeTexture2_U_Offset);
            WriteProperty(sw, idx++, "EdgeTexture2_U_Repeat", EdgeTexture2_U_Repeat);
            WriteProperty(sw, idx++, "Colour", Color);
            WriteProperty(sw, idx++, "EdgeColour", EdgeColor);
            WriteProperty(sw, idx++, "Duration", Duration);
            WriteProperty(sw, idx++, "Blending", (int)Blending);
            WriteProperty(sw, idx++, "Texture", Texture.DataPath);
            WriteProperty(sw, idx++, "EdgeTexture", EdgeTexture.DataPath);
            WriteProperty(sw, idx++, "EdgeTexture2", EdgeTexture2.DataPath);
        }
    }
}
