﻿using System;
using System.Drawing;
using System.IO;
using NLua;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    public class TrailEffect : Effect
    {
        public event EffectUpdate BeforeUpdate;
        public event EffectUpdate AfterUpdate;

        public override EffectStyle Style { get; } = EffectStyle.Trail;

        public bool Billboard { get; set; }
        public EffectBlending Blending { get; set; }
        public Color ColourHead { get; set; }
        public Color ColourTail { get; set; }
        public float DeathRate { get; set; }
        public float Duration { get; set; }
        public float FadeOutTime { get; set; }
        public float Length { get; set; }
        public float LengthEpsilon { get; set; }
        public float LineDistance { get; set; }
        [Editor(typeof(TextureReferenceEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(TextureConverter))]
        public TextureReference Texture { get; set; } = new TextureReference();
        public float Width { get; set; }
        public float WidthEpsilon { get; set; }

        internal protected TrailEffect(string filename, string name, LuaTable table) : base(filename, name, table)
        {
            Billboard = GetPropertyValue("Billboard", false);
            Blending = (EffectBlending)GetPropertyValue("Blending", 0);
            ColourHead = GetPropertyValue("ColourHead", Color.Black);
            ColourTail = GetPropertyValue("ColourTail", Color.Black);
            DeathRate = GetPropertyValue("DeathRate", 0f);
            Duration = GetPropertyValue("Duration", 0f);
            FadeOutTime = GetPropertyValue("FadeOutTime", 0f);
            Length = GetPropertyValue("Length", 0f);
            LengthEpsilon = GetPropertyValue("LengthEpsilon", 0f);
            LineDistance = GetPropertyValue("LineDistance", 0f);
            Texture.DataPath = GetPropertyValue("Texture", "");
            Width = GetPropertyValue("Width", 0f);
            WidthEpsilon = GetPropertyValue("WidthEpsilon", 0f);

            Texture.BeforeUpdate += TextureReference_BeforeUpdate;
            Texture.AfterUpdate += TextureReference_AfterUpdate;
        }

        private void TextureReference_BeforeUpdate(TextureReference reference)
        {
            if (BeforeUpdate != null) BeforeUpdate.Invoke(this);
        }

        private void TextureReference_AfterUpdate(TextureReference reference)
        {
            if (!reference.IsValid)
                MessageBox.Show("The referenced texture \"" + reference.DataPath + "\" does not exist in the current data paths.");
            if (AfterUpdate != null) AfterUpdate.Invoke(this);
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            int idx = 0;
            WriteProperty(sw, idx++, "Length", Length);
            WriteProperty(sw, idx++, "Width", Width);
            WriteProperty(sw, idx++, "Blending", (int)Blending);
            WriteProperty(sw, idx++, "Texture", Texture.DataPath);
            WriteProperty(sw, idx++, "ColourHead", ColourHead);
            WriteProperty(sw, idx++, "ColourTail", ColourTail);
            WriteProperty(sw, idx++, "Billboard", Billboard);
            WriteProperty(sw, idx++, "DeathRate", DeathRate);
            WriteProperty(sw, idx++, "FadeOutTime", FadeOutTime);
            WriteProperty(sw, idx++, "Duration", Duration);
            WriteProperty(sw, idx++, "LengthEpsilon", LengthEpsilon);
            WriteProperty(sw, idx++, "WidthEpsilon", WidthEpsilon);
            WriteProperty(sw, idx++, "LineDistance", LineDistance);
        }
    }
}