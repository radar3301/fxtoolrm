﻿using System;
using System.IO;
using NLua;
using System.Windows.Forms;
using System.Drawing.Design;
using System.ComponentModel;

namespace FXToolRM.FX
{
    public class LensFlareEffect : Effect
    {
        public override EffectStyle Style { get; } = EffectStyle.LensFlare;

        public ColorKeyframes Color { get; } = new ColorKeyframes();
        public float Duration { get; set; }
        public bool Infinite { get; set; }
        [Editor(typeof(LensFlareReferenceEditor), typeof(UITypeEditor))]
        public string LensFlare { get; set; }
        public FloatKeyframes Radius { get; }

        internal protected LensFlareEffect(string filename, string name, LuaTable table) : base(filename, name, table)
        {
            Color = GetPropertyValue("Colour", new ColorKeyframes());
            Duration = GetPropertyValue("Duration", 1f);
            Infinite = GetPropertyValue("Infinite", false);
            LensFlare = GetPropertyValue("LensFlare", "");
            Radius = GetPropertyValue("Radius", new FloatKeyframes());
        }

        protected override void WriteProperties(StreamWriter sw)
        {
            int idx = 0;
            WriteProperty(sw, idx++, "Radius", Radius);
            WriteProperty(sw, idx++, "Colour", Color);
            WriteProperty(sw, idx++, "LensFlare", LensFlare);
            WriteProperty(sw, idx++, "Duration", Duration);
            WriteProperty(sw, idx++, "Infinite", Infinite);
        }
    }
}