﻿using FXToolRM.Rendering;
using System.IO;

namespace FXToolRM.FX
{
    public delegate void EffectReferenceUpdate(EffectReference reference);

    public class EffectReference
    {
        public event EffectReferenceUpdate BeforeUpdate;
        public event EffectReferenceUpdate AfterUpdate;

        private string name = "";
        private Effect effect = null;

        public string Name
        {
            get
            {
                return effect != null ? effect.Name : name;
            }
            set
            {
                if (name != value)
                {
                    if (BeforeUpdate != null) BeforeUpdate.Invoke(this);
                    if (value != "")
                    {
                        string path = Program.GetDataPath(Path.Combine("art", "fx", value + ".lua"));
                        effect = path == "" ? null : Effect.Load(path);
                    }
                    else
                    {
                        effect = null;
                    }
                    name = value;
                    if (AfterUpdate != null) AfterUpdate.Invoke(this);
                }
            }
        }

        public Effect Effect
        {
            get
            {
                return effect;
            }
            set
            {
                if (effect != value)
                {
                    if (value == null)
                    {
                        name = "";
                        effect = null;
                    }
                    else
                    {
                        name = value.Name;
                        effect = value;
                    }
                }
            }
        }

        public bool IsValid => name == "" || effect != null;

        public EffectReference() {  }

        public override string ToString()
        {
            return Name;
        }
    }

    public delegate void TextureReferenceUpdate(TextureReference reference);

    public class TextureReference
    {
        public event TextureReferenceUpdate BeforeUpdate;
        public event TextureReferenceUpdate AfterUpdate;

        private string datapath = "";
        private Texture texture = null;

        public string DataPath
        {
            get
            {
                if (texture == null) texture = Texture.DefaultTexture;
                return texture != Texture.DefaultTexture ? texture.DataPath : datapath;
            }
            set
            {
                if (datapath != value)
                {
                    if (BeforeUpdate != null) BeforeUpdate.Invoke(this);
                    if (value != "")
                    {
                        string path = Program.GetDataPath(value.ToLower().Replace("data:", "").Replace('/', '\\'));
                        texture = path == "" ? Texture.DefaultTexture : Texture.LoadTexture(path);
                    }
                    else
                    {
                        texture = Texture.DefaultTexture;
                    }
                    datapath = value;
                    if (AfterUpdate != null) AfterUpdate.Invoke(this);
                }
            }
        }

        public Texture Texture
        {
            get
            {
                return texture;
            }
            set
            {
                if (texture != value)
                {
                    if (value == null)
                    {
                        datapath = "";
                        texture = Texture.DefaultTexture;
                    }
                    else
                    {
                        datapath = value.DataPath;
                        texture = value;
                    }
                }
            }
        }

        public bool IsValid => datapath == "" || texture != Texture.DefaultTexture;
        public bool IsAnimated => texture is AnimatedTexture;

        public TextureReference() { }

        public override string ToString()
        {
            return DataPath;
        }
    }
}
