﻿namespace FXToolRM.FX
{
    public enum EffectStyle
    {
        Invalid = -1,
        Spray,
        Ring,
        Beam,
        Trail,
        Combo,
        Light,
        Hyperspace,
        LensFlare
    }
}