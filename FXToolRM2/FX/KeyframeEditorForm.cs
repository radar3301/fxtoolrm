﻿using Microsoft.VisualBasic;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace FXToolRM.FX
{
    public partial class KeyframeEditorForm<T> : Form
    {
        internal KeyframeList<T> Keyframes;

        public KeyframeEditorForm(KeyframeList<T> keyframes)
        {
            InitializeComponent();
            Keyframes = keyframes;
            if (keyframes != null)
                trkKeyframeIndex.Maximum = keyframes.Count - 1;
            if (trkKeyframeIndex.Maximum >= 0)
                trkKeyframeIndex.Value = 0;
            trkKeyframeIndex_ValueChanged(null, null);
        }

        private void trkKeyframeIndex_ValueChanged(object sender, EventArgs e)
        {
            if (trkKeyframeIndex.Value < 0 || trkKeyframeIndex.Value >= Keyframes.Count)
            {
                pgridKeyframe.SelectedObject = null;
            }
            else
            {
                pgridKeyframe.SelectedObject = Keyframes[trkKeyframeIndex.Value];
            }
        }

        private void pgridKeyframe_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            Keyframe<T> kf = (Keyframe<T>)pgridKeyframe.SelectedObject;
            Keyframes[trkKeyframeIndex.Value] = kf;
            trkKeyframeIndex.Value = Keyframes.GetIndexOfTime(kf.Time);
        }

        private void btnKeyframeAdd_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Enter the time for the new keyframe.", "Add Keyframe", "0.0");
            float time;
            if (float.TryParse(input, out time))
            {
                if (Keyframes.GetIndexOfTime(time) != -1)
                    MessageBox.Show("A keyframe with this time already exists.", "Add Keyframe");
                else
                {
                    Keyframes[time] = Keyframes[time];
                    int index = Keyframes.GetIndexOfTime(time);
                    if (Keyframes.Count == 1)
                        trkKeyframeIndex.Minimum++;
                    else
                        trkKeyframeIndex.Maximum++;
                    trkKeyframeIndex.Value = index;
                    pgridKeyframe.SelectedObject = Keyframes[index];
                }
            }
            else
                MessageBox.Show("Unable to parse input.", "Add Keyframe");
        }

        private void btnKeyframeRemove_Click(object sender, EventArgs e)
        {
            if (trkKeyframeIndex.Value >= 0)
            {
                Keyframes.RemoveAt(trkKeyframeIndex.Value);
                trkKeyframeIndex.Maximum--;
            }
            else
                MessageBox.Show("No keyframes to remove.", "Remove Keyframe");
        }
    }
}
