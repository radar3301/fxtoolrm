﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using OpenTK;
using System.Globalization;

namespace FXToolRM.FX
{
    public struct Keyframe<T>
    {
        public float Time { get; set; }
        public T Value { get; set; }

        internal Keyframe(float time, T value)
        {
            Time = time;
            Value = value;
        }
    }

    public abstract class KeyframeList<T>
    {
        protected SortedList<float, T> keyframes = new SortedList<float, T>();

        public Keyframe<T> this[int index]
        {
            get
            {
                return new Keyframe<T>(keyframes.Keys[index], keyframes.Values[index]);
            }
            set
            {
                keyframes.RemoveAt(index);
                keyframes.Add(value.Time, value.Value);
            }
        }

        public float GetTimeAtIndex(int index)
        {
            if (keyframes.Count == 0) return 0;
            if (index <= 0) return keyframes.Keys.First();
            if (index >= keyframes.Count) return keyframes.Keys.Last();
            return keyframes.Keys[index];
        }

        public int SetTimeAtIndex(int index, float value)
        {
            Keyframe<T> keyframe = this[index];
            keyframe.Time = value;
            return keyframes.IndexOfKey(value);
        }

        public T GetValueAtIndex(int index)
        {
            if (keyframes.Count == 0) return DefaultKeyframeValue();
            if (index <= 0) return keyframes.Values.First();
            if (index >= keyframes.Count) return keyframes.Values.Last();
            return keyframes.Values[index];
        }

        public void SetValueAtIndex(int index, T value)
        {
            keyframes.Values[index] = value;
        }

        internal int GetIndexOfTime(float time)
        {
            return keyframes.IndexOfKey(time);
        }

        public virtual T this[float time]
        {
            get
            {
                if (keyframes.Count == 0) return DefaultKeyframeValue();
                if (time <= keyframes.Keys.First()) return keyframes.Values.First();
                if (time >= keyframes.Keys.Last()) return keyframes.Values.Last();
                if (keyframes.ContainsKey(time)) return keyframes[time];

                for (int i = 1; i < keyframes.Count; i++)
                {
                    float prev_time = keyframes.Keys[i - 1];
                    float next_time = keyframes.Keys[i];
                    T prev_value = keyframes.Values[i - 1];
                    T next_value = keyframes.Values[i];

                    if (prev_time < time && time < next_time)
                    {
                        float k = (time - prev_time) / (next_time - prev_time);
                        return Lerp(prev_value, next_value, k);
                    }
                }
                return DefaultKeyframeValue();
            }
            set
            {
                if (keyframes.ContainsKey(time))
                    keyframes[time] = value;
                else
                    keyframes.Add(time, value);
            }
        }

        [Browsable(false)]
        public int Count { get { return keyframes.Count; } }

        public void Add(float time, T value)
        {
            keyframes[time] = value;
        }

        public void Remove(float time)
        {
            keyframes.Remove(time);
        }

        public void RemoveAt(int index)
        {
            keyframes.RemoveAt(index);
        }

        protected abstract T Lerp(T a, T b, float k);
        protected abstract T DefaultKeyframeValue();
    }

    [Editor(typeof(KeyframeEditor<float>), typeof(UITypeEditor))]
    public class FloatKeyframes : KeyframeList<float>
    {
        public override string ToString()
        {
            return "Count = " + Count;
        }

        protected override float Lerp(float a, float b, float k)
        {
            return (1 - k) * a + k * b;
        }

        protected override float DefaultKeyframeValue()
        {
            return 0f;
        }
    }

    [Editor(typeof(KeyframeEditor<Color>), typeof(UITypeEditor))]
    public class ColorKeyframes : KeyframeList<Color>
    {
        public override string ToString()
        {
            return "Count = " + Count;
        }

        protected override Color Lerp(Color a, Color b, float k)
        {
            byte R = (byte)((1 - k) * a.R + k * b.R);
            byte G = (byte)((1 - k) * a.G + k * b.G);
            byte B = (byte)((1 - k) * a.B + k * b.B);
            byte A = (byte)((1 - k) * a.A + k * b.A);
            return Color.FromArgb(A, R, G, B);
        }

        protected override Color DefaultKeyframeValue()
        {
            return Color.FromArgb(0);
        }
    }

    public class VectorConverter : ExpandableObjectConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                try
                {
                    string s = (string)value;
                    int first = s.IndexOf(';');
                    if (first != -1)
                    {
                        int second = s.IndexOf(';', first + 1);
                        if (second != -1)
                        {
                            if (s.IndexOf(';', second + 1) >= 0)
                                throw new ArgumentException("Can not convert '" + (string)value + "' to type Vector");
                            float X = float.Parse(s.Substring(0, first));
                            float Y = float.Parse(s.Substring(first + 1, second - (first + 1)));
                            float Z = float.Parse(s.Substring(second + 1));
                            return new Vector(X, Y, Z);
                        }
                    }
                }
                catch
                { }
                // if we got this far, complain that we couldn't parse the string
                throw new ArgumentException("Can not convert '" + (string)value + "' to type Vector");
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) && value is Vector)
            {
                return ((Vector)value).ToString();
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            if (value is string)
            {
                try {
                    string s = (string)value;
                    int first = s.IndexOf(';');
                    if (first != -1)
                    {
                        int second = s.IndexOf(';', first + 1);
                        if (second != -1)
                        {
                            if (s.IndexOf(';', second + 1) >= 0)
                                throw new ArgumentException("Can not convert '" + (string)value + "' to type Vector");
                            float X = float.Parse(s.Substring(0, first));
                            float Y = float.Parse(s.Substring(first + 1, second - (first + 1)));
                            float Z = float.Parse(s.Substring(second + 1));
                            return true;
                        }
                    }
                }
                catch
                { }
            }
            return base.IsValid(context, value);
        }
    }
    
    [TypeConverter(typeof(VectorConverter))]
    public struct Vector
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Vector(Vector3 v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
        }

        public Vector(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector(float k)
        {
            X = Y = Z = k;
        }

        public static Vector operator *(Vector a, Vector b)
        {
            return (Vector3)a * (Vector3)b;
        }

        public static Vector operator *(float a, Vector b)
        {
            return a * (Vector3)b;
        }

        public static Vector operator *(Vector a, float b)
        {
            return (Vector3)a * b;
        }

        public static Vector operator +(Vector a, Vector b)
        {
            return (Vector3)a + (Vector3)b;
        }

        public static Vector operator +(Vector3 a, Vector b)
        {
            return a + (Vector3)b;
        }

        public static Vector operator +(Vector a, Vector3 b)
        {
            return (Vector3)a + b;
        }

        public static implicit operator Vector3(Vector v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }

        public static implicit operator Vector(Vector3 v)
        {
            return new Vector(v);
        }

        public override string ToString()
        {
            return X + "; " + Y + "; " + Z;
        }
    }

    [Editor(typeof(KeyframeEditor<Vector>), typeof(UITypeEditor))]
    public class VectorKeyframes : KeyframeList<Vector>
    {
        public override string ToString()
        {
            return "Count = " + Count;
        }

        protected override Vector Lerp(Vector a, Vector b, float k)
        {
            return (1 - k) * a + k * b;
        }

        protected override Vector DefaultKeyframeValue()
        {
            return Vector3.Zero;
        }
    }
    
    public class KeyframeEditor<T> : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            if (context != null && context.Instance != null)
            {
                return UITypeEditorEditStyle.Modal;
            }
            return base.GetEditStyle(context);
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService editorService = null;

            if (provider != null)
            {
                editorService =
                    provider.GetService(typeof(IWindowsFormsEditorService))
                    as IWindowsFormsEditorService;
            }

            if (editorService != null)
            {
                KeyframeEditorForm<T> EditorForm = new KeyframeEditorForm<T>((KeyframeList<T>)value);
                editorService.ShowDialog(EditorForm);
                value = EditorForm.Keyframes;
            }

            return value;
        }
    }
}