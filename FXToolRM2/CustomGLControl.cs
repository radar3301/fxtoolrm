﻿using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using FXToolRM.Rendering;


namespace FXToolRM
{
    public partial class CustomGLControl : GLControl
    {
        private bool IsInitialized { get; set; }
        
        // 32bpp color, 24bpp z-depth, 8bpp stencil and 4x antialiasing
        // OpenGL version is major=3, minor=0
        public CustomGLControl()
            : base(new GraphicsMode(32, 24, 8, 4), 3, 0, GraphicsContextFlags.Default)
        {

        }

        public void Init()
        {
            GL.ClearColor(Program.BackgroundColor);
            GL.Enable(EnableCap.CullFace);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.Zero);
            GL.LineWidth(1);
            
            IsInitialized = true;
        }

        public void Render()
        {
            if (!IsInitialized) return;
            GL.ClearColor(Program.BackgroundColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Scene.Draw();

            SwapBuffers();
        }

        public void ResizeView()
        {
            if (!IsInitialized) return;
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);
        }

        public void UpdateView()
        {
            if (!IsInitialized) return;
        }
    }
}
