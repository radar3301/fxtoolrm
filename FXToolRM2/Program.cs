﻿using FXToolRM.Rendering;
using HWShaderManifest;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace FXToolRM
{
    static class Program
    {
        public static OpenTK.NativeWindow NativeWindow = new OpenTK.NativeWindow();

        public static string EXECUTABLE_PATH = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        public static SettingsForm frmSettings;
        public static MainForm frmMain;
        public static CustomGLControl GLControl;
        public static List<string> DataPaths = new List<string>();

        public static Color BackgroundColor { get; internal set; }

        /// <summary>
        /// </summary>
        /// The main entry point for the application.
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            frmMain = new MainForm();
            frmSettings = new SettingsForm();
            SettingsForm.LoadSettings();
            CreateGLControl();
            Application.Run(frmMain);
        }

        private static void CreateGLControl()
        {
            GLControl = new CustomGLControl();
            GLControl.BackColor = System.Drawing.Color.Black;
            GLControl.Dock = DockStyle.Fill;
            GLControl.Location = new System.Drawing.Point(0, 0);
            GLControl.Name = "ctlCanvas";
            GLControl.Size = new System.Drawing.Size(200, 200);
            GLControl.TabIndex = 0;
            GLControl.VSync = true;
            GLControl.Paint += new PaintEventHandler(frmMain.GLControl_Paint);
            GLControl.MouseDown += new MouseEventHandler(frmMain.GLControl_MouseDown);
            GLControl.MouseMove += new MouseEventHandler(frmMain.GLControl_MouseMove);
            GLControl.MouseWheel += new MouseEventHandler(frmMain.GLControl_MouseWheel);
            GLControl.Resize += new EventHandler(frmMain.GLControl_Resize);

            frmMain.splitContainer1.Panel2.Controls.Add(GLControl);
        }

        public static string GetDataPath(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename)) return "";
            string file = "";
            foreach (string datapath in DataPaths)
            {
                string datafile = Path.Combine(datapath, filename);
                if (File.Exists(datafile))
                    file = datafile;
            }
            return file;
        }
    }
}
