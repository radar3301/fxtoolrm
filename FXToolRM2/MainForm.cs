﻿
using System;
using System.Windows.Forms;
using HWShaderManifest;
using FXToolRM.FX;
using FXToolRM.Rendering;
using System.Drawing;
using OpenTK;
using Microsoft.VisualBasic;

namespace FXToolRM
{
    public partial class MainForm : Form
    {
        private Effect CurrentEffect;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Program.GLControl.Init();
            ShaderManifest.DataPaths.AddRange(Program.DataPaths);
            ShaderManifest.Init();
            Texture.Init();
            Application.Idle += GLControl_Update;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Idle -= GLControl_Update;
            SettingsForm.SaveSettings();
        }

        public void GLControl_Paint(object sender, PaintEventArgs e)
        {
            Program.GLControl.Render();
        }

        private Point SaveMousePos = new Point();

        public void GLControl_MouseDown(object sender, MouseEventArgs e)
        {
            bool left = (e.Button & MouseButtons.Left) == MouseButtons.Left;
            bool right = (e.Button & MouseButtons.Right) == MouseButtons.Right;
            if (left && !right)
                SaveMousePos = e.Location;
        }

        public void GLControl_MouseMove(object sender, MouseEventArgs e)
        {
            bool left = (e.Button & MouseButtons.Left) == MouseButtons.Left;
            bool right = (e.Button & MouseButtons.Right) == MouseButtons.Right;
            if (left && !right)
            {
                Point NewMousePos = e.Location;
                Vector2 Delta = new Vector2(NewMousePos.X - SaveMousePos.X, NewMousePos.Y - SaveMousePos.Y);
                SaveMousePos = NewMousePos;

                Scene.Camera.CameraRotate(Delta);
            }
        }

        public void GLControl_MouseWheel(object sender, MouseEventArgs e)
        {
            Scene.Camera.CameraZoom((float)e.Delta / Program.GLControl.Height);
        }

        public void GLControl_Resize(object sender, EventArgs e)
        {
            Program.GLControl.ResizeView();
            Program.GLControl.UpdateView();
            Program.GLControl.Invalidate();
        }

        public void GLControl_Update(object sender, EventArgs e)
        {
            RenderManager.Update();
            Program.GLControl.Invalidate();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                btnStop_Click(null, null);
                RenderManager.ClearRenderers();
                CurrentEffect = Effect.Load(openFileDialog1.FileName);
                propertyGrid1.SelectedObject = CurrentEffect;
                Renderer r = Utils.GetRenderer(CurrentEffect);
                if (r != null) RenderManager.AddRenderer(r);
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            RenderManager.Start();
            btnPlay.Enabled = false;
            btnPause.Enabled = true;
            btnStop.Enabled = true;
            btnStepBack.Enabled = false;
            btnStepForward.Enabled = false;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            RenderManager.Stop();
            btnPlay.Enabled = true;
            btnPause.Enabled = false;
            btnStop.Enabled = true;
            btnStepBack.Enabled = true;
            btnStepForward.Enabled = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            RenderManager.Reset();
            btnPlay.Enabled = true;
            btnPause.Enabled = false;
            btnStop.Enabled = false;
            btnStepBack.Enabled = true;
            btnStepForward.Enabled = true;
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.frmSettings = new SettingsForm();
            Program.frmSettings.Init();
            Program.frmSettings.ShowDialog();
        }

        private void btnStepBack_Click(object sender, EventArgs e)
        {
            RenderManager.SystemTime -= 0.1f;
            if (RenderManager.SystemTime < 0) RenderManager.SystemTime = 0;
        }

        private void btnStepForward_Click(object sender, EventArgs e)
        {
            RenderManager.SystemTime += 0.1f;
        }

        private void reloadShaderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Scene.EditorShader.Reload();
            ShaderManifest.Reload();
        }

        private void numZScale_ValueChanged(object sender, EventArgs e)
        {
            Scene.Camera.Scale.Z = (float)numZScale.Value;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentEffect.Save();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() != DialogResult.OK) return;
            CurrentEffect.Save(saveFileDialog1.FileName);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void beamEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = Interaction.InputBox("Enter the name of the new effect.", "New Beam Effect");
            if (effectName == "") return;
            RenderManager.ClearRenderers();
            CurrentEffect = new BeamEffect("", effectName, null);
            propertyGrid1.SelectedObject = CurrentEffect;
            Renderer r = Utils.GetRenderer(CurrentEffect);
            if (r != null) RenderManager.AddRenderer(r);
        }

        private void comboEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = Interaction.InputBox("Enter the name of the new effect.", "New Combo Effect");
            if (effectName == "") return;
            RenderManager.ClearRenderers();
            CurrentEffect = new ComboEffect("", effectName, null);
            propertyGrid1.SelectedObject = CurrentEffect;
            Renderer r = Utils.GetRenderer(CurrentEffect);
            if (r != null) RenderManager.AddRenderer(r);
        }

        private void hyperspaceEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = Interaction.InputBox("Enter the name of the new effect.", "New Hyperspace Effect");
            if (effectName == "") return;
            RenderManager.ClearRenderers();
            CurrentEffect = new HyperspaceEffect("", effectName, null);
            propertyGrid1.SelectedObject = CurrentEffect;
            Renderer r = Utils.GetRenderer(CurrentEffect);
            if (r != null) RenderManager.AddRenderer(r);
        }

        private void lensFlareEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = Interaction.InputBox("Enter the name of the new effect.", "New LensFlare Effect");
            if (effectName == "") return;
            RenderManager.ClearRenderers();
            CurrentEffect = new LensFlareEffect("", effectName, null);
            propertyGrid1.SelectedObject = CurrentEffect;
            Renderer r = Utils.GetRenderer(CurrentEffect);
            if (r != null) RenderManager.AddRenderer(r);
        }

        private void lightEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = Interaction.InputBox("Enter the name of the new effect.", "New Light Effect");
            if (effectName == "") return;
            RenderManager.ClearRenderers();
            CurrentEffect = new LightEffect("", effectName, null);
            propertyGrid1.SelectedObject = CurrentEffect;
            Renderer r = Utils.GetRenderer(CurrentEffect);
            if (r != null) RenderManager.AddRenderer(r);
        }

        private void ringEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = Interaction.InputBox("Enter the name of the new effect.", "New Ring Effect");
            if (effectName == "") return;
            RenderManager.ClearRenderers();
            CurrentEffect = new RingEffect("", effectName, null);
            propertyGrid1.SelectedObject = CurrentEffect;
            Renderer r = Utils.GetRenderer(CurrentEffect);
            if (r != null) RenderManager.AddRenderer(r);
        }

        private void sprayEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = Interaction.InputBox("Enter the name of the new effect.", "New Spray Effect");
            if (effectName == "") return;
            RenderManager.ClearRenderers();
            CurrentEffect = new SprayEffect("", effectName, null);
            propertyGrid1.SelectedObject = CurrentEffect;
            Renderer r = Utils.GetRenderer(CurrentEffect);
            if (r != null) RenderManager.AddRenderer(r);
        }

        private void trailEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string effectName = Interaction.InputBox("Enter the name of the new effect.", "New Trail Effect");
            if (effectName == "") return;
            RenderManager.ClearRenderers();
            CurrentEffect = new TrailEffect("", effectName, null);
            propertyGrid1.SelectedObject = CurrentEffect;
            Renderer r = Utils.GetRenderer(CurrentEffect);
            if (r != null) RenderManager.AddRenderer(r);
        }
    }
}
