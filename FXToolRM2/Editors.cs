﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.IO;
using FXToolRM.FX;

namespace FXToolRM
{
    public class TextureReferenceEditor : FileNameEditor
    {
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            TextureReference reference = (TextureReference)value;
            string path = (string)base.EditValue(context, provider, reference.Texture == null ? "" : reference.Texture.Filename);
            if (path == "") return reference;
            foreach (string datapath in Program.DataPaths)
            {
                if (path.StartsWith(datapath))
                {
                    reference.DataPath = path
                        .Replace(datapath + Path.DirectorySeparatorChar, "data:")
                        .Replace(Path.DirectorySeparatorChar, '/');
                    return reference;
                }
            }
            MessageBox.Show("The specified file is not in the currently specified data paths.");
            return path;
        }

        protected override void InitializeDialog(OpenFileDialog openFileDialog)
        {
            base.InitializeDialog(openFileDialog);
            openFileDialog.Filter = "Static or Animated Texture File (*.tga, *.anim)|*.tga;*.anim|All files (*.*)|*.*";
        }
    }

    public class EffectReferenceEditor : FileNameEditor
    {
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            EffectReference reference = (EffectReference)value;
            string path = (string)base.EditValue(context, provider, reference.Effect == null ? "" : reference.Effect.Filename);
            if (path == "") return reference;
            foreach (string datapath in Program.DataPaths)
            {
                if (path.StartsWith(Path.Combine(Path.Combine(datapath, "art"), "fx")))
                {
                    reference.Name = Path.GetFileNameWithoutExtension(path);
                    return reference;
                }
            }
            MessageBox.Show("The specified file is not in the \"data:art/fx/\" folder of the currently specified data paths.");
            return reference;
        }

        protected override void InitializeDialog(OpenFileDialog openFileDialog)
        {
            base.InitializeDialog(openFileDialog);
            openFileDialog.Filter = "Effect File (*.lua)|*.lua|All files (*.*)|*.*";
        }
    }

    public class LensFlareReferenceEditor : FileNameEditor
    {
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            object v = base.EditValue(context, provider, value);
            string path = (string)v;
            if (path == "") return value;
            foreach (string datapath in Program.DataPaths)
                if (path.StartsWith(Path.Combine(Path.Combine(datapath, "effect"), "lensflare")))
                    return Path.GetFileNameWithoutExtension(path);
            MessageBox.Show("The specified file is not in the \"data:effect/lensflare/\" folder of the currently specified data paths.");
            return path;
        }

        protected override void InitializeDialog(OpenFileDialog openFileDialog)
        {
            base.InitializeDialog(openFileDialog);
            openFileDialog.Filter = "Lens Flare File (*.flare)|*.flare|All files (*.*)|*.*";
        }
    }
}
