﻿namespace FXToolRM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beamEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hyperspaceEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lensFlareEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lightEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ringEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sprayEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trailEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadShaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.numZScale = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPlay = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStepForward = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStepBack = new System.Windows.Forms.Button();
            this.pgbEffectTime = new System.Windows.Forms.ProgressBar();
            this.numSpeed = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numZScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.reloadShaderToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(768, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.beamEffectToolStripMenuItem,
            this.comboEffectToolStripMenuItem,
            this.hyperspaceEffectToolStripMenuItem,
            this.lensFlareEffectToolStripMenuItem,
            this.lightEffectToolStripMenuItem,
            this.ringEffectToolStripMenuItem,
            this.sprayEffectToolStripMenuItem,
            this.trailEffectToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // beamEffectToolStripMenuItem
            // 
            this.beamEffectToolStripMenuItem.Name = "beamEffectToolStripMenuItem";
            this.beamEffectToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.beamEffectToolStripMenuItem.Text = "&Beam Effect";
            this.beamEffectToolStripMenuItem.Click += new System.EventHandler(this.beamEffectToolStripMenuItem_Click);
            // 
            // comboEffectToolStripMenuItem
            // 
            this.comboEffectToolStripMenuItem.Name = "comboEffectToolStripMenuItem";
            this.comboEffectToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.comboEffectToolStripMenuItem.Text = "&Combo Effect";
            this.comboEffectToolStripMenuItem.Click += new System.EventHandler(this.comboEffectToolStripMenuItem_Click);
            // 
            // hyperspaceEffectToolStripMenuItem
            // 
            this.hyperspaceEffectToolStripMenuItem.Name = "hyperspaceEffectToolStripMenuItem";
            this.hyperspaceEffectToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.hyperspaceEffectToolStripMenuItem.Text = "&Hyperspace Effect";
            this.hyperspaceEffectToolStripMenuItem.Click += new System.EventHandler(this.hyperspaceEffectToolStripMenuItem_Click);
            // 
            // lensFlareEffectToolStripMenuItem
            // 
            this.lensFlareEffectToolStripMenuItem.Name = "lensFlareEffectToolStripMenuItem";
            this.lensFlareEffectToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.lensFlareEffectToolStripMenuItem.Text = "Lens &Flare Effect";
            this.lensFlareEffectToolStripMenuItem.Click += new System.EventHandler(this.lensFlareEffectToolStripMenuItem_Click);
            // 
            // lightEffectToolStripMenuItem
            // 
            this.lightEffectToolStripMenuItem.Name = "lightEffectToolStripMenuItem";
            this.lightEffectToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.lightEffectToolStripMenuItem.Text = "&Light Effect";
            this.lightEffectToolStripMenuItem.Click += new System.EventHandler(this.lightEffectToolStripMenuItem_Click);
            // 
            // ringEffectToolStripMenuItem
            // 
            this.ringEffectToolStripMenuItem.Name = "ringEffectToolStripMenuItem";
            this.ringEffectToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.ringEffectToolStripMenuItem.Text = "&Ring Effect";
            this.ringEffectToolStripMenuItem.Click += new System.EventHandler(this.ringEffectToolStripMenuItem_Click);
            // 
            // sprayEffectToolStripMenuItem
            // 
            this.sprayEffectToolStripMenuItem.Name = "sprayEffectToolStripMenuItem";
            this.sprayEffectToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.sprayEffectToolStripMenuItem.Text = "&Spray Effect";
            this.sprayEffectToolStripMenuItem.Click += new System.EventHandler(this.sprayEffectToolStripMenuItem_Click);
            // 
            // trailEffectToolStripMenuItem
            // 
            this.trailEffectToolStripMenuItem.Name = "trailEffectToolStripMenuItem";
            this.trailEffectToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.trailEffectToolStripMenuItem.Text = "&Trail Effect";
            this.trailEffectToolStripMenuItem.Click += new System.EventHandler(this.trailEffectToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "&Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // reloadShaderToolStripMenuItem
            // 
            this.reloadShaderToolStripMenuItem.Name = "reloadShaderToolStripMenuItem";
            this.reloadShaderToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.reloadShaderToolStripMenuItem.Text = "Reload Shader";
            this.reloadShaderToolStripMenuItem.Click += new System.EventHandler(this.reloadShaderToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Homeworld FX File (*.lua)|*.lua";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.propertyGrid1);
            this.splitContainer1.Panel1MinSize = 200;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(768, 337);
            this.splitContainer1.SplitterDistance = 256;
            this.splitContainer1.TabIndex = 1;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid1.BackColor = System.Drawing.SystemColors.Control;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propertyGrid1.Size = new System.Drawing.Size(256, 337);
            this.propertyGrid1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.numZScale);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnPlay);
            this.panel1.Controls.Add(this.btnPause);
            this.panel1.Controls.Add(this.btnStepForward);
            this.panel1.Controls.Add(this.btnStop);
            this.panel1.Controls.Add(this.btnStepBack);
            this.panel1.Controls.Add(this.pgbEffectTime);
            this.panel1.Controls.Add(this.numSpeed);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(508, 24);
            this.panel1.TabIndex = 3;
            // 
            // numZScale
            // 
            this.numZScale.DecimalPlaces = 2;
            this.numZScale.Location = new System.Drawing.Point(184, 2);
            this.numZScale.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numZScale.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numZScale.Name = "numZScale";
            this.numZScale.Size = new System.Drawing.Size(56, 20);
            this.numZScale.TabIndex = 11;
            this.numZScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numZScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numZScale.ValueChanged += new System.EventHandler(this.numZScale_ValueChanged);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(128, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 24);
            this.label2.TabIndex = 10;
            this.label2.Text = "Z Scale:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnPlay
            // 
            this.btnPlay.ImageIndex = 5;
            this.btnPlay.ImageList = this.imageList1;
            this.btnPlay.Location = new System.Drawing.Point(272, 0);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(24, 24);
            this.btnPlay.TabIndex = 9;
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "backstep.png");
            this.imageList1.Images.SetKeyName(1, "begin.png");
            this.imageList1.Images.SetKeyName(2, "end.png");
            this.imageList1.Images.SetKeyName(3, "forwardstep.png");
            this.imageList1.Images.SetKeyName(4, "pause.png");
            this.imageList1.Images.SetKeyName(5, "play.png");
            this.imageList1.Images.SetKeyName(6, "rewind.png");
            this.imageList1.Images.SetKeyName(7, "stop.png");
            // 
            // btnPause
            // 
            this.btnPause.Enabled = false;
            this.btnPause.ImageIndex = 4;
            this.btnPause.ImageList = this.imageList1;
            this.btnPause.Location = new System.Drawing.Point(296, 0);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(24, 24);
            this.btnPause.TabIndex = 8;
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnStepForward
            // 
            this.btnStepForward.Enabled = false;
            this.btnStepForward.ImageIndex = 3;
            this.btnStepForward.ImageList = this.imageList1;
            this.btnStepForward.Location = new System.Drawing.Point(344, 0);
            this.btnStepForward.Name = "btnStepForward";
            this.btnStepForward.Size = new System.Drawing.Size(24, 24);
            this.btnStepForward.TabIndex = 7;
            this.btnStepForward.UseVisualStyleBackColor = true;
            this.btnStepForward.Click += new System.EventHandler(this.btnStepForward_Click);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.ImageIndex = 7;
            this.btnStop.ImageList = this.imageList1;
            this.btnStop.Location = new System.Drawing.Point(320, 0);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(24, 24);
            this.btnStop.TabIndex = 6;
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStepBack
            // 
            this.btnStepBack.Enabled = false;
            this.btnStepBack.ImageIndex = 0;
            this.btnStepBack.ImageList = this.imageList1;
            this.btnStepBack.Location = new System.Drawing.Point(248, 0);
            this.btnStepBack.Name = "btnStepBack";
            this.btnStepBack.Size = new System.Drawing.Size(24, 24);
            this.btnStepBack.TabIndex = 4;
            this.btnStepBack.UseVisualStyleBackColor = true;
            this.btnStepBack.Click += new System.EventHandler(this.btnStepBack_Click);
            // 
            // pgbEffectTime
            // 
            this.pgbEffectTime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgbEffectTime.Location = new System.Drawing.Point(376, 2);
            this.pgbEffectTime.Name = "pgbEffectTime";
            this.pgbEffectTime.Size = new System.Drawing.Size(129, 20);
            this.pgbEffectTime.Step = 1;
            this.pgbEffectTime.TabIndex = 2;
            // 
            // numSpeed
            // 
            this.numSpeed.DecimalPlaces = 2;
            this.numSpeed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSpeed.Location = new System.Drawing.Point(72, 2);
            this.numSpeed.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numSpeed.Name = "numSpeed";
            this.numSpeed.Size = new System.Drawing.Size(56, 20);
            this.numSpeed.TabIndex = 1;
            this.numSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Speed Mult:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Homeworld FX File (*.lua)|*.lua";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 361);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "HWRM FXTool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numZScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beamEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comboEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hyperspaceEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lensFlareEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lightEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ringEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sprayEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trailEffectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ImageList imageList1;
        internal System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStepForward;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStepBack;
        private System.Windows.Forms.ProgressBar pgbEffectTime;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.NumericUpDown numSpeed;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadShaderToolStripMenuItem;
        internal System.Windows.Forms.NumericUpDown numZScale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

