﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using HWShaderManifest;
using FXToolRM.Rendering;
using System.Collections.Generic;
using System;

namespace FXToolRM.Meshes
{
    public class AxesMesh : EditorMesh
    {
        private static float[] GetVertices()
        {
            return new float[] {
                0f, 0f, 0f,
                1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, 1f
            };
        }

        private static float[] GetColors()
        {
            return new float[] {
                1f, 1f, 1f, 1f,
                1f, 0f, 0f, 1f,
                0f, 1f, 0f, 1f,
                0f, 0f, 1f, 1f
            };
        }

        private static float[] GetUV0s()
        {
            return new float[8];
        }

        private static ushort[] GetIndices()
        {
            return new ushort[] {
                0, 1, 0, 2, 0, 3
            };
        }

        public AxesMesh() : base(GetVertices(), GetColors(), GetUV0s(), GetIndices(), PrimitiveType.Lines) { }
    }
}
