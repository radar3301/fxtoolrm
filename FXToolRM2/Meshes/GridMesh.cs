﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using HWShaderManifest;
using FXToolRM.Rendering;
using System.Collections.Generic;
using System;

namespace FXToolRM.Meshes
{
    public class GridMesh : EditorMesh
    {
        private static float[] GetVertices()
        {
            return new float[] {
                -2f, 0f, -2f, -2f, 0f, 2f, 2f, 0f, -2f, -1.8f, 0f, -2f, -1.8f, 0f, 2f, -2f, 0f, -1.8f, 2f, 0f, -1.8f, -1.6f, 0f, -2f,
                -1.6f, 0f, 2f, -2f, 0f, -1.6f, 2f, 0f, -1.6f, -1.4f, 0f, -2f, -1.4f, 0f, 2f, -2f, 0f, -1.4f, 2f, 0f, -1.4f, -1.2f, 0f, -2f,
                -1.2f, 0f, 2f, -2f, 0f, -1.2f, 2f, 0f, -1.2f, -1.0f, 0f, -2f, -1.0f, 0f, 2f, -2f, 0f, -1.0f, 2f, 0f, -1.0f, -0.8f, 0f, -2f,
                -0.8f, 0f, 2f, -2f, 0f, -0.8f, 2f, 0f, -0.8f, -0.6f, 0f, -2f, -0.6f, 0f, 2f, -2f, 0f, -0.6f, 2f, 0f, -0.6f, -0.4f, 0f, -2f,
                -0.4f, 0f, 2f, -2f, 0f, -0.4f, 2f, 0f, -0.4f, -0.2f, 0f, -2f, -0.2f, 0f, 2f, -2f, 0f, -0.2f, 2f, 0f, -0.2f, 0f, 0f, -2f,
                0f, 0f, 2.5f, -2f, 0f, 0f, 2.5f, 0f, 0f, 0.2f, 0f, -2f, 0.2f, 0f, 2f, -2f, 0f, 0.2f, 2f, 0f, 0.2f, 0.4f, 0f, -2f,
                0.4f, 0f, 2f, -2f, 0f, 0.4f, 2f, 0f, 0.4f, 0.6f, 0f, -2f, 0.6f, 0f, 2f, -2f, 0f, 0.6f, 2f, 0f, 0.6f, 0.8f, 0f, -2f,
                0.8f, 0f, 2f, -2f, 0f, 0.8f, 2f, 0f, 0.8f, 1.0f, 0f, -2f, 1.0f, 0f, 2f, -2f, 0f, 1.0f, 2f, 0f, 1.0f, 1.2f, 0f, -2f,
                1.2f, 0f, 2f, -2f, 0f, 1.2f, 2f, 0f, 1.2f, 1.4f, 0f, -2f, 1.4f, 0f, 2f, -2f, 0f, 1.4f, 2f, 0f, 1.4f, 1.6f, 0f, -2f,
                1.6f, 0f, 2f, -2f, 0f, 1.6f, 2f, 0f, 1.6f, 1.8f, 0f, -2f, 1.8f, 0f, 2f, -2f, 0f, 1.8f, 2f, 0f, 1.8f, 2f, 0f, 2f,
            };
        }

        private static float[] GetColors()
        {
            float[] color = new float[] { 1f, 1f, 1f, 0.5f };
            float[] colors = new float[320];
            int len = colors.Length / 4;
            for (int i = 0; i < len; ++i)
                Array.Copy(color, 0, colors, i * 4, 4);
            return colors;
        }

        private static float[] GetUV0s()
        {
            return new float[160];
        }

        private static ushort[] GetIndices()
        {
            return new ushort[] {
                 0,  1,  0,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
                19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
                39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58,
                59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78,
                 2, 79,  1, 79
            };
        }
        
        public GridMesh() : base(GetVertices(), GetColors(), GetUV0s(), GetIndices(), PrimitiveType.Lines) { }
    }
}
