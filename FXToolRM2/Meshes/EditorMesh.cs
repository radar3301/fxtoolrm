﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using HWShaderManifest;
using FXToolRM.Rendering;
using System.Collections.Generic;

namespace FXToolRM.Meshes
{
    public class EditorMesh
    {
        private int pos_buffer_id;
        private int col_buffer_id;
        private int uv0_buffer_id;
        private int ind_buffer_id;
        
        private int IndiceCount;
        private PrimitiveType PrimitiveType;

        public Texture Texture { get; set; } = null;

        public EditorMesh(float[] vertices, float[] colors, float[] uv0s, ushort[] indices, PrimitiveType primType)
        {
            pos_buffer_id = GL.GenBuffer();
            col_buffer_id = GL.GenBuffer();
            uv0_buffer_id = GL.GenBuffer();
            ind_buffer_id = GL.GenBuffer();
            
            GL.BindBuffer(BufferTarget.ArrayBuffer, pos_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
            
            GL.BindBuffer(BufferTarget.ArrayBuffer, col_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, colors.Length * sizeof(float), colors, BufferUsageHint.StaticDraw);
            
            GL.BindBuffer(BufferTarget.ArrayBuffer, uv0_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, uv0s.Length * sizeof(float), uv0s, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(ushort), indices, BufferUsageHint.StaticDraw);

            IndiceCount = indices.Length;
            PrimitiveType = primType;
        }

        public void Draw()
        {
            Scene.EditorShader.LinkAttrib(pos_buffer_id, "inPos", 3, false);
            Scene.EditorShader.LinkAttrib(col_buffer_id, "inCol", 4, false);
            Scene.EditorShader.LinkAttrib(uv0_buffer_id, "inUV0", 2, false);
            Scene.EditorShader.SetUniform("inMatM", Matrix4.Identity);
            if (Texture != null)
            {
                Scene.EditorShader.SetUniform("useTexture", 1);
                Texture.Attach();
            }
            else
            {
                Scene.EditorShader.SetUniform("useTexture", 0);
            }
            
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            Scene.EditorShader.Draw(PrimitiveType, IndiceCount, DrawElementsType.UnsignedShort, 0);
        }
    }
}
