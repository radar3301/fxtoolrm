﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using HWShaderManifest;
using FXToolRM.Rendering;

namespace FXToolRM.Meshes
{
    public class HyperspaceEdgeMesh
    {
        private Vector3 position = new Vector3();
        private float width = 0;
        private float height = 0;
        private Color4 color = new Color4();

        public Vector3 Position
        {
            get { return position; }
            set
            {
                if (position == value) return;
                position = value;
                UpdateVertexData();
            }
        }
        public float Width
        {
            get { return width; }
            set
            {
                if (width == value) return;
                width = value;
                UpdateVertexData();
            }
        }
        public float Height
        {
            get { return height; }
            set
            {
                if (height == value) return;
                height = value;
                UpdateVertexData();
            }
        }
        public Color4 Color
        {
            get { return color; }
            set
            {
                if (color == value) return;
                color = value;
                UpdateColorData();
            }
        }

        public Texture Texture { get; set; }

        private int pos_buffer_id;
        private int col_buffer_id;
        private int uv0_buffer_id;
        private int ind_buffer_id;

        public HyperspaceEdgeMesh()
        {
            pos_buffer_id = GL.GenBuffer();
            col_buffer_id = GL.GenBuffer();
            uv0_buffer_id = GL.GenBuffer();
            ind_buffer_id = GL.GenBuffer();

            UpdateVertexData();
            UpdateColorData();

            float[] uvs = new float[] {
                0, 1, 1, 1, 1, 0, 0, 0
            };

            GL.BindBuffer(BufferTarget.ArrayBuffer, uv0_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, uvs.Length * sizeof(float), uvs, BufferUsageHint.StaticDraw);

            ushort[] indices = new ushort[] {
                0, 1, 2, 3
            };

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(ushort), indices, BufferUsageHint.StaticDraw);
        }

        private void UpdateVertexData()
        {
            float[] vertices = new float[] {
                -width / 2 + position.X, -height / 2 + position.Y, position.Z,
                 width / 2 + position.X, -height / 2 + position.Y, position.Z,
                 width / 2 + position.X,  height / 2 + position.Y, position.Z,
                -width / 2 + position.X,  height / 2 + position.Y, position.Z
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, pos_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StreamDraw);
        }

        private void UpdateColorData()
        {
            float[] colors = new float[] {
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, col_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, colors.Length * sizeof(float), colors, BufferUsageHint.StreamDraw);
        }

        public void Draw()
        {
            Surface surface = ShaderManifest.GetSurface("fx_hyperspace");

            surface.LinkAttrib("inPos", pos_buffer_id, 3, false);
            surface.LinkAttrib("inCol0", col_buffer_id, 4, false);
            surface.LinkAttrib("inUV0", uv0_buffer_id, 2, false);

            if (Texture != null)
                surface.BindTexture("inTexHyper", Texture.ID);
            else
                surface.BindTexture("inTexHyper", 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            surface.Draw(BeginMode.LineLoop, 4, DrawElementsType.UnsignedShort, 0);
        }
    }
}
