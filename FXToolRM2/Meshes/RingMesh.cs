﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using HWShaderManifest;
using FXToolRM.Rendering;
using System;

namespace FXToolRM.Meshes
{
    public class RingMesh
    {
        private bool billboard = false;
        private Vector3 position = new Vector3();
        private float scale = 1;
        private Color4 color = new Color4();
        private Vector3 rotation = new Vector3();
        private float depth;
        private int texFrame = -1;

        public bool Billboard
        {
            get { return billboard; }
            set
            {
                if (billboard == value) return;
                billboard = value;
                UpdateMisc0Data();
            }
        }
        public Vector3 Position
        {
            get { return position; }
            set
            {
                if (position == value) return;
                position = value;
                UpdateVertexData();
            }
        }
        public float Scale
        {
            get { return scale; }
            set
            {
                if (scale == value) return;
                scale = value;
                UpdateMisc0Data();
            }
        }
        public Color4 Color
        {
            get { return color; }
            set
            {
                if (color == value) return;
                color = value;
                UpdateColorData();
            }
        }
        public Vector3 Rotation
        {
            get { return rotation; }
            set
            {
                if (rotation == value) return;
                rotation = value;
                UpdateMisc0Data();
            }
        }
        public float DepthOffset
        {
            get { return depth; }
            set
            {
                if (depth == value) return;
                depth = value;
                UpdateMisc1Data();
            }
        }

        public Texture Texture { get; set; }
        public int TextureFrame
        {
            get
            {
                return texFrame;
            }
            set
            {
                if (texFrame == value) return;
                texFrame = value;
                UpdateUVData();
            }
        }

        private int pos_buffer_id;
        private int col_buffer_id;
        private int uv0_buffer_id;
        private int misc0_buffer_id;
        private int misc1_buffer_id;
        private int ind_buffer_id;

        public RingMesh()
        {
            pos_buffer_id = GL.GenBuffer();
            col_buffer_id = GL.GenBuffer();
            uv0_buffer_id = GL.GenBuffer();
            misc0_buffer_id = GL.GenBuffer();
            misc1_buffer_id = GL.GenBuffer();
            ind_buffer_id = GL.GenBuffer();

            UpdateVertexData();
            UpdateColorData();
            UpdateUVData();
            UpdateMisc0Data();
            UpdateMisc1Data();

            ushort[] indices = new ushort[] {
                0, 1, 2,
                3, 0, 2
            };

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(ushort), indices, BufferUsageHint.StaticDraw);
        }

        private void UpdateVertexData()
        {
            Vector3 v0 = position;
            Vector3 v1 = position;
            Vector3 v2 = position;
            Vector3 v3 = position;

            float[] vertices = new float[] {
                v0.X, v0.Y, v0.Z, 1,
                v1.X, v1.Y, v1.Z, 1,
                v2.X, v2.Y, v2.Z, 1,
                v3.X, v3.Y, v3.Z, 1
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, pos_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StreamDraw);
        }

        private void UpdateColorData()
        {
            float[] colors = new float[] {
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, col_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, colors.Length * sizeof(float), colors, BufferUsageHint.StreamDraw);
        }

        private void UpdateUVData()
        {
            float[] uvs = new float[] { 0, 1, 0, 0, 1, 0, 1, 1 };

            if (Texture != null && Texture is AnimatedTexture)
            {
                AnimatedTexture.TextureFrame frame = ((AnimatedTexture)Texture).GetFrame(texFrame);
                uvs = frame.UVData;
                //for (int i = 0; i < uvs.Length; ++i)
                //    uvs[i] *= xform[i];
            }

            GL.BindBuffer(BufferTarget.ArrayBuffer, uv0_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, uvs.Length * sizeof(float), uvs, BufferUsageHint.StreamDraw);
        }

        private void UpdateMisc0Data()
        {
            Matrix3 rotMat = Matrix3.CreateRotationZ(rotation.Z) *
                Matrix3.CreateRotationY(rotation.Y) *
                Matrix3.CreateRotationX(rotation.X);
            Vector3 v0 = rotMat * new Vector3(-scale / 2, -scale / 2, 0);
            Vector3 v1 = rotMat * new Vector3(scale / 2, -scale / 2, 0);
            Vector3 v2 = rotMat * new Vector3(scale / 2, scale / 2, 0);
            Vector3 v3 = rotMat * new Vector3(-scale / 2, scale / 2, 0);

            float[] misc0 = new float[] {
                v0.X, v0.Y, v0.Z, Billboard ? 1 : 0,
                v1.X, v1.Y, v1.Z, Billboard ? 1 : 0,
                v2.X, v2.Y, v2.Z, Billboard ? 1 : 0,
                v3.X, v3.Y, v3.Z, Billboard ? 1 : 0
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, misc0_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, misc0.Length * sizeof(float), misc0, BufferUsageHint.StreamDraw);
        }

        private void UpdateMisc1Data()
        {
            float[] misc1 = new float[] {
                0.5f, 0.5f, 0, depth,
                0.5f, 0.5f, 0, depth,
                0.5f, 0.5f, 0, depth,
                0.5f, 0.5f, 0, depth,
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, misc1_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, misc1.Length * sizeof(float), misc1, BufferUsageHint.StreamDraw);
        }

        public void Draw()
        {
            Surface surface = ShaderManifest.GetSurface("fx_ring_basic");

            surface.LinkAttrib("inPos", pos_buffer_id, 4, false);
            surface.LinkAttrib("inCol0", col_buffer_id, 4, false);
            surface.LinkAttrib("inUV0", uv0_buffer_id, 2, false);
            surface.LinkAttrib("inMisc0", misc0_buffer_id, 4, false);
            surface.LinkAttrib("inMisc1", misc1_buffer_id, 4, false);

            if (Texture != null)
            {
                if (Texture is AnimatedTexture)
                {
                    Texture tex = ((AnimatedTexture)Texture).GetFrame(texFrame).Texture;
                    surface.BindTexture("inTexRing", tex.ID);
                }
                else
                    surface.BindTexture("inTexRing", Texture.ID);
            }
            else
                surface.BindTexture("inTexRing", 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            //GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedShort, 0);
            surface.Draw(BeginMode.Triangles, 6, DrawElementsType.UnsignedShort, 0);
        }
    }
}
