﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using HWShaderManifest;
using FXToolRM.Rendering;
using System;

namespace FXToolRM.Meshes
{
    public class BeamMesh
    {
        private Vector3 position = new Vector3();
        private float length = 1f;
        private float width = 1f;

        private Color4 color = new Color4();
        private float texUOff = 0f;
        private float texURep = 1f;
        private int texFrame = -1;

        public Vector3 Position
        {
            get { return position; }
            set
            {
                if (position == value) return;
                position = value;
                UpdateVertexData();
            }
        }
        public float Length
        {
            get { return length; }
            set
            {
                if (length == value) return;
                length = value;
                UpdateVertexData();
            }
        }
        public float Width
        {
            get { return width; }
            set
            {
                if (width == value) return;
                width = value;
                UpdateVertexData();
            }
        }
        public Color4 Color
        {
            get { return color; }
            set
            {
                if (color == value) return;
                color = value;
                UpdateColorData();
            }
        }

        public Texture Texture { get; set; }

        public float Texture_U_Offset
        {
            get { return texUOff; }
            set
            {
                if (texUOff == value) return;
                texUOff = value;
                UpdateUV0Data();
            }
        }
        public float Texture_U_Repeat
        {
            get { return texURep; }
            set
            {
                if (texURep == value) return;
                texURep = value;
                UpdateUV0Data();
            }
        }
        public int TextureFrame
        {
            get
            {
                return texFrame;
            }
            set
            {
                if (texFrame == value) return;
                texFrame = value;
                UpdateUV0Data();
            }
        }

        private int pos_buffer_id;
        private int col_buffer_id;
        private int uv0_buffer_id;
        private int uv1_buffer_id;
        private int ind_buffer_id;

        public BeamMesh()
        {
            pos_buffer_id = GL.GenBuffer();
            col_buffer_id = GL.GenBuffer();
            uv0_buffer_id = GL.GenBuffer();
            uv1_buffer_id = GL.GenBuffer();
            ind_buffer_id = GL.GenBuffer();

            UpdateVertexData();
            UpdateColorData();
            UpdateUV0Data();
            UpdateUV1Data();

            ushort[] indices = new ushort[] {
                0, 1, 2,
                3, 0, 2
            };

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(ushort), indices, BufferUsageHint.StaticDraw);
        }

        private void UpdateVertexData()
        {
            Vector3 v0 = new Vector3(width / 2, 0, length) + position;
            Vector3 v1 = new Vector3(width / 2, 0, 0) + position;
            Vector3 v2 = new Vector3(-width / 2, 0, 0) + position;
            Vector3 v3 = new Vector3(-width / 2, 0, length) + position;

            float[] vertices = new float[] {
                v0.X, v0.Y, v0.Z, 0,
                v1.X, v1.Y, v1.Z, 0,
                v2.X, v2.Y, v2.Z, 0,
                v3.X, v3.Y, v3.Z, 0
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, pos_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StreamDraw);
        }

        private void UpdateColorData()
        {
            float[] colors = new float[] {
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A,
                color.R, color.G, color.B, color.A
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, col_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, colors.Length * sizeof(float), colors, BufferUsageHint.StreamDraw);
        }

        private void UpdateUV0Data()
        {
            //float[] uv0 = new float[] { 1, 0, 0, 0, 0, 1, 1, 1 };
            float[] uv0 = new float[] { texURep - texUOff, 0, 0 - texUOff, 0, 0 - texUOff, 1, texURep - texUOff, 1 };
            //float[] uv0 = new float[] { texUOff, 0, texUOff + texURep, 0, texUOff + texURep, 1, texUOff, 1 };

            //if (Texture != null && Texture is AnimatedTexture)
            //{
            //    AnimatedTexture.TextureFrame frame = ((AnimatedTexture)Texture).GetFrame(texFrame);
            //    uv0 = frame.UVData;
            //}

            GL.BindBuffer(BufferTarget.ArrayBuffer, uv0_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, uv0.Length * sizeof(float), uv0, BufferUsageHint.StreamDraw);
        }

        private void UpdateUV1Data()
        {
            float[] uv1 = new float[] { 1, 0, 0, 0, 0, 1, 1, 1 };

            if (Texture != null && Texture is AnimatedTexture)
            {
                AnimatedTexture.TextureFrame frame = ((AnimatedTexture)Texture).GetFrame(texFrame);
                uv1 = frame.UVData;
            }

            GL.BindBuffer(BufferTarget.ArrayBuffer, uv1_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, uv1.Length * sizeof(float), uv1, BufferUsageHint.StreamDraw);
        }

        public void Draw()
        {
            Surface surface = ShaderManifest.GetSurface("fx_beam_basic");

            Matrix3 rot = Matrix3.CreateRotationZ((float)Math.Atan2(Scene.Camera.Direction.X, Scene.Camera.Direction.Y));
            Vector3 v0 = rot * new Vector3(width / 2, 0, length) + position;
            Vector3 v1 = rot * new Vector3(width / 2, 0, 0) + position;
            Vector3 v2 = rot * new Vector3(-width / 2, 0, 0) + position;
            Vector3 v3 = rot * new Vector3(-width / 2, 0, length) + position;

            float[] vertices = new float[] {
                v0.X, v0.Y, v0.Z * Scene.Camera.Scale.Z, 0,
                v1.X, v1.Y, v1.Z * Scene.Camera.Scale.Z, 0,
                v2.X, v2.Y, v2.Z * Scene.Camera.Scale.Z, 0,
                v3.X, v3.Y, v3.Z * Scene.Camera.Scale.Z, 0
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, pos_buffer_id);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StreamDraw);

            surface.LinkAttrib("inPos", pos_buffer_id, 4, false);
            surface.LinkAttrib("inCol0", col_buffer_id, 4, false);
            surface.LinkAttrib("inUV0", uv0_buffer_id, 2, false);
            surface.LinkAttrib("inUV1", uv1_buffer_id, 2, false);

            if (Texture != null)
            {
                if (Texture is AnimatedTexture)
                {
                    Texture tex = ((AnimatedTexture)Texture).GetFrame(texFrame).Texture;
                    surface.BindTexture("inTexBeam", tex.ID);
                }
                else
                    surface.BindTexture("inTexBeam", Texture.ID);
            }
            else
                surface.BindTexture("inTexBeam", 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer_id);
            surface.Draw(BeginMode.Triangles, 6, DrawElementsType.UnsignedShort, 0);
        }
    }
}
