﻿using System.Collections.Generic;
using FXToolRM.FX;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System;
using System.Windows.Forms;

namespace FXToolRM
{
    static class Utils
    {
        public static T First<T>(this IList<T> list)
        {
            return list[0];
        }

        public static T Last<T>(this IList<T> list)
        {
            return list[list.Count - 1];
        }

        public static bool IsBetween(float x, float a, float b)
        {
            return a <= x && x <= b;
        }

        public static float Integrate(float t, FloatKeyframes frames)
        {
            if (t <= frames.GetTimeAtIndex(0)) return frames.GetValueAtIndex(0);
            float result = 0f;
            for (int i = 0; i < frames.Count - 1; i++)
            {
                if (t < frames.GetTimeAtIndex(i))
                    break;
                else if (IsBetween(t, frames.GetTimeAtIndex(i), frames.GetTimeAtIndex(i + 1)))
                {
                    float a = frames.GetTimeAtIndex(i);
                    float b = frames.GetValueAtIndex(i);
                    float c = t;
                    float d = frames[t];
                    float k = (d - b) / (c - a);
                    result += (c - a) * (k * (a + c) + 2 * b) / 2;
                }
                else
                {
                    float a = frames.GetTimeAtIndex(i);
                    float b = frames.GetValueAtIndex(i);
                    float c = frames.GetTimeAtIndex(i + 1);
                    float d = frames.GetValueAtIndex(i + 1);
                    float k = (d - b) / (c - a);
                    result += (c - a) * (k * (a + c) + 2 * b) / 2;
                }
            }
            return result;
        }

        private static float Integrate(float m, float k, float a, float b)
        {
            // integrate `mx + k` from a to b
            return (b - a) * (m * (a + b) + 2 * k) / 2;
        }

        public static Renderer GetRenderer(Effect effect)
        {
            if (effect is HyperspaceEffect)
                return new HyperspaceRenderer(effect as HyperspaceEffect);
            else if (effect is RingEffect)
                return new RingRenderer(effect as RingEffect);
            else if (effect is BeamEffect)
                return new BeamRenderer(effect as BeamEffect);
            else if (effect is SprayEffect)
                return new SprayRenderer(effect as SprayEffect);
            else if (effect is ComboEffect)
                return new ComboRenderer(effect as ComboEffect);
            else
                return null;
        }

        public static void ApplyBlending(EffectBlending blending)
        {
            switch (blending)
            {
                case EffectBlending.AdditiveAlphaBlend:
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.One);
                    break;
                case EffectBlending.AdditiveBlend:
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
                    break;
                case EffectBlending.AlphaBlend:
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactorSrc.DstAlpha, BlendingFactorDest.One);
                    break;
                case EffectBlending.MultiplyBlend:
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactorSrc.DstColor, BlendingFactorDest.Zero);
                    break;
                case EffectBlending.NoBlending:
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.Zero);
                    break;
            }
        }

        public static int[] GetBlending(EffectBlending blending)
        {
            switch (blending)
            {
                case EffectBlending.AdditiveAlphaBlend:
                    return new int[] { (int)BlendingFactorSrc.SrcAlpha, (int)BlendingFactorDest.One };
                case EffectBlending.AdditiveBlend:
                    return new int[] { (int)BlendingFactorSrc.One, (int)BlendingFactorDest.One };
                case EffectBlending.AlphaBlend:
                    return new int[] { (int)BlendingFactorSrc.DstAlpha, (int)BlendingFactorDest.One };
                case EffectBlending.MultiplyBlend:
                    return new int[] { (int)BlendingFactorSrc.DstColor, (int)BlendingFactorDest.Zero };
                case EffectBlending.NoBlending:
                    return new int[] { (int)BlendingFactorSrc.One, (int)BlendingFactorDest.Zero };
            }
            return new int[] { 0, 0 };
        }

        public static Vector4 test(Vector4 inPos, Vector4 inMisc0, Vector4 inMisc1, Vector3 inCamera, Matrix4 inMatV, Matrix4 inMatP)
        {
            Vector3 usePos = inPos.Xyz;
            Vector3 useOfs = inMisc0.Xyz;

            Vector3 eyeZ = Vector3.Normalize(usePos - inCamera);
            usePos -= eyeZ * inMisc1.W; // FX forced depth offset

            if (inMisc0.W > 0.0)    // Billboard?
            {
                Vector3 billU = new Vector3(inMatV.Row0.Y, inMatV.Row1.Y, inMatV.Row2.Y);
                Vector3 billZ = eyeZ;
                Vector3 billX = Vector3.Normalize(Vector3.Cross(billU, billZ));
                Vector3 billY = Vector3.Cross(billZ, billX);

                Matrix3 billB;
                billB.Row0 = billX;
                billB.Row1 = billY;
                billB.Row2 = billZ;

                useOfs = billB * useOfs;
            }

            Vector3 centerPos = usePos;
            usePos += useOfs;
            
            Vector4 posT = inMatV * new Vector4(usePos, 1);
            return inMatP * posT;
        }
    }

}
