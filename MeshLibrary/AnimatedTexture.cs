﻿using System;
using System.IO;
using DevILSharp;
using OpenTK.Graphics.OpenGL;
using NLua;

namespace RenderLibrary
{
    public class AnimatedTexture : Texture
    {
        private float FPS;
        private int StartFrame;
        private int LoopCount;
        private int LayoutX;
        private int LayoutY;
        private Texture[] Textures;

        private int currentFrame = 0;

        public int CurrentFrame
        {
            get
            {
                return currentFrame;
            }
            set
            {
                if (value < 0) currentFrame = 0;
                if (value >= Textures.Length) value = Textures.Length - 1;
                currentFrame = value;
            }
        }

        public override int ID
        {
            get
            {
                return Textures[CurrentFrame].ID;
            }
        }

        private AnimatedTexture() { }

        public AnimatedTexture(string filename) : base(filename, 0)
        {
            LoadAnimation(filename);
        }

        private void LoadAnimation(string filename)
        {
            Lua Interpreter = new Lua();
            object[] results = Interpreter.DoFile(filename);
            FPS = (float)Interpreter.GetNumber("fps");
            currentFrame = StartFrame = (int)Interpreter.GetNumber("startFrame");
            LoopCount = (int)Interpreter.GetNumber("loopCount");

            LuaTable texture = Interpreter.GetTable("texture");
            LuaTable layout = Interpreter.GetTable("layout");
            if (layout != null)
            { 
                LayoutX = (int)(double)layout[1];
                LayoutY = (int)(double)layout[2];
            }
            LuaTable textureLayout = Interpreter.GetTable("textureLayout");
            
            if (texture != null)
            {
                Textures = new Texture[texture.Values.Count];
                int i = 0;
                foreach (double key in texture.Keys)
                {
                    string tex = (string)texture[key];
                    string path = tex.Replace("data:", Config.DataPath).Replace('/', '\\');
                    Textures[i++] = new Texture(path);
                }
            }
            else if (layout != null)
            {

            }
            Interpreter.Close();
        }
    }
}