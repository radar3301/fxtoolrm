﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace RenderLibrary
{
    public struct Vertex
    {
        public Vector3 Pos;
        public Vector2 UV;
        public Vector3 Color;

        public Vertex(Vector3 pos, Vector2 uv) : this()
        {
            Pos = pos;
            UV = uv;
            Color = new Vector3(0.5f);
        }

        public Vertex(Vector3 pos, Vector2 uv, Vector3 col) : this()
        {
            Pos = pos;
            UV = uv;
            Color = col;
        }
    }
}
