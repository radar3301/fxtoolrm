﻿using System;
using System.IO;
using DevILSharp;
using OpenTK.Graphics.OpenGL;
using System.Reflection;

namespace RenderLibrary
{
    public class Texture
    {
        protected static bool ILInit = false;
        private static Texture missingTexture;
        public static Texture DefaultTexture { get { return missingTexture; } }

        private int id;

        public virtual int ID
        {
            get
            {
                return id;
            }
        }
        public string Filename { get; }

        protected Texture() { }

        protected Texture(string filename, int id)
        {
            Filename = filename;
            this.id = id;
        }

        public Texture(string filename) : this(filename, LoadImage(filename)) { }

        public Texture(Stream stream) : this(stream.ToString(), LoadImage(stream)) { }

        public void Attach()
        {
            GL.BindTexture(TextureTarget.Texture2D, ID);
        }

        private static int RawLoadImage(int width, int height, PixelFormat pixelFormat, PixelType pixeltype, IntPtr ptr)
        {
            int texID = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texID);

            //Anisotropic filtering
            float maxAniso;
            GL.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out maxAniso);
            GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, maxAniso);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.SrgbAlpha, width, height, 0, pixelFormat, pixeltype, ptr);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            return texID;
        }

        private static int LoadImage(string filename)
        {
            if (!ILInit) Init();

            bool exists = File.Exists(filename);
            bool flip = false;

            if (!exists)
                return DefaultTexture.ID;

            if (Path.GetExtension(filename).ToLower() != ".tga")
                flip = true;

            int img = IL.GenImage();
            IL.BindImage(img);
            IL.LoadImage(filename);

            if (flip)
                ILU.FlipImage();

            IL.ConvertImage(ChannelFormat.RGBA, ChannelType.UnsignedByte);

            int imageWidth = IL.GetInteger(IntName.ImageWidth);
            int imageHeight = IL.GetInteger(IntName.ImageHeight);

            int texID = RawLoadImage(imageWidth, imageHeight, (PixelFormat)IL.GetInteger(IntName.ImageFormat), PixelType.UnsignedByte, IL.GetData());

            IL.DeleteImage(img);
            IL.BindImage(0);

            return texID;
        }

        private static int LoadImage(Stream stream)
        {
            if (!ILInit) Init();
            bool flip = false;

            int img = IL.GenImage();
            IL.BindImage(img);
            IL.LoadStream(stream);

            if (flip)
                ILU.FlipImage();

            IL.ConvertImage(ChannelFormat.RGBA, ChannelType.UnsignedByte);

            int imageWidth = IL.GetInteger(IntName.ImageWidth);
            int imageHeight = IL.GetInteger(IntName.ImageHeight);
            double widthLog2 = Math.Log(imageWidth) / Math.Log(2);
            double heightLog2 = Math.Log(imageHeight) / Math.Log(2);

            int texID = RawLoadImage(imageWidth, imageHeight, (PixelFormat)IL.GetInteger(IntName.ImageFormat), PixelType.UnsignedByte, IL.GetData());

            IL.DeleteImage(img);
            IL.BindImage(0);

            return texID;
        }

        public static void Init()
        {
            IL.Init();
            ILInit = true;
            missingTexture = new Texture(
                Assembly.GetExecutingAssembly().GetManifestResourceStream("MeshLibrary.resources.missing.tga"));
        }

        public static void Close()
        {
            IL.ShutDown();
        }
    }
}