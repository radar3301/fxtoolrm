﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK;
using System.Drawing;

namespace RenderLibrary
{
    public class Camera
    {
        private GLControl control;

        public Camera(GLControl control)
        {
            this.control = control;
        }

        public float Distance = 5;
        public Vector3 Position = Vector3.UnitZ * 5;
        private Vector3 rotation = Vector3.Zero;
        public Vector3 UpVector = Vector3.UnitY;
        public Vector3 LookAt = Vector3.Zero;

        public float NearClipDistance { get; set; } = 0.01f;
        public float FarClipDistance { get; set; } = 1000f;
        public float FieldOfView { get; set; } = (float)(Math.PI / 2.0);
        public float Width => control.Width;
        public float Height => control.Height;

        public Matrix4 ViewMatrix
        {
            get { return Matrix4.LookAt(Position, LookAt, UpVector); }
        }

        public Matrix4 RotationMatrix {
            get {
                return Matrix4.CreateRotationZ(rotation.Z) *
                    Matrix4.CreateRotationX(rotation.X) *
                    Matrix4.CreateRotationY(rotation.Y);
            }
        }

        private Point SaveMousePos;

        public void MouseDown(MouseEventArgs e)
        {
            bool left = (e.Button & MouseButtons.Left) == MouseButtons.Left;
            bool right = (e.Button & MouseButtons.Right) == MouseButtons.Right;
            if (left && right)
            {
            }
            else if (left && !right)
            {
                SaveMousePos = e.Location;
            }
            else if (!left && right)
            {
                SaveMousePos = e.Location;
            }
        }

        public void MouseMove(MouseEventArgs e)
        {
            bool left = (e.Button & MouseButtons.Left) == MouseButtons.Left;
            bool right = (e.Button & MouseButtons.Right) == MouseButtons.Right;
            if (left && right)
            {
                //Pan();
            }
            else if (left && !right)
            {
                Point NewMousePos = e.Location;
                Vector2 Delta = new Vector2(NewMousePos.X - SaveMousePos.X, NewMousePos.Y - SaveMousePos.Y);
                SaveMousePos = NewMousePos;

                rotation.X -= 5 * Delta.Y / Height;
                rotation.Y -= 5 * Delta.X / Width;
                if (Math.Abs(rotation.X) > Math.PI / 2)
                    rotation.X = (float)(Math.Sign(rotation.X) * Math.PI / 2);
                if (Math.Abs(rotation.Y) > Math.PI)
                    rotation.Y += (float)(-Math.Sign(rotation.Y) * 2 * Math.PI);
                
                Position = (Matrix4.CreateTranslation(Vector3.UnitZ * Distance) * RotationMatrix).Row3.Xyz;
                UpVector = (Matrix4.CreateTranslation(Vector3.UnitY) * RotationMatrix).Row3.Xyz;
                Position += LookAt;
            }
            else if (!left && right)
            {
                Point NewMousePos = e.Location;
                Vector2 Delta = new Vector2(NewMousePos.X - SaveMousePos.X, NewMousePos.Y - SaveMousePos.Y);
                SaveMousePos = NewMousePos;

                Distance -= 5 * Delta.Y / Height;
                if (Distance < 0.01) Distance = 0.01f;

                Position -= LookAt;
                Position = Vector3.Normalize(Position);
                Position = Vector3.Multiply(Position, Distance);
                Position += LookAt;
            }
        }

        public void MouseWheel(MouseEventArgs e)
        {
            FieldOfView -= (float)(Math.Sign(e.Delta) * Math.PI / 36);
            if (FieldOfView < Math.PI / 4) FieldOfView = (float)(Math.PI / 4);
            if (FieldOfView > Math.PI / 2) FieldOfView = (float)(Math.PI / 2);
        }
    }
}
