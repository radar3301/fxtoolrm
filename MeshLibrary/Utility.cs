﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace RenderLibrary
{
    internal static class Utility
    {
        public static void BindBufferData(string type, int buffer, Vector3[] data)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, buffer);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(data.Length * Vector3.SizeInBytes), data, BufferUsageHint.StaticDraw);
            GetGLError(type);
        }

        public static void BindBufferData(string type, int buffer, Vector2[] data)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, buffer);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(data.Length * Vector2.SizeInBytes), data, BufferUsageHint.StaticDraw);
            GetGLError(type);
        }

        public static void BindBufferData(string type, int buffer, ushort[] data)
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, buffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(data.Length * sizeof(ushort)), data, BufferUsageHint.StaticDraw);
            GetGLError(type);
        }

        public static void LinkAttrib1(int attr, int buffer, bool normalized)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, buffer);
            GL.VertexAttribPointer(attr, 1, VertexAttribPointerType.Float, normalized, 0, 0);
            GL.EnableVertexAttribArray(attr);
            GetGLError("LinkAttrib");
        }

        public static void LinkAttrib2(int attr, int buffer, bool normalized)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, buffer);
            GL.VertexAttribPointer(attr, 2, VertexAttribPointerType.Float, normalized, 0, 0);
            GL.EnableVertexAttribArray(attr);
            GetGLError("LinkAttrib");
        }

        public static void LinkAttrib3(int attr, int buffer, bool normalized)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, buffer);
            GL.VertexAttribPointer(attr, 3, VertexAttribPointerType.Float, normalized, 0, 0);
            GL.EnableVertexAttribArray(attr);
            GetGLError("LinkAttrib");
        }

        public static void GetGLError(string type)
        {
            ErrorCode code = GL.GetError();
            if (code != ErrorCode.NoError)
                Console.WriteLine(type + ": " + code);
        }
    }
}
