﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace RenderLibrary
{
    public enum SceneUniform
    {
        ModelMatrix,
        ViewMatrix,
        ProjectionMatrix
    }

    public static class Scene
    {
        public static Shader Shader { get; set; }
        public static Camera Camera { get; set; }

        public static Dictionary<string, Mesh> Meshes { get; } = new Dictionary<string, Mesh>();
        public static List<Model> Models { get; } = new List<Model>();

        public static Model FloorModel;
        //public static Model Axes;

        public static void LoadMesh(string filename)
        {

        }

        public static void Draw()
        {
            Utility.GetGLError("Scene Pre-Draw");
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(
                Camera.FieldOfView, Camera.Width / Camera.Height, Camera.NearClipDistance, Camera.FarClipDistance);
            Matrix4 view = Camera.ViewMatrix;

            GL.UseProgram(Shader.ProgramID);
            Shader.SetUniform("inMatP", ref projection);
            Shader.SetUniform("inMatV", ref view);

            foreach (Model m in Models)
                m.Draw();
            Utility.GetGLError("Scene Post-Draw");
        }
    }
}