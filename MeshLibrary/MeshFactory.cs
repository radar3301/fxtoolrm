﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace RenderLibrary
{
    public static class MeshFactory
    {
        public static Mesh CreateGrid(int divisions = 20)
        {
            if (divisions < 2) divisions = 2;

            string name = "Grid" + divisions;
            if (Scene.Meshes.ContainsKey(name)) return Scene.Meshes[name];

            List<Vertex> vertices = new List<Vertex>();
            List<ushort> indices = new List<ushort>();

            Vertex v;

            float step = 10f / divisions;
            for (float i = -10f; i < (10f - step / 2); i += step)
            {
                v = new Vertex(new Vector3(i, 0f, -10f), new Vector2(), new Vector3(0.5f));
                if (!vertices.Contains(v))
                {
                    indices.Add((ushort)vertices.Count);
                    Console.Out.Write((ushort)vertices.Count + ",");
                    vertices.Add(v);
                }
                else
                {
                    indices.Add((ushort)vertices.IndexOf(v));
                    Console.Out.Write(vertices.IndexOf(v) + ",");
                }

                v = new Vertex(new Vector3(i, 0f, 10f), new Vector2(), new Vector3(0.5f));
                if (!vertices.Contains(v))
                {
                    indices.Add((ushort)vertices.Count);
                    Console.Out.Write((ushort)vertices.Count + ",");
                    vertices.Add(v);
                }
                else
                {
                    indices.Add((ushort)vertices.IndexOf(v));
                    Console.Out.Write(vertices.IndexOf(v) + ",");
                }

                v = new Vertex(new Vector3(-10f, 0f, i), new Vector2(), new Vector3(0.5f));
                if (!vertices.Contains(v))
                {
                    indices.Add((ushort)vertices.Count);
                    Console.Out.Write((ushort)vertices.Count + ",");
                    vertices.Add(v);
                }
                else
                {
                    indices.Add((ushort)vertices.IndexOf(v));
                    Console.Out.Write(vertices.IndexOf(v) + ",");
                }

                v = new Vertex(new Vector3(10f, 0f, i), new Vector2(), new Vector3(0.5f));
                if (!vertices.Contains(v))
                {
                    indices.Add((ushort)vertices.Count);
                    Console.Out.Write((ushort)vertices.Count + ",");
                    vertices.Add(v);
                }
                else
                {
                    indices.Add((ushort)vertices.IndexOf(v));
                    Console.Out.Write(vertices.IndexOf(v) + ",");
                }
            }

            v = new Vertex(new Vector3(10f, 0f, -10f), new Vector2(), new Vector3(0.5f));
            if (!vertices.Contains(v))
            {
                indices.Add((ushort)vertices.Count);
                Console.Out.Write((ushort)vertices.Count + ",");
                vertices.Add(v);
            }
            else
            {
                indices.Add((ushort)vertices.IndexOf(v));
                Console.Out.Write(vertices.IndexOf(v) + ",");
            }

            v = new Vertex(new Vector3(10f, 0f, 10f), new Vector2(), new Vector3(0.5f));
            if (!vertices.Contains(v))
            {
                indices.Add((ushort)vertices.Count);
                Console.Out.Write((ushort)vertices.Count + ",");
                vertices.Add(v);
            }
            else
            {
                indices.Add((ushort)vertices.IndexOf(v));
                Console.Out.Write(vertices.IndexOf(v) + ",");
            }

            v = new Vertex(new Vector3(-10f, 0f, 10f), new Vector2(), new Vector3(0.5f));
            if (!vertices.Contains(v))
            {
                indices.Add((ushort)vertices.Count);
                Console.Out.Write((ushort)vertices.Count + ",");
                vertices.Add(v);
            }
            else
            {
                indices.Add((ushort)vertices.IndexOf(v));
                Console.Out.Write(vertices.IndexOf(v) + ",");
            }

            v = new Vertex(new Vector3(10f, 0f, 10f), new Vector2(), new Vector3(0.5f));
            if (!vertices.Contains(v))
            {
                indices.Add((ushort)vertices.Count);
                Console.Out.Write((ushort)vertices.Count + ",");
                vertices.Add(v);
            }
            else
            {
                indices.Add((ushort)vertices.IndexOf(v));
                Console.Out.Write(vertices.IndexOf(v) + ",");
            }

            Mesh m = new Mesh(name, vertices.ToArray(), indices.ToArray(), PrimitiveType.Lines);
            m.IsVertexColored = true;
            return m;
        }

        public static Mesh CreateSquareOutline()
        {
            string name = "SquareOutline";
            if (Scene.Meshes.ContainsKey(name))
                return Scene.Meshes[name];

            List<Vertex> vertices = new List<Vertex>();
            List<ushort> indices = new List<ushort>();

            vertices.Add(new Vertex(new Vector3(-0.5f, -0.5f, 0f), new Vector2(0.0001f, 0.9999f)));
            vertices.Add(new Vertex(new Vector3(0.5f, -0.5f, 0f), new Vector2(0.9999f, 0.9999f)));
            vertices.Add(new Vertex(new Vector3(0.5f, 0.5f, 0f), new Vector2(0.9999f, 0.0001f)));
            vertices.Add(new Vertex(new Vector3(-0.5f, 0.5f, 0f), new Vector2(0.0001f, 0.0001f)));
            indices.Add(0); indices.Add(1); indices.Add(2); indices.Add(3);

            Mesh m = new Mesh(name, vertices.ToArray(), indices.ToArray(), PrimitiveType.LineLoop);
            m.IsTextured = false;
            m.IsVertexColored = true;
            return m;
        }

        public static Mesh CreateAxes()
        {
            string name = "Axes";
            if (Scene.Meshes.ContainsKey(name)) return Scene.Meshes[name];

            List<Vertex> vertices = new List<Vertex>();
            List<ushort> indices = new List<ushort>();
            
            vertices.Add(new Vertex(Vector3.Zero, Vector2.Zero, Vector3.UnitZ));
            vertices.Add(new Vertex(Vector3.UnitX, Vector2.Zero, Vector3.UnitZ));
            vertices.Add(new Vertex(Vector3.Zero, Vector2.Zero, Vector3.UnitY));
            vertices.Add(new Vertex(Vector3.UnitY, Vector2.Zero, Vector3.UnitY));
            vertices.Add(new Vertex(Vector3.Zero, Vector2.Zero, Vector3.UnitX));
            vertices.Add(new Vertex(Vector3.UnitZ, Vector2.Zero, Vector3.UnitX));
            indices.Add(0); indices.Add(1);
            indices.Add(2); indices.Add(3);
            indices.Add(4); indices.Add(5);

            Mesh m = new Mesh(name, vertices.ToArray(), indices.ToArray(), PrimitiveType.Lines);
            m.IsVertexColored = true;
            return m;
        }

        public static Mesh CreateSquare(bool doubleSided = false, bool isTextured = true)
        {
            string name = doubleSided ? "SquareDS" : "Square";
            if (Scene.Meshes.ContainsKey(name))
                return Scene.Meshes[name];

            List<Vertex> vertices = new List<Vertex>();
            List<ushort> indices = new List<ushort>();

            vertices.Add(new Vertex(new Vector3(-0.5f, -0.5f, 0f), new Vector2(0.0001f, 0.9999f)));
            vertices.Add(new Vertex(new Vector3(0.5f, -0.5f, 0f), new Vector2(0.9999f, 0.9999f)));
            vertices.Add(new Vertex(new Vector3(0.5f, 0.5f, 0f), new Vector2(0.9999f, 0.0001f)));
            vertices.Add(new Vertex(new Vector3(-0.5f, 0.5f, 0f), new Vector2(0.0001f, 0.0001f)));
            indices.Add(0); indices.Add(1); indices.Add(2);
            indices.Add(3); indices.Add(0); indices.Add(2);
            if (doubleSided)
            {
                indices.Add(0); indices.Add(2); indices.Add(1);
                indices.Add(3); indices.Add(2); indices.Add(0);
            }

            Mesh m = new Mesh(name, vertices.ToArray(), indices.ToArray(), PrimitiveType.Triangles);
            m.IsTextured = isTextured;
            return m;
        }

        public static Mesh CreatePoint()
        {
            string name = "Point";
            if (Scene.Meshes.ContainsKey(name)) return Scene.Meshes[name];

            List<Vertex> vertices = new List<Vertex>();
            List<ushort> indices = new List<ushort>();

            vertices.Add(new Vertex());
            indices.Add(0);

            Mesh m = new Mesh(name, vertices.ToArray(), indices.ToArray(), PrimitiveType.Points);
            m.IsVertexColored = true;
            return m;
        }
    }
}
