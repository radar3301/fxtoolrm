﻿#version 330

smooth in vec2 outUV;
smooth in vec3 outCol;

uniform int isVertexColored;
uniform int isTextured;
uniform int isShaderColored;

uniform sampler2D inTex;
uniform vec4 color;

out vec4 finalColor;

void main()
{
	vec4 vtxColor = vec4(1.0);
	vec4 texColor = vec4(1.0);
	vec4 shdColor = vec4(1.0);

	if (isVertexColored == 1)
		vtxColor = vec4(outCol, 1.0);
	if (isTextured == 1)
		texColor = vec4(texture(inTex, outUV).xyz, 1.0);
	if (isShaderColored == 1)
		shdColor = color;
	
	finalColor = vtxColor * texColor * shdColor;
}