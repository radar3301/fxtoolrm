﻿#version 330

uniform mat4 inMatM;
uniform mat4 inMatV;
uniform mat4 inMatP;

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec2 inUV;
layout (location = 2) in vec3 inCol;

smooth out vec2 outUV;
smooth out vec3 outCol;

void main()
{
	outUV = inUV;
	outCol = inCol;
	gl_Position = inMatP*inMatV*inMatM*vec4(inPos, 1.0);
}