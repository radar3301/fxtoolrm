﻿using System.Collections.Generic;
using OpenTK;

namespace RenderLibrary
{
    public class Model
    {
        public Node parent = null;

        public string Name { get; }
        private List<MeshPart> MeshParts = new List<MeshPart>();
        
        public Vector3 Position = Vector3.Zero;
        public Vector3 Rotation = Vector3.Zero;
        public Vector3 Scale = Vector3.One;
        public Matrix4 CustomMatrix = Matrix4.Identity;

        public bool Visible { get; set; } = true;

        public Model(string name, Mesh mesh)
        {
            Name = name;
            MeshParts.Add(new MeshPart(mesh));
        }

        public void AddMeshPart(MeshPart part)
        {
            MeshParts.Add(part);
        }

        public Model(string name, bool addToList = true)
        {
            Name = name;
            if (addToList)
                Scene.Models.Add(this);
        }

        public Matrix4 ModelMatrix
        {
            get
            {
                return Matrix4.CreateScale(Scale) *
                    Matrix4.CreateRotationZ(Rotation.Z) *
                    Matrix4.CreateRotationY(Rotation.Y) *
                    Matrix4.CreateRotationX(Rotation.X) *
                    Matrix4.CreateTranslation(Position) *
                    CustomMatrix;
            }
        }

        public void Draw()
        {
            if (!Visible) return;
            Utility.GetGLError("Model Pre-Draw");
            Matrix4 modelview = ModelMatrix;
            Scene.Shader.SetUniform("inMatM", ref modelview);
            foreach (MeshPart m in MeshParts)
                m.Draw();
            Utility.GetGLError("Model Post-Draw");
        }
    }
}