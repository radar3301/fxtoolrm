﻿using System.Collections.Generic;
using OpenTK;

namespace RenderLibrary
{
    public class Node
    {
        public Node Parent = null;
        public List<Node> Children = new List<Node>();

        public Vector3 Position = Vector3.Zero;
        public Vector3 Rotation = Vector3.Zero;
        public Vector3 Scale = Vector3.One;
        public Matrix4 CustomMatrix = Matrix4.Identity;

        public Matrix4 NodeMatrix
        {
            get
            {
                return Matrix4.CreateScale(Scale) *
                    Matrix4.CreateRotationZ(Rotation.Z) *
                    Matrix4.CreateRotationY(Rotation.Y) *
                    Matrix4.CreateRotationX(Rotation.X) *
                    Matrix4.CreateTranslation(Position) *
                    CustomMatrix;
            }
        }

        public Matrix4 WorldMatrix
        {
            get
            {
                Matrix4 mat = NodeMatrix;
                Node p = Parent;
                while (p != null)
                {
                    mat *= p.NodeMatrix;
                    p = p.Parent;
                }
                return mat;
            }
        }
    }
}