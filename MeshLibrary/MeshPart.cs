﻿using OpenTK.Graphics.OpenGL;

namespace RenderLibrary
{
    public class MeshPart
    {
        private Mesh mesh;
        public Texture Texture { get; set; }

        public MeshPart(Mesh mesh)
        {
            this.mesh = mesh;
        }

        public void Draw()
        {
            Utility.GetGLError("MeshPart Pre-Draw");
            if (Texture != null)
            {
                Texture.Attach();
                Scene.Shader.SetUniform("inTex", Texture.ID);
            }
            else
            {
                Scene.Shader.SetUniform("inTex", 0);
            }
            mesh.Draw();
            Utility.GetGLError("MeshPart Post-Draw");
        }
    }
}