﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using static RenderLibrary.Utility;

namespace RenderLibrary
{
    public class Mesh
    {
        public string Name { get; }
        public bool IsVertexColored { get; set; }
        public bool IsTextured { get; set; }
        public bool IsShaderColored { get; set; }

        //public Texture Texture { get; set; }

        public List<Vertex> Vertices { get; } = new List<Vertex>();
        public List<ushort> Indices { get; } = new List<ushort>();
        public PrimitiveType PrimitiveType { get; }

        private int pos_buffer = 0;
        private int uv_buffer = 0;
        private int col_buffer = 0;
        private int ind_buffer = 0;

        public Mesh(string name, Vertex[] vertices, ushort[] indices, PrimitiveType primType)
        {
            Name = name;
            Vertices.AddRange(vertices);
            Indices.AddRange(indices);
            PrimitiveType = primType;

            pos_buffer = GL.GenBuffer();
            uv_buffer = GL.GenBuffer();
            col_buffer = GL.GenBuffer();
            ind_buffer = GL.GenBuffer();

            Vector3[] mesh_verts = new Vector3[VertexCount];
            Vector2[] mesh_uvs = new Vector2[VertexCount];
            Vector3[] mesh_cols = new Vector3[VertexCount];

            for (int i = 0; i < VertexCount; i++)
            {
                mesh_verts[i] = vertices[i].Pos;
                mesh_uvs[i] = vertices[i].UV;
                mesh_cols[i] = vertices[i].Color;
            }

            BindBufferData("MeshBuffering", pos_buffer, mesh_verts);
            BindBufferData("MeshBuffering", uv_buffer, mesh_uvs);
            BindBufferData("MeshBuffering", col_buffer, mesh_cols);
            BindBufferData("MeshBuffering", ind_buffer, indices);

            Scene.Meshes.Add(name, this);
        }

        public Mesh(string name) : this(name, new Vertex[0], new ushort[0], PrimitiveType.Triangles) { }

        public int VertexCount => Vertices.Count;
        public int IndiceCount => Indices.Count;

        public void Draw()
        {
            GetGLError("Mesh Pre-Draw");
            LinkAttrib3(0, pos_buffer, false);
            LinkAttrib2(1, uv_buffer, false);
            LinkAttrib3(2, col_buffer, false);

            Scene.Shader.SetUniform("isVertexColored", IsVertexColored ? 1 : 0);
            Scene.Shader.SetUniform("isTextured", IsTextured ? 1 : 0);
            Scene.Shader.SetUniform("isShaderColored", IsShaderColored ? 1 : 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ind_buffer);
            GL.DrawElements(PrimitiveType, IndiceCount, DrawElementsType.UnsignedShort, 0);
            GetGLError("Mesh Post-Draw");
        }
    }
}
